try{
  describe ("DroidScript API", function(){
    describe ("DSError", function(){
      it ("should be an instance of Error (inheritance)", function(){
        new DSError().should.be.an.instanceOf(Error)
        new Error().should.not.be.an.instanceOf(DSError)
      })
      it ("should be an instance of DSError", function(){
        new DSError().should.be.an.instanceOf(DSError)
      })
    })
    describe ("DSTouchEvent", function(){
      var ev = new DSTouchEvent( { source:{id:0}, action:"mock", X:2, Y:3, x:[4,5,6], y:[7,8,9] } )
      it ("should be an instance of DSEvent (inheritance)", function(){
        ev.should.be.an.instanceOf(DSEvent)
        ev.should.be.an.instanceOf(DSTouchEvent)
        ev.action.should.equal("mock")
      })
    })
    describe ("DSObject", function(){
      window._map = []
      var dso = new DSObject(0)
   	   alert(JSON.stringify(_map))
      it ("should be ok", function(){
        dso.should.be.ok
      })
    })
/*
    describe ("DSTouchEvent", function(){
      var ev = new DSTouchEvent( { source:{id:0}, action:"mock", X:2, Y:3, x:[4,5,6], y:[7,8,9] } )
      it ("should be an instance of DSEvent (inheritance)", function(){
      })
    })
    describe ("DSTouchEvent", function(){
      var ev = new DSTouchEvent( { source:{id:0}, action:"mock", X:2, Y:3, x:[4,5,6], y:[7,8,9] } )
      it ("should be an instance of DSEvent (inheritance)", function(){
      })
    })
    describe ("DSTouchEvent", function(){
      var ev = new DSTouchEvent( { source:{id:0}, action:"mock", X:2, Y:3, x:[4,5,6], y:[7,8,9] } )
      it ("should be an instance of DSEvent (inheritance)", function(){
      })
    })
*/

  })
}
catch (exception) {
  alert (exception.stack);
}
