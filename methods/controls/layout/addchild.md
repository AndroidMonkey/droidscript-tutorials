# DroidScript Tutorials
## Reference
### Methods » Controls » Layout » AddChild

[← Back to Layout](README.md)

#### Purpose

Adds child control to layout.

#### Parameters

Parameter | Type | Required
----------|------|---------
child | control | yes
order | integer | no

#### Returns

Nothing.

#### Example

```js

//Called when application is started.
function OnStart()
{
  //Create a layout with objects vertically centered.
  lay = app.CreateLayout( "linear", "VCenter,FillXY" );	

  //Create a text label and add it to layout.
  txt = app.CreateText( "Hello" );
  txt.SetTextSize( 32 );
  lay.AddChild( txt );

  //Add layout to app.	
  app.AddLayout( lay );
}

```

#### Related

[← Back to Layout](README.md)