# DroidScript Tutorials
## Reference
### Methods » Controls » Layout
 - [AddChild](addchild.md)
 - [Animate](animate.md)
 - [ChildToFront](childtofront.md)
 - [DestroyChild](destroychild.md)
 - [GetChildOrder](getchildorder.md)
 - [GetType](gettype.md)
 - [RemoveChild](removechild.md)
 - [SetGravity](setgravity.md)
 - [SetOnChildChange](setonchildchange.md)
 - [SetOnLongTouch](setonlongtouch.md)
 - [SetOnTouch](setontouch.md)
 - [SetOnTouchDown](setontouchdown.md)
 - [SetOnTouchMove](setontouchmove.md)
 - [SetOnTouchUp](setontouchup.md)
 - [SetOrientation](setorientation.md)
 - [SetTouchable](settouchable.md)