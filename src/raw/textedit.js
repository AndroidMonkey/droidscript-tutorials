
/**
 * TextEdit
 * @constructor
 * @param {String} text
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function TextEdit( text, width, height, options ) {
    var id = prompt( "#", "App.CreateTextEdit("+text+"\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating TextEdit"); }
}
TextEdit.prototype = Object.create( DSControl.prototype ); 
TextEdit.prototype.constructor = TextEdit; 

/**
 * 
 * @memberof TextEdit
 */
TextEdit.prototype.ClearHistory = function() { prompt( this.id, "Txe.ClearHistory("); }

/**
 * Returns the cursor line.
 * @memberof TextEdit
 * @returns {Number}
 */
TextEdit.prototype.GetCursorLine = function() { return parseInt(prompt( this.id, "Txe.GetCursorLine(")); }

/**
 * Returns the cursor position.
 * @memberof TextEdit
 * @returns {Number}
 */
TextEdit.prototype.GetCursorPos = function() { return parseInt(prompt( this.id, "Txe.GetCursorPos(")); }

/**
 * Returns the HTML content.
 * @memberof TextEdit
 * @returns {String}
 */
TextEdit.prototype.GetHtml = function() { return prompt( this.id, "Txe.GetHtml(" ); }

/**
 * Returns the line count.
 * @memberof TextEdit
 * @returns {Number}
 */
TextEdit.prototype.GetLineCount = function() { return parseInt(prompt( this.id, "Txe.GetLineCount(")); }

/**
 * Returns the line start.
 * @memberof TextEdit
 * @param {Number} line
 * @returns {Number}
 */
TextEdit.prototype.GetLineStart = function( line ) { return parseInt(prompt( this.id, "Txe.GetLineStart("+line )); }

/**
 * Returns the line top.
 * @memberof TextEdit
 * @param {Number} line
 * @returns {Number}
 */
TextEdit.prototype.GetLineTop = function( line ) { return parseFloat(prompt( this.id, "Txe.GetLineTop("+line )); }

/**
 * Returns the maximum number of lines.
 * @memberof TextEdit
 * @returns {Number}
 */
TextEdit.prototype.GetMaxLines = function() { return parseInt(prompt( this.id, "Txe.GetMaxLines(")); }

/**
 * Returns the selected text.
 * @memberof TextEdit
 * @returns {String}
 */
TextEdit.prototype.GetSelectedText = function() { return prompt( this.id, "Txe.GetSelectedText("); }

/**
 * Returns the end of selected text.
 * @memberof TextEdit
 * @returns {Number}
 */
TextEdit.prototype.GetSelectionEnd = function() { return parseInt(prompt( this.id, "Txe.GetSelectionEnd(")); }

/**
 * Returns the start of selected text.
 * @memberof TextEdit
 * @returns {Number}
 */
TextEdit.prototype.GetSelectionStart = function() { return parseInt(prompt( this.id, "Txe.GetSelectionStart(")); }

/**
 * Returns the content as text.
 * @memberof TextEdit
 * @returns {String}
 */
TextEdit.prototype.GetText = function() { return prompt( this.id, "Txe.GetText(" ); }

/**
 * Returns the text size.
 * @memberof TextEdit
 * @param {MeasurementUnit} [mode="sp"]
 * @returns {Number}
 */
TextEdit.prototype.GetTextSize = function( mode ) { return parseFloat(prompt( this.id, "Txe.GetTextSize(\f"+mode )); }

/**
 * Returns the type name.
 * @memberof TextEdit
 * @returns {String}
 */
TextEdit.prototype.GetType = function() { return "TextEdit"; }

/**
 * 
 * @memberof TextEdit
 * @param {String} text
 * @param {Number} start
 * @param {Number} end
 */
TextEdit.prototype.InsertText = function( text,start,end ) { prompt( this.id, "Txe.InsertText(\f"+text+"\f"+start ); }

/**
 * 
 * @memberof TextEdit
 */
TextEdit.prototype.Redo = function() { prompt( this.id, "Txe.Redo("); }

/**
 * 
 * @memberof TextEdit
 * @param {String} text
 * @param {Number} start
 * @param {Number} end
 */
TextEdit.prototype.ReplaceText = function( text,start,end ) { prompt( this.id, "Txe.ReplaceText(\f"+text+"\f"+start+"\f"+end ); }

/**
 * 
 * @memberof TextEdit
 * @param {String} color
 */
TextEdit.prototype.SetCursorColor = function( color ) { prompt( this.id, "Txe.SetCursorColor(\f"+color ); }

/**
 * 
 * @memberof TextEdit
 * @param {Number} pos
 */
TextEdit.prototype.SetCursorPos = function( pos ) { prompt( this.id, "Txe.SetCursorPos("+pos ); }

/**
 * 
 * @memberof TextEdit
 * @param {String} text
 */
TextEdit.prototype.SetHint = function( text ) { prompt( this.id, "Txe.SetHint("+text ); }

/**
 * 
 * @memberof TextEdit
 * @param {String} html
 */
TextEdit.prototype.SetHtml = function( html ) { prompt( this.id, "Txe.SetHtml("+html ); }

/**
 * 
 * @memberof TextEdit
 * @param {Function} callback
 */
TextEdit.prototype.SetOnChange = function( callback ) { prompt( this.id, "Txe.SetOnChange(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof TextEdit
 * @param {Function} callback
 */
TextEdit.prototype.SetOnEnter = function( callback ) { prompt( this.id, "Txe.SetOnEnter(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof TextEdit
 * @param {Function} callback
 */
TextEdit.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Txe.SetOnTouch(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof TextEdit
 * @param {Number} start
 * @param {Number} stop
 */
TextEdit.prototype.SetSelection = function( start,stop ) { prompt( this.id, "Txe.SetSelection(\f"+start+"\f"+stop ); }

/**
 * 
 * @memberof TextEdit
 * @param {String} txt
 */
TextEdit.prototype.SetText = function( txt ) { prompt( this.id, "Txe.SetText("+txt ); }

/**
 * 
 * @memberof TextEdit
 * @param {String} color
 */
TextEdit.prototype.SetTextColor = function( color ) { prompt( this.id, "Txe.SetTextColor("+color ); }

/**
 * 
 * @memberof TextEdit
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
TextEdit.prototype.SetTextSize = function( size,mode ) { prompt( this.id, "Txe.SetTextSize(\f"+size+"\f"+mode ); }

/**
 * 
 * @memberof TextEdit
 */
TextEdit.prototype.Undo = function() { prompt( this.id, "Txe.Undo("); }
