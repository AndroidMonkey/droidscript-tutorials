
function _Try( p1,p2,p3 ) { return prompt( "#", "App.Try(\f"+p1+"\f"+p2+"\f"+p3 ); } 
function _Call( id, func, params ) { if( func ) func.apply( _map[id], params ); }
function _Cb( obj, func ) { return new _ObjCb(obj, func); }
function _ObjCb( obj, func ) { _cbMap[++_cbId] = obj; this.name = "_cbMap['"+_cbId+"']."+func; }
function _Cbm( func ) { return ( func ? (func.name ? func.name : new _ObjCbm(func).name) : null ); }
function _ObjCbm( func ) { _cbMap[++_cbId] = func; this.name = "_cbMap['"+_cbId+"']"; }

function _ExecV8( file ) { return prompt( "#", "_ExecV8(\f"+file ); }
function _Thread( file ) { return prompt( "#", "_Thread(\f"+file ); }
function _CreateCP( service ) { return prompt( "#", "_CreateCP(\f"+service ); }
function _ExecCP( callbackId,service,action,argsJson ) { 
	return prompt( "#", "_ExecCP(\f"+callbackId+"\f"+service+"\f"+action+"\f"+argsJson ); 
}

function _LoadScript( url, callback )
{
    if( _scripts[url] ) { 
		if( callback ) callback(); return; 
	}
	var dbg = _dbg; app.SetDebugEnabled( false );
	if( url.indexOf(":")<0 && !app.FileExists(url) ) 
	    alert("Error: "+url+" not found!" + 
	        (app.IsAPK?"\n\n(Note: Assets are case sensitive)":"") );
	app.SetDebugEnabled( dbg );
	if( _isV8 ) _ExecV8(url);
	else {
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;
		script.onload = callback;
		head.appendChild(script);
    }
    _scripts[url] = true;
}

function _LoadScriptSync( url )
{
    if( _scripts[url] ) return; 
    var dbg = _dbg; app.SetDebugEnabled( false );
    if( url.indexOf(":")<0 && !app.FileExists(url) ) 
	    alert("Error: "+url+" not found!" + 
	        (app.IsAPK?"\n\n(Note: Assets are case sensitive)":"") );
	app.SetDebugEnabled( dbg );
    if( _isV8 ) _ExecV8(url);
	else {
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		var dbg = _dbg; app.SetDebugEnabled( false );
		script.text = app.ReadFile( url ); 
		app.SetDebugEnabled( dbg );
		head.appendChild(script);
    }
    _scripts[url] = true;
}

function _LoadPlugin( name )
{
    if( !name ) return;
    var privDir = app.GetPrivateFolder( "Plugins" );
    _LoadScriptSync( privDir+"/"+name.toLowerCase()+"/"+name+".inc" );
}

function _CreatePlugin( name, options ) 
{ 
	var ret = prompt( "#", "App.CreatePlugin("+name+"\f"+options ); 
	if( ret ) return new Plg(ret); 
	else throw "Failed to create plugin:" + name; 
}	

function _Run(s) { 
    //if( _busy ) { setTimeout( function(){_Run(s)},0); return; }
	_busy = true; eval( s ); _busy = false;
}

function _SafeRun(s) {
	//if( _busy ) { setTimeout( function(){_SafeRun(s)},0); return; }
	try { _busy = true; eval( s ); _busy = false; }
	catch(e) {}
}

function T( id, lang ) {
	var tr = _languages.trans[id];
	if( tr ) tr = tr[lang?lang:_curLang]; else return "[No Translation]";
	return (tr ? tr : _languages.trans[id]["en"]);
}

function OnCreate(extract,debug) 
{       
	if( typeof _CheckFolderName=='function' ) _CheckFolderName();
	//if( extract ) app._Extract( true );
	if(typeof OnStart=='function') { OnStart(); prompt("#","_Start"); }
	if( debug ) app.CreateDebug();
}

app = new DSClient();

function _Log( msg ) { app.Debug( msg ); }
function _Err( msg ) { app.Debug( "ERROR: " + msg ); }
function _GetStack() { return new Error().stack }
function _AddPermissions() {}
function _RemovePermissions() {}
function _AddOptions() {}

if( typeof navigator=="undefined" ) { navigator = {userAgent:"Android"} };
if( navigator.userAgent.indexOf("Android")>-1 )
{
	//Provide toast popup on Remix.
	_dlgPop = null;
	if( app.GetModel().indexOf("Remix")>-1 )
	{
		app.ShowPopup = function( msg, options )
		{
			if( options ) options = options.toLowerCase();
			else options = "";
			if( _dlgPop==null ) 
			{ 
				_dlgPop = app.CreateDialog( null, "NoDim,NoTouch,NoFocus" ); 
				_dlgPop.SetBackColor( "#cc000000" );
				_dlgPop.SetPosition( -1, options.indexOf("bottom")>-1 ? 0.75 : 0.25  );
				var lay = app.CreateLayout( "linear", "vcenter" );
				lay.SetPadding( 0.02, 0.02, 0.02, 0.02 );
				_dlgPop.AddLayout( lay );
				_txtDlgPop = app.CreateText( msg );
				_txtDlgPop.SetTextSize( 22 );
				_txtDlgPop.SetTextColor( "#ffffff" );
				lay.AddChild( _txtDlgPop );
			}
			else _txtDlgPop.SetText( msg );
			_dlgPop.Show();
			if( _dlgPop.timer ) clearTimeout( _dlgPop.timer ); 
			var time = ( options.indexOf("short") ? 2000 : 4000 );
			_dlgPop.timer = setTimeout( function() { _dlgPop.Hide(); }, time );
		}
	}

	//Init app.
	prompt( "#", "_Init" );
}
