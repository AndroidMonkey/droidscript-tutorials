/**
 * CheckBox
 * @constructor
 * @param {String} text
 * @param {Number} width
 * @param {Number} height
 * @param {String} options 
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function CheckBox( text, width, height, options ) {
    var id = prompt( "#", "App.CreateCheckBox("+text+"\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating CheckBox"); }
}
CheckBox.prototype = Object.create( DSControl.prototype ); 
CheckBox.prototype.constructor = CheckBox; 

/**
 * 
 * @memberof CheckBox
 * @param {Number} hue
 * @param {Number} sat
 * @param {Number} bright
 * @param {Number} cont
 */
CheckBox.prototype.AdjustColor = function( hue, sat, bright, cont ) { prompt( this.id, "Chk.AdjustColor(\f"+hue+"\f"+sat+"\f"+bright+"\f"+cont ); }

/**
 * Returns if the toggle is checked or not.
 * @memberof CheckBox
 * @returns {Boolean}
 */
CheckBox.prototype.GetChecked = function() { return prompt( this.id, "Chk.GetChecked(" )=="true"; }

/**
 * Returns the text contents.
 * @memberof CheckBox
 * @returns {String}
 */
CheckBox.prototype.GetText = function() { return prompt( this.id, "Chk.GetText(" ); }

/**
 * Returns the size of text.
 * @memberof CheckBox
 * @param {MeasurementUnit} [mode="sp"]
 * @returns {Number}
 */
CheckBox.prototype.GetTextSize = function( mode ) { return parseFloat(prompt( this.id, "Chk.GetTextSize(\f"+mode )); }

/**
 * Returns the type name.
 * @memberof CheckBox
 * @returns {String}
 */
CheckBox.prototype.GetType = function() { return "CheckBox"; }

/**
 * 
 * @memberof CheckBox
 * @param {Boolean} checked
 */
CheckBox.prototype.SetChecked = function( checked ) { prompt( this.id, "Chk.SetChecked("+checked ); }

/**
 * 
 * @memberof CheckBox
 * @param {String} clr
 * @param {String} mode
 */
CheckBox.prototype.SetColorFilter = function( clr, mode ) { prompt( this.id, "Chk.SetColorFilter(\f"+clr+"\f"+mode ); }

/**
 * 
 * @memberof CheckBox
 * @param {Function} callback
 */
CheckBox.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Chk.SetOnClick("+_Cbm(callback) ); }

/**
 * 
 * @memberof CheckBox
 * @param {String} text
 */
CheckBox.prototype.SetText = function( text ) { prompt( this.id, "Chk.SetText("+text ); }

/**
 * 
 * @memberof CheckBox
 * @param {String} clr
 */
CheckBox.prototype.SetTextColor = function( clr ) { prompt( this.id, "Chk.SetTextColor("+clr ); }

/**
 * 
 * @memberof CheckBox
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
CheckBox.prototype.SetTextSize = function( size, mode ) { prompt( this.id, "Chk.SetTextSize(\f"+size+"\f"+mode ); }
