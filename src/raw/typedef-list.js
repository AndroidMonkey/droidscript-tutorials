
/**
 * A string containing a delimited list of items, most often seperated by a comma (",").
 * @typedef {String} List
 * @example <caption>Example usage of List.</caption>
 * var menu_list = "Undo,Redo,Cut,Copy,Paste";
 * function OnStart
 * {
 *   app.SetMenu( menu_list );
 *   app.ShowMenu();
 * }
 * function OnMenu( item )
 * {
 *   app.ShowPopup( item, "Short" );
 * }
 */
