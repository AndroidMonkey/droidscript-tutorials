
/**
 * VideoView
 * @constructor
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function VideoView( width, height, options ) {
    var id = prompt( "#", "App.CreateVideoView(\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating VideoView"); }
}
VideoView.prototype = Object.create( DSControl.prototype ); 
VideoView.prototype.constructor = VideoView; 

/**
 * 
 * @memberof VideoView
 * @returns {Number}
 */
VideoView.prototype.GetDuration = function() { return parseFloat(prompt( this.id, "Vid.GetDuration(" )); }

/**
 * 
 * @memberof VideoView
 * @returns {Number}
 */
VideoView.prototype.GetPosition = function() { return parseFloat(prompt( this.id, "Vid.GetPosition(" )); }

/**
 * Returns the type name.
 * @memberof VideoView
 * @returns {String}
 */
VideoView.prototype.GetType = function() { return "VideoView"; }

/**
 * 
 * @memberof VideoView
 * @returns {Boolean}
 */
VideoView.prototype.IsPlaying = function() { return prompt( this.id, "Vid.IsPlaying(" )=="true"; }

/**
 * 
 * @memberof VideoView
 * @returns {Boolean}
 */
VideoView.prototype.IsReady = function() { return prompt( this.id, "Vid.IsReady(" )=="true"; }

/**
 * 
 * @memberof VideoView
 */
VideoView.prototype.Pause = function() { prompt( this.id, "Vid.Pause(" ); }

/**
 * 
 * @memberof VideoView
 */
VideoView.prototype.Play = function() { prompt( this.id, "Vid.Play(" ); }

/**
 * 
 * @memberof VideoView
 * @param {Number} secs
 */
VideoView.prototype.SeekTo = function( secs ) { prompt( this.id, "Vid.SeekTo("+secs ); }

/**
 * 
 * @memberof VideoView
 * @param {String} file
 */
VideoView.prototype.SetFile = function( file ) { prompt( this.id, "Vid.SetFile("+file ); }

/**
 * 
 * @memberof VideoView
 * @param {Function} callback
 */
VideoView.prototype.SetOnComplete = function( callback ) { prompt( this.id, "Vid.SetOnComplete("+_Cbm(callback) ); }

/**
 * 
 * @memberof VideoView
 * @param {Function} callback
 */
VideoView.prototype.SetOnError = function( callback ) { prompt( this.id, "Vid.SetOnError("+_Cbm(callback) ); }

/**
 * 
 * @memberof VideoView
 * @param {Function} callback
 */
VideoView.prototype.SetOnReady = function( callback ) { prompt( this.id, "Vid.SetOnReady("+_Cbm(callback) ); }

/**
 * 
 * @memberof VideoView
 * @param {Function} callback
 */
VideoView.prototype.SetOnSubtitle = function( callback ) { prompt( this.id, "Vid.SetOnSubtitle(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof VideoView
 * @param {String} file
 */
VideoView.prototype.SetSubtitles = function( file ) { prompt( this.id, "Vid.SetSubtitles(\f"+file ); }

/**
 * 
 * @memberof VideoView
 * @param {Number} left
 * @param {Number} right
 */
VideoView.prototype.SetVolume = function( left,right ) { prompt( this.id, "Vid.SetVolume(\f"+left+"\f"+right ); }

/**
 * 
 * @memberof VideoView
 */
VideoView.prototype.Stop = function() { prompt( this.id, "Vid.Stop(" ); }
