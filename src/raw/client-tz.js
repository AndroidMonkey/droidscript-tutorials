
/**
 * 
 * @memberof DSClient
 * @param {String} text
 * @param {String} pitch
 * @param {String} rate
 * @param {Function} callback
 * @param {String} stream
 * @param {String} locale
 * @param {String} engine
 */
DSClient.prototype.TextToSpeech = function ( text,pitch,rate,callback,stream,locale,engine ) { prompt( "#", "App.TextToSpeech(\f"+text+"\f"+pitch+"\f"+rate+"\f"+_Cbm(callback)+"\f"+stream+"\f"+locale+"\f"+engine ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.ToBack = function () { prompt( "#", "App.ToBack(" ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.Unlock = function () { prompt( "#", "App.Unlock(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} side
 */
DSClient.prototype.UnlockDrawer = function ( side ) { prompt( "#", "App.UnlockDrawer(\f"+side ); }

/**
 * 
 * @memberof DSClient
 * @param {String} address
 * @param {Function} callback
 */
DSClient.prototype.UnpairBtDevice = function ( address,callback ) { prompt( "#", "App.UnpairBtDevice(\f"+address+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 */
DSClient.prototype.UnzipFile = function ( src,dest ) { prompt( "#", "App.UnzipFile(\f"+src+"\f"+dest); }

/**
 * 
 * @memberof DSClient
 * @param {Number} percent
 */
DSClient.prototype.UpdateProgressBar = function ( percent ) { prompt( "#", "App.UpdateProgressBar(\f"+percent ); }

/**
 * 
 * @memberof DSClient
 * @param {String} url
 * @param {String} file
 * @param {String} name
 * @param {Function} callback
 */
DSClient.prototype.UploadFile = function ( url,file,name,callback ) { prompt( "#", "App.UploadFile(\f"+url+"\f"+file+"\f"+name+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {Number} url
 */
DSClient.prototype.Uri2Path = function ( uri ) { return prompt( "#", "App.Uri2Path(\f"+uri); }

/**
 * 
 * @memberof DSClient
 * @param {Number} pattern
 */
DSClient.prototype.Vibrate = function ( pattern ) { prompt( "#", "App.Vibrate("+pattern ); }

/**
 * 
 * @memberof DSClient
 * @param {Number} secs
 */
DSClient.prototype.Wait = function ( secs ) { prompt( "#", "App.Wait("+secs ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.WakeUp = function () { prompt( "#", "App.WakeUp(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} ssid
 * @param {String} key
 */
DSClient.prototype.WifiConnect = function ( ssid,key ) { prompt( "#", "App.WifiConnect(\f"+ssid+"\f"+key ); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @param {String} text
 * @param {String} mode
 * @param {String} encoding
 */
DSClient.prototype.WriteFile = function ( file,text,mode,encoding ) { prompt( "#", "App.WriteFile(\f"+file+"\f"+text+"\f"+mode+"\f"+encoding ); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 */
DSClient.prototype.ZipFile = function ( src,dest ) { prompt( "#", "App.ZipFile(\f"+src+"\f"+dest); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 */
DSClient.prototype.ZipFolder = function ( src,dest ) { prompt( "#", "App.ZipFile(\f"+src+"\f"+dest); }
