
/**
 * List
 * @constructor
 * @param {String} list
 * @param {String} title
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function ListView( list, title, options ) {
    if ( _lvw ) { _lvw.Release(); }
    var id = prompt( "#", "App.CreateListView(\f"+list+"\f"+title+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating ListView"); }
    _lvw = this;
}
ListView.prototype = Object.create( DSControl.prototype ); 
ListView.prototype.constructor = ListView; 

ListView.prototype.GetType = function() { return "ListView"; }
ListView.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Lvw.SetOnClick("+_Cbm(callback) ); }   
ListView.prototype.Show = function() { prompt( this.id, "Lvw.Show(" ); }
