
/**
 * DSError - Extends Error to aid in tracing.
 * @constructor
 * @param {String} [message="DS Error"]
 * @example <caption>Example usage of DSError</caption>
 * try {
 *   throw new DSError( "Unable to create Layout" );
 * }
 * catch (err) {
 *   if( err instanceof DSError ) {
 *     // handle error
 *   }
 *   else {
 *     // rethrow error 
 *     throw err;
 *   }
 * }
 */
function DSError( message ) { 
    this.name = "DSError"; 
    this.message = message || "DS Error"; 
    this.stack=(new Error (this.message)).stack.replace (/at new DSError[^\n]*\n/, ""); 
};
DSError.prototype = Object.create( Error.prototype ); 
DSError.prototype.constructor = DSError; 
