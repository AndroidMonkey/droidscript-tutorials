
/**
 * CodeEdit
 * @constructor
 * @param {String} text
 * @param {Number} width
 * @param {Number} height
 * @param {String} options 
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function CodeEdit( text, width, height, options ) {
    var id = prompt( "#", "App.CreateCodeEdit("+text+"\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating CodeEdit"); }
}
CodeEdit.prototype = Object.create( DSControl.prototype ); 
CodeEdit.prototype.constructor = CodeEdit; 

/**
 * 
 * @memberof CodeEdit
 */
CodeEdit.prototype.ClearHistory = function() { prompt( obj.id, "Cde.ClearHistory("); }

/**
 * 
 * @memberof CodeEdit
 */
CodeEdit.prototype.Copy = function() { prompt( obj.id, "Cde.Copy(" ); }

/**
 * 
 * @memberof CodeEdit
 */
CodeEdit.prototype.Cut = function() { prompt( obj.id, "Cde.Cut(" ); }

/**
 * Returns the cursor line.
 * @memberof CodeEdit
 * @returns {Number}
 */
CodeEdit.prototype.GetCursorLine = function() { return parseInt(prompt( obj.id, "Cde.GetCursorLine(")); }

/**
 * Returns the cursor position.
 * @memberof CodeEdit
 * @returns {Number}
 */
CodeEdit.prototype.GetCursorPos = function() { return parseInt(prompt( obj.id, "Cde.GetCursorPos(")); }

/**
 * Returns the line start.
 * @memberof CodeEdit
 * @param {Number} line
 * @returns {Number}
 */
CodeEdit.prototype.GetLineStart = function( line ) { return parseInt(prompt( obj.id, "Cde.GetLineStart("+line )); }

/**
 * Returns the select mode.
 * @memberof CodeEdit
 * @returns {String}
 */
CodeEdit.prototype.GetSelectMode = function() { return prompt( obj.id, "Cde.GetSelectMode(" )=="true"; }

/**
 * Returns the selected text.
 * @memberof CodeEdit
 * @returns {String}
 */
CodeEdit.prototype.GetSelectedText = function() { return prompt( obj.id, "Cde.GetSelectedText(" ); }

/**
 * Returns the content as text.
 * @memberof CodeEdit
 * @returns {String}
 */
CodeEdit.prototype.GetText = function() { return prompt( obj.id, "Cde.GetText(" ); }

/**
 * Returns the type name.
 * @memberof CodeEdit
 * @returns {String}
 */
CodeEdit.prototype.GetType = function() { return "CodeEdit"; }

/**
 * 
 * @memberof CodeEdit
 * @param {Number} line
 */
CodeEdit.prototype.HighlightLine = function( line ) { prompt( obj.id, "Cde.HighlightLine(\f"+line ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} text
 * @param {Number} start
 * @param {Number} end
 */
CodeEdit.prototype.InsertText = function( text,start,end ) { prompt( obj.id, "Cde.InsertText(\f"+text+"\f"+start ); }

/**
 * 
 * @memberof CodeEdit
 */
CodeEdit.prototype.Paste = function() { prompt( obj.id, "Cde.Paste(" ); }

/**
 * 
 * @memberof CodeEdit
 */
CodeEdit.prototype.Redo = function() { prompt( obj.id, "Cde.Redo("); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} text
 */
CodeEdit.prototype.Replace = function( text ) { prompt( obj.id, "Cde.Replace(\f"+text ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} text
 * @param {String} newText
 * @param {Boolean} matchCase
 * @param {Boolean} wholeWord
 */
CodeEdit.prototype.ReplaceAll = function( text,newText,matchCase,wholeWord ) { prompt( obj.id, "Cde.ReplaceAll(\f"+text+"\f"+newText+"\f"+matchCase+"\f"+wholeWord ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} text
 * @param {Number} start
 * @param {Number} end
 */
CodeEdit.prototype.ReplaceText = function( text,start,end ) { prompt( obj.id, "Cde.ReplaceText(\f"+text+"\f"+start+"\f"+end ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} text
 * @param {String} dir
 * @param {Boolean} matchCase
 * @param {Boolean} wholeWord
 */
CodeEdit.prototype.Search = function( text,dir,matchCase,wholeWord ) { prompt( obj.id, "Cde.Search(\f"+text+"\f"+dir+"\f"+matchCase+"\f"+wholeWord ); }

/**
 * 
 * @memberof CodeEdit
 */
CodeEdit.prototype.SelectAll = function() { prompt( obj.id, "Cde.SelectAll(" ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} scheme
 */
CodeEdit.prototype.SetColorScheme = function( scheme ) { prompt( obj.id, "Cde.SetColorScheme(\f"+scheme ); }

/**
 * 
 * @memberof CodeEdit
 * @param {Number} pos
 */
CodeEdit.prototype.SetCursorPos = function( pos ) { prompt( obj.id, "Cde.SetCursorPos("+pos ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} html
 */
CodeEdit.prototype.SetHtml = function( html ) { prompt( obj.id, "Cde.SetText(\f"+html ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} ext
 */
CodeEdit.prototype.SetLanguage = function( ext ) { prompt( obj.id, "Cde.SetLanguage(\f"+ext ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} method
 */
CodeEdit.prototype.SetNavigationMethod = function( method ) { prompt( obj.id, "Cde.SetNavigationMethod(\f"+method); }

/**
 * 
 * @memberof CodeEdit
 * @param {Function} callback
 */
CodeEdit.prototype.SetOnChange = function( callback ) { prompt( obj.id, "Cde.SetOnChange(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof CodeEdit
 * @param {Function} callback
 */
CodeEdit.prototype.SetOnDoubleTap = function( callback ) { prompt( obj.id, "Cde.SetOnDoubleTap(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof CodeEdit
 * @param {Function} callback
 */
CodeEdit.prototype.SetOnKey = function( callback ) { prompt( obj.id, "Cde.SetOnKey(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof CodeEdit
 * @param {Boolean} onOff
 */
CodeEdit.prototype.SetSelectMode = function( onOff ) { prompt( obj.id, "Cde.SetSelectMode(\f"+onOff ); }

/**
 * 
 * @memberof CodeEdit
 * @param {Number} start
 * @param {Number} stop
 */
CodeEdit.prototype.SetSelection = function( start,stop ) { prompt( obj.id, "Cde.SetSelection(\f"+start+"\f"+stop ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} txt
 */
CodeEdit.prototype.SetText = function( txt ) { prompt( obj.id, "Cde.SetText(\f"+txt ); }

/**
 * 
 * @memberof CodeEdit
 * @param {String} color
 */
CodeEdit.prototype.SetTextColor = function( color ) { prompt( obj.id, "Cde.SetTextColor(\f"+color ); }

/**
 * 
 * @memberof CodeEdit
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
CodeEdit.prototype.SetTextSize = function( size,mode ) { prompt( obj.id, "Cde.SetTextSize(\f"+size+"\f"+mode ); }

/**
 * 
 * @memberof CodeEdit
 * @param {Boolean} onOff
 */
CodeEdit.prototype.SetUseKeyboard = function( onOff ) { prompt( obj.id, "Cde.SetUseKeyboard(\f"+onOff ); }

/**
 * 
 * @memberof CodeEdit
 */
CodeEdit.prototype.Undo = function() { prompt( obj.id, "Cde.Undo("); }
