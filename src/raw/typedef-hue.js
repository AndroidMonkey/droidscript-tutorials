
/**
 * A number from 0 to 360.
 *   0-60  red-yellow
 *  60-120 yellow-green
 * 120-180 green-cyan
 * 180-240 cyan-blue
 * 240-300 blue-magenta
 * 300-360 magenta-red
 * @typedef {Number} Hue
 */
