
/**
 * Theme
 * @constructor
 * @param {String} baseTheme
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function Theme( baseTheme ) {
    var id = prompt( "#", "App.CreateTheme("+baseTheme );
    if ( id ) { DSObject.call( this, id ); }
    else { throw new DSError("Error creating Theme"); }
}
Theme.prototype = Object.create( DSObject.prototype ); 
Theme.prototype.constructor = Theme; 

/**
 * 
 * @memberof Theme
 * @param {Number} hue
 * @param {Number} sat
 * @param {Number} bright
 * @param {Number} cont
 */
Theme.prototype.AdjustColor = function( hue,sat,bright,cont ) { prompt( obj.id, "Thm.AdjustColor(\f"+hue+"\f"+sat+"\f"+bright+"\f"+cont ); }

/**
 * Returns the type name.
 * @memberof Theme
 * @returns {String}
 */
Theme.prototype.GetType = function() { return "Theme"; }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetBackColor = function( clr ) { prompt( obj.id, "Thm.SetBackColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {String} file
 * @param {String} options
 */
Theme.prototype.SetBackground = function( file,options ) { prompt( obj.id, "Thm.SetBackground(\f"+file+"\f"+options ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetBtnTextColor = function( clr ) { prompt( obj.id, "Thm.SetBtnTextColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {String} options
 */
Theme.prototype.SetButtonOptions = function( options ) { prompt( obj.id, "Thm.SetButtonOptions(\f"+options ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr1
 * @param {String} clr2
 * @param {Number} radius
 * @param {String} strokeClr
 * @param {Number} strokeWidth
 * @param {String} shadow
 * @param {String} checkClr
 */
Theme.prototype.SetButtonStyle = function( clr1,clr2,radius,strokeClr,strokeWidth,shadow,checkClr ) { prompt( obj.id, "Thm.SetButtonStyle(\f"+clr1+"\f"+clr2+"\f"+radius+"\f"+strokeClr+"\f"+strokeWidth+"\f"+shadow+"\f"+checkClr ); }

/**
 * 
 * @memberof Theme
 * @param {String} options
 */
Theme.prototype.SetCheckBoxOptions = function( options ) { prompt( obj.id, "Thm.SetCheckBoxOptions(\f"+options ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetDialogBtnColor = function( clr ) { prompt( obj.id, "Thm.SetDialogBtnColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetDialogBtnTxtColor = function( clr ) { prompt( obj.id, "Thm.SetDialogBtnTxtColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetDialogColor = function( clr ) { prompt( obj.id, "Thm.SetDialogColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {Boolean} dim
 */
Theme.prototype.SetDimBehind = function( dim ) { prompt( obj.id, "Thm.SetDimBehind(\f"+dim ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetProgressBackColor = function( clr ) { prompt( obj.id, "Thm.SetProgressBackColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {String} options
 */
Theme.prototype.SetProgressBarOptions = function( options ) { prompt( obj.id, "Thm.SetProgressBarOptions(\f"+options ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetProgressTextColor = function( clr ) { prompt( obj.id, "Thm.SetProgressTextColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetTextColor = function( clr ) { prompt( obj.id, "Thm.SetTextColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {String} options
 */
Theme.prototype.SetTextEditOptions = function( options ) { prompt( obj.id, "Thm.SetTextEditOptions(\f"+options ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetTitleColor = function( clr ) { prompt( obj.id, "Thm.SetTitleColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {String} clr
 */
Theme.prototype.SetTitleDividerColor = function( clr ) { prompt( obj.id, "Thm.SetTitleDividerColor(\f"+clr ); }

/**
 * 
 * @memberof Theme
 * @param {Number} height
 * @param {String} options
 */
Theme.prototype.SetTitleHeight = function( height,options ) { prompt( obj.id, "Thm.SetTitleHeight(\f"+height+"\f"+options ); }
