
/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.HasSoftNav = function () { return prompt( "#", "App.HasSoftNav(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} hide
 */
DSClient.prototype.HideKeyboard = function ( hide ) { prompt( "#", "App.HideKeyboard("+hide ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.HideProgress = function () { prompt( "#", "App.HideProgress(" ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.HideProgressBar = function () { prompt( "#", "App.HideProgressBar(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} type
 * @param {String} baseUrl
 * @param {String} path
 * @param {String} params
 * @param {Function} callback
 * @param {String} headers
 */
DSClient.prototype.HttpRequest = function ( type,baseUrl,path,params,callback,headers ) { prompt( "#", "App.HttpRequest(\f"+type+"\f"+baseUrl+"\f"+path+"\f"+params+"\f"+_Cbm(callback)+"\f"+headers); }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.InIDE = function () { return prompt( "#", "App.InIDE(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} packageName
 * @param {String} className
 */
DSClient.prototype.InstallWallpaper = function ( packageName,className ) { prompt( "#", "App.InstallWallpaper\f"+packageName+"\f"+className ); }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsAPK = function () { return prompt( "#", "App.IsAPK(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} packageName
 * @returns {Boolean}
 */
DSClient.prototype.IsAppInstalled = function ( packageName ) { return prompt( "#", "App.IsAppInstalled(\f"+packageName )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsBluetoothEnabled = function () { return prompt( "#", "App.IsBluetoothEnabled(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsBluetoothOn = function () { return prompt( "#", "App.IsBluetoothOn(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @returns {Boolean}
 */
DSClient.prototype.IsBtDevicePaired = function ( name ) { return prompt( "#", "App.IsBtDevicePaired(\f"+name )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsCharging = function () { return prompt( "#", "App.IsCharging(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsChrome = function () { return prompt( "#", "App.IsChrome(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsConnected = function () { return prompt( "#", "App.IsConnected(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsEngine = function () { return prompt( "#", "App.IsEngine(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} fldr
 * @returns {Boolean}
 */
DSClient.prototype.IsFolder = function ( fldr ) { return prompt( "#", "App.IsFolder("+fldr )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsKeyboardShown = function () { return prompt( "#", "App.IsKeyboardShown(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} types
 * @returns {Boolean}
 */
DSClient.prototype.IsLocationEnabled = function ( types ) { return prompt( "#", "App.IsLocationEnabled(\f"+types )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsNewVersion = function () { return prompt( "#", "App.IsNewVersion(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsPremium = function () { return prompt( "#", "App.IsPremium(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsScreenOn = function () { return prompt( "#", "App.IsScreenOn(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsService = function () { return prompt( "#", "App.IsService(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsTablet = function () { return prompt( "#", "App.IsTablet(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsThings = function () { return prompt( "#", "App.IsThings(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @returns {Boolean}
 */
DSClient.prototype.IsWifiEnabled = function () { return prompt( "#", "App.IsWifiEnabled(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} procId
 */
DSClient.prototype.KillApp = function ( procId ) { prompt( "#", "App.KillApp("+procId ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @returns {String}
 */
DSClient.prototype.Language2Code = function (name) { 
		if( name ) return _languages.codes[name.toLowerCase()]; 
		else return _curLang; 
	}

/**
 * 
 * @memberof DSClient
 * @param {String} path
 * @param {String} filter
 * @param {String} limit
 * @param {String} options
 * @returns {String}
 */
DSClient.prototype.ListFolder = function ( path,filter,limit,options ) { return eval(prompt( "#", "App.ListFolder(\f"+path+"\f"+filter+"\f"+limit+"\f"+options )); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} dflt
 * @param {String} file
 * @returns {Boolean}
 */
DSClient.prototype.LoadBoolean = function ( name,dflt,file ) { return (prompt( "#", "App.LoadBoolean("+name+"\f"+dflt+"\f"+file )=="true"); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} dflt
 * @param {String} file
 * @returns {Number}
 */
DSClient.prototype.LoadNumber = function ( name,dflt,file ) { return parseFloat(prompt( "#", "App.LoadNumber("+name+"\f"+dflt+"\f"+file )); }

/**
 * 
 * @memberof DSClient
 * @param {String} url
 */
DSClient.prototype.LoadPlugin = function ( url ) { _LoadPlugin( url ); }

/**
 * 
 * @memberof DSClient
 * @param {String} url
 * @param {Function} callback
 */
DSClient.prototype.LoadScript = function ( url, callback ) { _LoadScript( url, callback ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} dflt
 * @param {String} file
 * @returns {Number}
 */
DSClient.prototype.LoadText = function ( name,dflt,file ) { return prompt( "#", "App.LoadText("+name+"\f"+dflt+"\f"+file ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.Lock = function () { prompt( "#", "App.Lock(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} side
 */
DSClient.prototype.LockDrawer = function ( side ) { prompt( "#", "App.LockDrawer(\f"+side ); }

/**
 * 
 * @memberof DSClient
 * @param {String} fldr
 */
DSClient.prototype.MakeFolder = function ( fldr ) { prompt( "#", "App.MakeFolder("+fldr ); }

/**
 * 
 * @memberof DSClient
 * @param {String} p1
 * @param {String} p2
 * @param {String} p3
 * @returns {String}
 */
DSClient.prototype.Odroid = function ( p1,p2,p3 ) { return prompt( "#", "App.Odroid(\f"+p1+"\f"+p2+"\f"+p3 ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @returns {Database}
 */
DSClient.prototype.OpenDatabase = function ( name ) 
	{
		_LoadScriptSync( "/Sys/cp.js" );
		_LoadScriptSync( "/Sys/sql.js" );
		_CreateCP( "sqliteplugin" );
		
		var db = sqlitePlugin.openDatabase( name );
		db.name = name;
	    
	    db.GetType = function() { return "Database"; }
	    db.GetName = function() { return db.name; }
		db.ExecuteSql = function( sql, params, success, error ) 
		{
			if( !success ) success = null;
			if( !error ) error = _Err;
	      
			db.transaction( function(tx) { 
				tx.executeSql( sql, params, 
					function(tx,res) { if(success) success.apply(db,[res]) }, 
					function(t,e) { error.apply(db,[e.message]); } 
				); }, error
			);
		}
		db.Close = function() { db.close( _Log, _Err ); }
		db.Delete = function() { sqlitePlugin.deleteDatabase(db.name,_Log,_Err); }
		return db;
	}

/**
 * 
 * @memberof DSClient
 * @param {String} side
 */
DSClient.prototype.OpenDrawer = function ( side ) { prompt( "#", "App.OpenDrawer(\f"+side ); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @param {String} type
 * @param {String} choose
 */
DSClient.prototype.OpenFile = function ( file,type,choose ) { prompt( "#", "App.OpenFile(\f"+file+"\f"+type+"\f"+choose ); }

/**
 * 
 * @memberof DSClient
 * @param {String} url
 * @param {String} type
 * @param {String} choose
 */
DSClient.prototype.OpenUrl = function ( url,type,choose ) { prompt( "#", "App.OpenUrl(\f"+url+"\f"+type+"\f"+choose ); }

/**
 * 
 * @memberof DSClient
 * @param {String} address
 * @param {Function} callback
 */
DSClient.prototype.PairBtDevice = function ( address,callback ) { prompt( "#", "App.PairBtDevice(\f"+address+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} type
 */
DSClient.prototype.PlayRingtone = function ( type ) { prompt( "#", "App.PlayRingtone(\f"+type ); }

/**
 * 
 * @memberof DSClient
 * @param {String} mode
 */
DSClient.prototype.PreventScreenLock = function ( mode ) { prompt( "#", "App.PreventScreenLock("+mode ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.PreventWifiSleep = function () { prompt( "#", "App.PreventWifiSleep(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} uri
 * @param {String} columns
 * @param {String} select
 * @param {String} args
 * @param {String} sort
 * @returns {String} 
 */
DSClient.prototype.QueryContent = function ( uri,columns,select,args,sort ) { return eval(prompt( "#", "App.QueryContent(\f"+uri+"\f"+columns+"\f"+select+"\f"+args+"\f"+sort)); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @param {String} encoding
 * @returns {String}
 */
DSClient.prototype.ReadFile = function ( file,encoding ) { return prompt( "#", "App.ReadFile(\f"+file+"\f"+encoding ); }

/**
 * 
 * @memberof DSClient
 * @param {Layout} layout
 */
DSClient.prototype.RemoveDrawer = function ( layout ) { prompt( "#", "App.RemoveDrawer(\f"+ layout.id ); }

/**
 * 
 * @memberof DSClient
 * @param {Layout} layout
 */
DSClient.prototype.RemoveLayout = function ( layout ) { prompt( "#", "App.RemoveLayout("+ layout.id ); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 */
DSClient.prototype.RenameFile = function ( src,dest ) { prompt( "#", "App.RenameFile(\f"+src+"\f"+dest); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 */
DSClient.prototype.RenameFolder = function ( src,dest ) { prompt( "#", "App.RenameFile(\f"+src+"\f"+dest); }
