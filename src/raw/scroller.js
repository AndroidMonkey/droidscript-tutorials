
/**
 * Scroller
 * @constructor
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function Scroller( width, height, options ) {
    var id = prompt( "#", "App.CreateScroller("+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating Scroller"); }
}
Scroller.prototype = Object.create( DSControl.prototype ); 
Scroller.prototype.constructor = Scroller; 

/**
 * 
 * @memberof Scroller
 * @param {DSControl}
 */
Scroller.prototype.AddChild = function( child ) { prompt( this.id, "Scr.AddChild(\f"+(child?child.id:null) ); }

/**
 * 
 * @memberof Scroller
 * @param {DSControl}
 */
Scroller.prototype.DestroyChild = function( child ) { prompt( this.id, "Scr.DestroyChild(\f"+(child?child.id:null) ); }

/**
 * 
 * @memberof Scroller
 * @returns {Number}
 */
Scroller.prototype.GetScrollX = function() { return parseFloat(prompt( this.id, "Scr.GetScrollX(" )); }

/**
 * 
 * @memberof Scroller
 * @returns {Number}
 */
Scroller.prototype.GetScrollY = function() { return parseFloat(prompt( this.id, "Scr.GetScrollY(" )); }

/**
 * Returns the type name.
 * @memberof Scroller
 * @returns {String}
 */
Scroller.prototype.GetType = function() { return "Scroller"; }

/**
 * 
 * @memberof Scroller
 * @param {DSControl}
 */
Scroller.prototype.RemoveChild = function( child ) { prompt( this.id, "Scr.RemoveChild(\f"+(child?child.id:null) ); }

/**
 * 
 * @memberof Scroller
 * @param {Number} x
 * @param {Number} y
 */
Scroller.prototype.ScrollBy = function( x,y ) { prompt( this.id, "Scr.ScrollBy\f"+x+"\f"+y ); }

/**
 * 
 * @memberof Scroller
 * @param {Number} x
 * @param {Number} y
 */
Scroller.prototype.ScrollTo = function( x,y ) { prompt( this.id, "Scr.ScrollTo\f"+x+"\f"+y ); }
