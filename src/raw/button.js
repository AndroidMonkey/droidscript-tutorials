
/**
 * Creates a button control.
 * @memberof DSClient
 * @constructs Button
 * @param {String} [text=""] - The button's lable.
 * @param {DecimalFraction} [width=-1] - The width of the button as a percentage of the parent container.
 * @param {DecimalFraction} [height=-1] - The height of the button as a percentage of the parent container.
 * @param {String} [options=""] -  "nosound", "link", "lego-bw", "lego-screen", "lego", "grayxs", "alum", "bar-dark", "bar-green", "bar-gray", "gray", "custom", "nopad", "trans", "fontawesome", "monospace", "html"
 * @returns {Button} The newly created button.
 * @Throws {DSError} Will throw an error if button cannot be created.
 */
DSClient.prototype.CreateButton = function( text, width, height, options ) { return new Button( text, width, height, options ); }

/**
 * Button
 * @class
 * @hideconstructor
 * @augments DSControl
 * @inheritdoc
 */
function Button( text, width, height, options ) {
    var id = prompt( "#", "App.CreateButton("+text+"\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating Button"); }
}
Button.prototype = Object.create( DSControl.prototype ); 
Button.prototype.constructor = Button; 

/**
 * Returns the text of the button.
 * @memberof Button
 * @returns {String}
 */
Button.prototype.GetText = function() { return prompt( this.id, "Btn.GetText(" ); }

/**
 * Get the button text size.
 * @memberof Button
 * @param {MeasurementUnit} [mode="sp"]
 * @returns {Number}
 * @see {@link }
 */
Button.prototype.GetTextSize = function( mode ) { return parseFloat(prompt( this.id, "Btn.GetTextSize(\f"+mode )); } 

/**
 * Get the control type.
 * @memberof Button
 * @returns {String}
 */
Button.prototype.GetType = function() { return "Button"; }

/**
 * Controls where the ellipsis (...) used to shorten the text is placed.
 * @memberof Button
 * @param {String} mode - "Start", "Middle", "End", or "Marq"
 * @see {@link Text#SetEllipsize}
 */
Button.prototype.SetEllipsize = function( mode ) { prompt( this.id, "Btn.SetEllipsize(\f"+mode ); }

/**
 * Enables or disables the button.
 * @memberof Button
 * @param {Boolean} enable
 */
Button.prototype.SetEnabled = function( enable ) { prompt( this.id, "Btn.SetEnabled(\f"+enable ); }

/**
 * Specifies a font to use.
 * @memberof Button
 * @param {String} file - /path/to/fontfile.ttf
 */
Button.prototype.SetFontFile = function( file ) { prompt( this.id, "Btn.SetFontFile(\f"+file ); }

/**
 * Sets the text of the button to a html string.
 * @memberof Button
 * @param {String} html
 */
Button.prototype.SetHtml = function( html ) { prompt( this.id, "Btn.SetHtml("+html ); }

/**
 * Sets the function to call when the button is tapped.
 * @memberof Button
 * @param {Function} callback
 */
Button.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Btn.SetOnClick("+_Cbm(callback) ); }

/**
 * Sets the function to call when the button is held down.
 * @memberof Button
 * @param {Function} callback
 */
Button.prototype.SetOnTouchEx = function( callback ) { prompt( this.id, "Btn.SetOnClick("+_Cbm(callback) ); }

/**
 * Sets the style of the button.
 * @memberof Button
 * @param {String} color1
 * @param {String} color2
 * @param {Number} radius
 * @param {String} strokeColor
 * @param {Number} strokeWidth
 * @param {Number} shadow
 */
Button.prototype.SetStyle = function( color1, color2, radius, strokeColor, strokeWidth, shadow ) { prompt( this.id, "Btn.SetStyle(\f"+color1+"\f"+color2+"\f"+radius+"\f"+strokeColor+"\f"+strokeWidth+"\f"+shadow ); }

/**
 * Sets the text of the button.
 * @memberof Button
 * @param {String}
 */
Button.prototype.SetText = function( text ) { prompt( this.id, "Btn.SetText("+text ); }

/**
 * Sets the color of the text.
 * @memberof Button
 * @param {String} color
 */
Button.prototype.SetTextColor = function( color ) { prompt( this.id, "Btn.SetTextColor("+color ); }

/**
 * Adds a shadow effect to the text.
 * @memberof Button
 * @param {Number} radius
 * @param {Number} dx
 * @param {Number} dy
 * @param {String} color
 */
Button.prototype.SetTextShadow = function( radius, dx, dy, color ) { prompt( this.id, "Btn.SetTextShadow(\f"+radius+"\f"+dx+"\f"+dy+"\f"+color ); }

/**
 * Sets the text size.
 * @memberof Button
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
Button.prototype.SetTextSize = function( size, mode ) { prompt( this.id, "Btn.SetTextSize(\f"+size+"\f"+mode ); }
