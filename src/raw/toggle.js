
/**
 * Toggle
 * @constructor
 * @param {String} text
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function Toggle( text, width, height, options ) {
    var id = prompt( "#", "App.CreateToggle("+text+"\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating Toggle"); }
}
Toggle.prototype = Object.create( DSControl.prototype ); 
Toggle.prototype.constructor = Toggle; 

/**
 * Returns if the toggle is checked or not.
 * @memberof Toggle
 * @returns {Boolean}
 */
Toggle.prototype.GetChecked = function() { return prompt( this.id, "Tgl.GetChecked(" )=="true"; }

/**
 * Returns the text contents.
 * @memberof Toggle
 * @returns {String}
 */
Toggle.prototype.GetText = function() { return prompt( this.id, "Tgl.GetText(" ); }

/**
 * Returns the size of text.
 * @memberof Toggle
 * @param {MeasurementUnit} [mode="sp"]
 * @returns {Number}
 */
Toggle.prototype.GetTextSize = function( mode ) { return parseFloat(prompt( this.id, "Tgl.GetTextSize(\f"+mode )); }

/**
 * Returns the type name.
 * @memberof Toggle
 * @returns {String}
 */
Toggle.prototype.GetType = function() { return "Toggle"; }

/**
 * 
 * @memberof Toggle
 * @param {Boolean} checked
 */
Toggle.prototype.SetChecked = function( checked ) { prompt( this.id, "Tgl.SetChecked("+checked ); }

/**
 * 
 * @memberof Toggle
 * @param {Function} callback
 */
Toggle.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Tgl.SetOnClick("+_Cbm(callback) ); }

/**
 * 
 * @memberof Toggle
 * @param {String} clr1
 * @param {String} clr2
 * @param {Number} radius
 * @param {String} strokeClr
 * @param {Number} strokeWidth
 * @param {String} shadow
 * @param {String} checkClr
 */
Toggle.prototype.SetStyle = function( clr1,clr2,radius,strokeClr,strokeWidth,shadow,checkClr ) { prompt( this.id, "Tgl.SetStyle(\f"+clr1+"\f"+clr2+"\f"+radius+"\f"+strokeClr+"\f"+strokeWidth+"\f"+shadow+"\f"+checkClr ); }

/**
 * 
 * @memberof Toggle
 * @param {String} text
 */
Toggle.prototype.SetText = function( text ) { prompt( this.id, "Tgl.SetText("+text ); }

/**
 * 
 * @memberof Toggle
 * @param {String} clr
 */
Toggle.prototype.SetTextColor = function( clr ) { prompt( this.id, "Tgl.SetTextColor("+clr ); }

/**
 * 
 * @memberof Toggle
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
Toggle.prototype.SetTextSize = function( size,mode ) { prompt( this.id, "Tgl.SetTextSize(\f"+size+"\f"+mode ); }
