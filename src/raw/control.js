
/**
 * DSControl - Class representing a visual DroidScript object or control.
 * @param {integer} id - The object's id.
 * @class
 * @hideconstructor
 * @augments DSObject
 * @inheritdoc
 */
function DSControl( id ) {
    DSObject.call( this, id );
    /** @private */
    this._left = 0;
    /** @private */
    this._top = 0; 
}
DSControl.prototype = Object.create( DSObject.prototype ); 
DSControl.prototype.constructor = DSControl; 

/**
 * Adjusts color.
 * @memberof DSControl
 * @param {Hue} hue - The hue value.
 * @param {DecimalFraction} saturation - Color saturation.
 * @param {DecimalFraction} contrast - Color contrast.
 * @param {DecimalFraction} brightness - Color brightness/lightness.
 * @example
 * //Called when application is started.
 * function OnStart()
 * {
 *   hue = 0;
 *   sat = 0;
 *   bright = 0;
 *   cont = 0;
 * 
 *   //Create a layout with objects vertically centered.
 *   lay = app.CreateLayout( "Linear", "VCenter,FillXY" );
 *   
 *   //Create a seekbar for "red" and add it to layout.
 *   txt_H = app.CreateText( "Hue: " + hue );
 *   lay.AddChild( txt_H );
 *   skb_H = app.CreateSeekBar( 0.8 );
 *   skb_H.SetRange( 360 );
 *   skb_H.SetValue( 180 );
 *   skb_H.SetOnTouch( skb_H_OnTouch );
 *   lay.AddChild( skb_H );
 * 
 *   //Create a seekbar for "red" and add it to layout.
 *   txt_S = app.CreateText( "Saturation: " + sat );
 *   lay.AddChild( txt_S );
 *   skb_S = app.CreateSeekBar( 0.8 );
 *   skb_S.SetRange( 100 );
 *   skb_S.SetValue( sat );
 *   skb_S.SetOnTouch( skb_S_OnTouch );
 *   lay.AddChild( skb_S );
 * 
 *   //Create a seekbar for "red" and add it to layout.
 *   txt_L = app.CreateText( "Brightness:" );
 *   lay.AddChild( txt_L );
 *   skb_L = app.CreateSeekBar( 0.8 );
 *   skb_L.SetRange( 100 );
 *   skb_L.SetValue( bright );
 *   skb_L.SetOnTouch( skb_L_OnTouch );
 *   lay.AddChild( skb_L );
 * 
 *   //Create a seekbar for "red" and add it to layout.
 *   txt_C = app.CreateText( "Contrast: " + cont);
 *   lay.AddChild( txt_C );
 *   skb_C = app.CreateSeekBar( 0.8 );
 *   skb_C.SetRange( 100 );
 *   skb_C.SetValue( cont );
 *   skb_C.SetOnTouch( skb_C_OnTouch );
 *   lay.AddChild( skb_C );
 *   
 *   //Create a blank text to color.
 *   txt = app.CreateText( "", 0.6, 0.3 );
 *   txt.SetBackColor( "Red" );
 *   lay.AddChild( txt );
 * 
 *   //Create a spinner to choose base color and add it to layout.
 *   spn = app.CreateSpinner( "Red,Blue,Green,White,Black,Gray" );
 *   spn.SetTextSize( 24 );
 *   spn.SetOnTouch( spn_OnTouch );
 *   lay.AddChild( spn );
 * 
 *   app.AddLayout( lay );
 * }
 * 
 * function adjustColor()
 * {
 *   txt.AdjustColor( hue, sat, bright, cont );
 *   txt.SetText("");
 * }
 * 
 * //Change the text background color to spinner item.
 * function spn_OnTouch( item )
 * {
 *     txt.SetBackColor( item );
 * }
 * 
 * function skb_H_OnTouch( value )
 * {
 *   hue = Math.round( value ) -180;
 *   txt_H.SetText( "Hue: " + hue );
 *   adjustColor()
 * }
 * 
 * function skb_S_OnTouch( value )
 * {
 *   sat = value;
 *   txt_S.SetText( "Saturation: " + sat );
 *   adjustColor();
 * }
 * 
 * function skb_L_OnTouch( value )
 * {
 *   bright = value;
 *   txt_L.SetText( "Brightness: " + bright );
 *   adjustColor();
 * }
 * 
 * function skb_C_OnTouch( value )
 * {
 *   cont = value;
 *   txt_C.SetText( "Contrast: " + cont );
 *   adjustColor();
 * }
 * @see {@link DSControl#SetBackAlpha}
 * @see {@link DSControl#SetBackColor}
 * @see {@link DSControl#SetBackGradient}
 * @see {@link DSControl#SetBackGradientRadial}
 * @since 1.33
 */
DSControl.prototype.AdjustColor = function( hue, saturation, brightness, contrast ) { prompt( this.id, "Obj.AdjustColor(\f"+hue+"\f"+saturation+"\f"+brightness+"\f"+contrast ); }

/**
 * Removes focus from the object.
 * @memberof DSControl
 * @example <caption>Example how to focus on an object, then remove that focus.</caption>
 * //Called when application is started.
 * function OnStart()
 * {
 * 	//Create a layout with objects vertically centered.
 * 	lay = app.CreateLayout( "linear", "VCenter,FillXY" );	
 * 	
 * 	//Create a text label and add it to layout.
 * 	txt = app.CreateTextEdit( "Hello" );
 * 	txt.SetTextSize( 32 );
 * 	lay.AddChild( txt );
 * 	
 * 	btn = app.CreateButton( "Focus" );
 * 	btn.SetMargins( 0.01, 0.03, 0.01, 0.01 );
 * 	btn.SetTextSize( 24 );
 * 	btn.SetOnTouch( btn_OnTouch );
 * 	lay.AddChild( btn );
 * 
 * 	//Add layout to app.	
 * 	app.AddLayout( lay );
 * }
 * 
 * function btn_OnTouch()
 * {
 *   if( btn.GetText() === "Focus" )
 *   {
 *     txt.Focus();
 *     btn.SetText( "Clear Focus" );
 *   }
 *   else
 *   {
 *     txt.ClearFocus();
 *     btn.SetText( "Focus" );
 *   }
 * }
 * @see {@link DSControl#Focus}
 */
DSControl.prototype.ClearFocus = function() { prompt(this.id,"Obj.ClearFocus(\f"); }

/**
 * Sets focus on the object.
 * @memberof DSControl
 * @see {@link DSControl#ClearFocus}
 */
DSControl.prototype.Focus = function() { prompt(this.id,"Obj.Focus(\f"); }

/**
 * Gets the absolute height of the object.
 * @memberof DSControl
 * @returns {Integer} - The height in pixels.
 * @example
 * //Called when application is started.
 * function OnStart()
 * {
 * 	//Create a layout with objects vertically centered.
 * 	lay = app.CreateLayout( "linear", "VCenter,FillXY" );	
 * 
 * 	//Create a text label and add it to layout.
 * 	txt = app.CreateText( "Hello" );
 * 	txt.SetTextSize( 32 );
 * 	lay.AddChild( txt );
 * 	
 * 	//Add layout to app.	
 * 	app.AddLayout( lay );
 * 	
 * 	//Set text to absolute height of text object.
 * 	aht = txt.GetAbsHeight();
 * 	txt.SetText( aht );
 * }
 * @see {@link DSControl#GetAbsWidth}
 * @see {@link DSControl#GetHeight}
 * @see {@link DSControl#GetWidth}
 * @see {@link DSControl#SetPosition}
 * @see {@link DSControl#SetScale}
 * @see {@link DSControl#SetSize}
 */
DSControl.prototype.GetAbsHeight = function() { return prompt( this.id, "Obj.GetAbsHeight(" ); } 

/**
 * Gets the absolute width of the object.
 * @memberof DSControl
 * @returns {Integer} - The width in pixels.
 * @see {@link DSControl#GetAbsHeight}
 * @see {@link DSControl#GetHeight}
 * @see {@link DSControl#GetWidth}
 * @see {@link DSControl#SetPosition}
 * @see {@link DSControl#SetScale}
 * @see {@link DSControl#SetSize}
 */
DSControl.prototype.GetAbsWidth = function() { return prompt( this.id, "Obj.GetAbsWidth(" ); }

/**
 * Gets the height of the object.
 * @memberof DSControl
 * @param {String} options - Use "px" for pixels, leave blank for decimal fraction.
 * @returns {Number} - Pixels or percentage of parent container.
 * @see {@link DSControl#GetAbsHeight}
 * @see {@link DSControl#GetAbsWidth}
 * @see {@link DSControl#GetWidth}
 * @see {@link DSControl#SetPosition}
 * @see {@link DSControl#SetScale}
 * @see {@link DSControl#SetSize}
 */
DSControl.prototype.GetHeight = function( options ) { return prompt( this.id, "Obj.GetHeight(\f"+options ); } 

/**
 * Gets the position of the left edge of the object.
 * @memberof DSControl
 * @param {String} options - Use "px" for pixels, leave blank for decimal fraction.
 * @returns {Number} - Pixels or percentage of parent container.
 * @example <caption>Example usage without options.</caption>
 * // returns 0.5
 * var lay1 = app.CreateLayout( "Linear","FullXY,VCenter" );
 * var lay2 = app.CreateLayout( "Linear" );
 * lay1.AddChild( lay2 )
 * app.AddLayout( lay1 );
 * lay2.GetLeft();
 * @example <caption>Example usage with options.</caption>
 * // returns 540
 * var lay1 = app.CreateLayout( "Linear","FullXY,VCenter" );
 * var lay2 = app.CreateLayout( "Linear" );
 * lay1.AddChild( lay2 )
 * app.AddLayout( lay1 );
 * lay2.GetLeft( "px");
 * @see {@link DSControl#GetPosition}
 * @see {@link DSControl#GetTop}
 */
DSControl.prototype.GetLeft = function( options ) { return prompt( this.id, "Obj.GetLeft(\f"+options ); }

/**
 * Gets the position of the object.
 * @memberof DSControl
 * @param {String} options - Use "px" for pixels, leave blank for decimal fraction.
 * @returns {Object}
 * @example <caption>Example usage without options.</caption>
 * // returns {left:0,top:0,width:1.0,height:1.0}
 * var lay = app.CreateLayout( "Linear","FullXY" );
 * app.AddLayout( lay );
 * lay.GetPosition();
 * @example <caption>Example usage with options.</caption>
 * // returns {left:0,top:0,width:1080,height:1857}
 * var lay = app.CreateLayout( "Linear","FullXY" );
 * app.AddLayout( lay );
 * lay.GetPosition( "px" );
 * @see {@link DSControl#GetLeft}
 * @see {@link DSControl#GetTop}
 */
DSControl.prototype.GetPosition = function( options ) { return eval(prompt( this.id, "Obj.GetPosition(\f"+options )); } 

/**
 * Gets the position of the top edge of the object.
 * @memberof DSControl
 * @param {String} options
 * @returns {Number}
 * @example <caption>Example usage without options.</caption>
 * // returns 0.49973074
 * var lay1 = app.CreateLayout( "Linear","FullXY,VCenter" );
 * var lay2 = app.CreateLayout( "Linear" );
 * lay1.AddChild( lay2 )
 * app.AddLayout( lay1 );
 * lay2.GetTop();
 * @example <caption>Example usage with options.</caption>
 * // returns 928
 * var lay1 = app.CreateLayout( "Linear","FullXY,VCenter" );
 * var lay2 = app.CreateLayout( "Linear" );
 * lay1.AddChild( lay2 )
 * app.AddLayout( lay1 );
 * lay2.GetTop( "px");
 * @see {@link DSControl#GetLeft}
 * @see {@link DSControl#GetPosition}
 */
DSControl.prototype.GetTop = function( options ) { return prompt( this.id, "Obj.GetTop(\f"+options ); } 

/** 
 * Returns the object's visibility mode.
 * @memberof DSControl
 * @returns {String}
 * @see {@link DSControl#SetVisibility}
 */
DSControl.prototype.GetVisibility = function() { return prompt( this.id, "Obj.GetVisibility(" ); }

/**
 * Gets the width of the object.
 * @memberof DSControl
 * @param {String} options
 * @returns {Number}
 * @see {@link DSControl#GetAbsHeight}
 * @see {@link DSControl#GetAbsWidth}
 * @see {@link DSControl#GetHeight}
 * @see {@link DSControl#SetPosition}
 * @see {@link DSControl#SetScale}
 * @see {@link DSControl#SetSize}
 */
DSControl.prototype.GetWidth = function( options ) { return prompt( this.id, "Obj.GetWidth(\f"+options ); }

/**
 * Sets the visibility mode to "Gone"; the object is hidden and the layout does not save that space.
 * @memberof DSControl
 * @see {@link DSControl#SetVisibility}
 */
DSControl.prototype.Gone = function() { prompt( this.id, "Obj.SetVisibility(Gone" ); }

/**
 * Sets the visibility mode to "Hide"; the object is hidden but the layout preserves that space.
 * @memberof DSControl
 * @see {@link DSControl#SetVisibility}
 */
DSControl.prototype.Hide = function() { prompt( this.id, "Obj.SetVisibility(Hide" ); }

/** 
 * Returns whether or not the object is enabled.
 * @memberof DSControl
 * @returns {Boolean}
 * @see {@link DSControl#SetEnabled}
 */
DSControl.prototype.IsEnabled = function() { return prompt( this.id, "Obj.IsEnabled(" )=="true"; } 

/** 
 * Returns whether or not the object is shown.
 * @memberof DSControl
 * @returns {Boolean}
 * @see {@link DSControl#SetVisibility}
 */
DSControl.prototype.IsVisible = function() { return prompt( this.id, "Obj.GetVisibility(" )=="Show"; }

/**
 * Sets the transparency of an object.
 * @memberof DSControl
 * @param {Number} alpha
 * @see {@link DSControl#AdjustColor}
 * @see {@link DSControl#SetBackColor}
 * @see {@link DSControl#SetBackGradient}
 * @see {@link DSControl#SetBackGradientRadial}
 * @see {@link DSControl#SetBackGround}
 * @see {@link DSControl#SetColorFilter}
 */
DSControl.prototype.SetBackAlpha = function( alpha ) { prompt( this.id, "Obj.SetBackAlpha(\f"+alpha ); }

/**
 * Sets the background color.
 * @memberof DSControl
 * @param {String} clr
 * @example
 * //Called when application is started.
 * function OnStart()
 * {
 *   red = 186;
 *   green = 218;
 *   blue = 85;
 * 
 *   //Create a layout with objects vertically centered.
 *   lay = app.CreateLayout( "Linear", "VCenter,FillXY" ); 
 * 
 *   //Create a seekbar for "red" and add it to layout.
 *   txt_R = app.CreateText( "Red: " + red.toString(16) );
 *   lay.AddChild( txt_R );
 *   skb_R = app.CreateSeekBar( 0.8 );
 *   skb_R.SetRange( 255 );
 *   skb_R.SetValue( red );
 *   skb_R.SetOnTouch( skb_R_OnTouch );
 *   lay.AddChild( skb_R );
 * 
 *   //Create a seekbar for "green" and add it to layout.
 *   txt_G = app.CreateText( "Green: " + green.toString(16) );
 *   lay.AddChild( txt_G );
 *   skb_G = app.CreateSeekBar( 0.8 );
 *   skb_G.SetRange( 255 );
 *   skb_G.SetValue( green );
 *   skb_G.SetOnTouch( skb_G_OnTouch );
 *   lay.AddChild( skb_G );
 * 
 *   //Create a seekbar for "blue" and add it to layout.
 *   txt_B = app.CreateText( "Blue: " + blue.toString(16) );
 *   lay.AddChild( txt_B );
 *   skb_B = app.CreateSeekBar( 0.8 );
 *   skb_B.SetRange( 255 );
 *   skb_B.SetValue( blue );
 *   skb_B.SetOnTouch( skb_B_OnTouch );
 *   lay.AddChild( skb_B );
 * 
 *   //Create a blank text to color.
 *   txt = app.CreateText( "", 0.6, 0.3 );
 *   txt.SetBackColor( rgb() );
 *   lay.AddChild( txt );
 *   
 *   //Add layout to app.
 *   app.AddLayout( lay );
 * }
 * 
 * function rgb() 
 * {
 *   return "#"
 *     + red.toString(16)
 *     + green.toString(16)
 *     + blue.toString(16)
 * }
 * 
 * function setBackColor()
 * {
 *   txt.SetBackColor( rgb() );
 * }
 * 
 * function skb_R_OnTouch( value )
 * {
 *   red = Math.round( value );
 *   txt_R.SetText( "Red: " + red.toString(16) );
 *   setBackColor();
 * }
 * 
 * function skb_G_OnTouch( value )
 * {
 *   green = Math.round( value );
 *   txt_G.SetText( "Green: " + green.toString(16) );
 *   setBackColor();
 * }
 * 
 * function skb_B_OnTouch( value )
 * {
 *   blue = Math.round( value );
 *   txt_B.SetText( "Blue: " + blue.toString(16) );
 *   setBackColor();
 * }
 * @see {@link DSControl#AdjustColor}
 * @see {@link DSControl#SetBackAlpha}
 * @see {@link DSControl#SetBackGradient}
 * @see {@link DSControl#SetBackGradientRadial}
 * @see {@link DSControl#SetBackGround}
 * @see {@link DSControl#SetColorFilter}
 */
DSControl.prototype.SetBackColor = function( clr ) { prompt( this.id, "Obj.SetBackColor(\f"+clr ); }

/**
 * Sets the background color to a gradient.
 * @memberof DSControl
 * @param {String} colour1
 * @param {String} colour2
 * @param {String} colour3
 * @param {String} options
 * @see {@link DSControl#AdjustColor}
 * @see {@link DSControl#SetBackAlpha}
 * @see {@link DSControl#SetBackColor}
 * @see {@link DSControl#SetBackGradientRadial}
 * @see {@link DSControl#SetBackGround}
 * @see {@link DSControl#SetColorFilter}
 */
DSControl.prototype.SetBackGradient = function( colour1, colour2, colour3, options ) { prompt( this.id, "Obj.SetBackGradient(Linear\f"+colour1+"\f"+colour2+"\f"+colour3+"\f"+options+"\f"+null+"\f"+null+"\f"+null ); }

/**
 * Sets the background color to a radial gradient.
 * @memberof DSControl
 * @param {Number} x
 * @param {Number} y
 * @param {Number} radius
 * @param {String} colour1
 * @param {String} colour2
 * @param {String} colour3
 * @param {String} options
 * @see {@link DSControl#AdjustColor}
 * @see {@link DSControl#SetBackAlpha}
 * @see {@link DSControl#SetBackColor}
 * @see {@link DSControl#SetBackGradient}
 * @see {@link DSControl#SetBackGround}
 * @see {@link DSControl#SetColorFilter}
 */
DSControl.prototype.SetBackGradientRadial = function( x, y, radius, colour1, colour2, colour3, options ) { prompt( this.id, "Obj.SetBackGradient(Radial\f"+x+"\f"+y+"\f"+radius+"\f"+colour1+"\f"+colour2+"\f"+colour3+"\f"+options ); }

/**
 * Sets the background of an object.
 * @memberof DSControl
 * @param {String} file
 * @param {String} options
 * @see {@link DSControl#AdjustColor}
 * @see {@link DSControl#SetBackAlpha}
 * @see {@link DSControl#SetBackColor}
 * @see {@link DSControl#SetBackGradient}
 * @see {@link DSControl#SetBackGradientRadial}
 * @see {@link DSControl#SetColorFilter}
 */
DSControl.prototype.SetBackground = function( file,options ) { prompt( this.id, "Obj.SetBackground("+file+"\f"+options ); }

/**
 * Image compositing(?)
 * @memberof DSControl
 * @param {String} clr
 * @param {string} mode
 * @see {@link DSControl#AdjustColor}
 * @see {@link DSControl#SetBackAlpha}
 * @see {@link DSControl#SetBackColor}
 * @see {@link DSControl#SetBackGradient}
 * @see {@link DSControl#SetBackGradientRadial}
 * @see {@link DSControl#SetBackGround}
	* @since 1.33
 */
DSControl.prototype.SetColorFilter = function( clr, mode ) { prompt( this.id, "Obj.SetColorFilter(\f"+clr+"\f"+mode ); }

/**
 * Enables or disables the object.
 * @memberof DSControl
 * @param {Boolean}
 * @see {@link DSControl#IsEnabled}
 */
DSControl.prototype.SetEnabled = function( enable ) { prompt( this.id, "Obj.SetEnabled(\f"+enable ); }

/**
 * Sets the margins (space) around a control.
 * @memberof DSControl
 * @param {Number} left
 * @param {Number} top
 * @param {Number} right
 * @param {Number} bottom
 * @param {String} mode - Set to "dp" or leave blank to indicate sizes are fraction of parent's size.
 * @see {@link DSControl#SetPadding}
 */
DSControl.prototype.SetMargins = function( left, top, right, bottom, mode ) { prompt( this.id, "Obj.SetMargins(\f"+left+"\f"+top+"\f"+right+"\f"+bottom+"\f"+mode ); }

/**
 * Sets the padding (space) within a control.
 * @memberof DSControl
 * @param {Number} left
 * @param {Number} top
 * @param {Number} right
 * @param {Number} bottom
 * @param {String} mode - Set to "dp" or leave blank to indicate sizes are fraction of parent's size.
 * @see {@link DSControl#SetMargins}
 */
DSControl.prototype.SetPadding = function( left, top, right, bottom, mode ) { prompt( this.id, "Obj.SetPadding(\f"+left+"\f"+top+"\f"+right+"\f"+bottom+"\f"+mode ); }

/**
 * Sets the position of the object in absolute-type [Layout]{@link Layout}
 * @memberof DSControl
 * @param {Number} left
 * @param {Number} top
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @see {@link DSControl#GetPosition}
 * @see {@link DSControl#SetSize}
 */
DSControl.prototype.SetPosition = function( left, top, width, height, options ) { prompt( this.id, "Obj.SetPosition(\f"+left+"\f"+top+"\f"+width+"\f"+height+"\f"+options ); this._left = left; this._top = top;}

/**
 * Scales the object.
 * @memberof DSControl
 * @param {Number} x - from -1.0 to 1.0
 * @param {Number} y - from -1.0 to 1.0
 * @see {@link DSControl#GetAbsHeight}
 * @see {@link DSControl#GetAbsWidth}
 * @see {@link DSControl#GetHeight}
 * @see {@link DSControl#GetWidth}
 * @see {@link DSControl#SetPosition}
 * @see {@link DSControl#SetSize}
 */
DSControl.prototype.SetScale = function( x, y ) { prompt(this.id,"Obj.SetScale(\f"+x+"\f"+y); }

/**
 * Sets the size of the object.
 * @memberof DSControl
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @see {@link DSControl#GetAbsHeight}
 * @see {@link DSControl#GetAbsWidth}
 * @see {@link DSControl#GetHeight}
 * @see {@link DSControl#GetWidth}
 * @see {@link DSControl#SetPosition}
 * @see {@link DSControl#SetScale}
 */
DSControl.prototype.SetSize = function( width, height, options ) { prompt( this.id, "Obj.SetSize(\f"+width+"\f"+height+"\f"+options ); }

/** 
 * Sets the visibility of the object.
 * @memberof DSControl
 * @param {String} mode - "Gone", "Hide", or "Show".
 * @see {@link DSControl#GetVisibility}
 * @see {@link DSControl#Gone}
 * @see {@link DSControl#Hide}
 * @see {@link DSControl#IsVisible}
 * @see {@link DSControl#Show}
 */
DSControl.prototype.SetVisibility = function( mode ) { prompt( this.id, "Obj.SetVisibility("+mode ); }

/**
 * Sets the visibility mode to "Show"; the object is shown.
 * @memberof DSControl
 * @see {@link DSControl#SetVisibility}
 */
DSControl.prototype.Show = function() { prompt( this.id, "Obj.SetVisibility(Show" ); }

/**
 * Provides tweening and animation to object.
 * @memberof DSControl
 * @param {Object} target
 * @param {Number} duration
 * @param {String} type
 * @param {Number} repeat
 * @param {Boolean} yoyo
 * @param {Function} callback
 */
DSControl.prototype.Tween = function( target, duration, type, repeat, yoyo, callback ) { _Tween.apply( this, [target,duration,type,repeat,yoyo,callback] ); }
