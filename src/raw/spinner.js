
/**
 * Spinner
 * @constructor
 * @param {String} list
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function Spinner( list, width, height, options ) {
    var id = prompt( "#", "App.CreateSpinner("+list+"\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating Spinner"); }
}
Spinner.prototype = Object.create( DSControl.prototype ); 
Spinner.prototype.constructor = Spinner; 

/**
 * 
 * @memberof Spinner
 * @param {Number} hue
 * @param {Number} sat
 * @param {Number} bright
 * @param {Number} cont
 */
Spinner.prototype.AdjustColor = function( hue,sat,bright,cont ) { prompt( this.id, "Spn.AdjustColor(\f"+hue+"\f"+sat+"\f"+bright+"\f"+cont ); }

/**
 * 
 * @memberof Spinner
 * @returns {String}
 */
Spinner.prototype.GetText = function() { return prompt( this.id, "Spn.GetText(" ); }

/**
 * Returns the text size.
 * @memberof Spinner
 * @param {MeasurementUnit} [mode="sp"]
 * @returns {Number}
 */
Spinner.prototype.GetTextSize = function( mode ) { return parseFloat(prompt( this.id, "Spn.GetTextSize(\f"+mode )); }

/**
 * Returns the type name.
 * @memberof Spinner
 * @returns {String}
 */
Spinner.prototype.GetType = function() { return "Spinner"; }

/**
 * 
 * @memberof Spinner
 * @param {String} item
 */
Spinner.prototype.SelectItem = function( item ) { prompt( this.id, "Spn.SetText("+item ); }

/**
 * 
 * @memberof Spinner
 * @param {String} list
 * @param {String} delim
 */
Spinner.prototype.SetList = function( list,delim ) { prompt(this.id, "Spn.SetList(\f"+list+"\f"+delim); }

/**
 * 
 * @memberof Spinner
 * @param {Function} callback
 */
Spinner.prototype.SetOnChange = function( callback ) { prompt( this.id, "Spn.SetOnClick("+_Cbm(callback) ); }

/**
 * 
 * @memberof Spinner
 * @param {Function} callback
 */
Spinner.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Spn.SetOnClick("+_Cbm(callback) ); }

/**
 * 
 * @memberof Spinner
 * @param {String} txt
 */
Spinner.prototype.SetText = function( txt ) { prompt( this.id, "Spn.SetText("+txt ); }

/**
 * 
 * @memberof Spinner
 * @param {String} clr
 */
Spinner.prototype.SetTextColor = function( clr ) { prompt( this.id, "Spn.SetTextColor("+clr ); }

/**
 * 
 * @memberof Spinner
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
Spinner.prototype.SetTextSize = function( size,mode ) { prompt( this.id, "Spn.SetTextSize(\f"+size+"\f"+mode ); }
