
/** 
 * Global vars, currently affixes to root (window or global) object.
 @todo Eliminate as many global vars as possible. */

(function() {
    /** Is Debug enabled?
     * @type {Boolean}
     * @global 
     * @private
     * @see {@link DSClient#SetDebugEnabled}
     */
    this._dbg = true;

    /** 
     * Array of DSObject ids.
     * @type {String[]}
     * @global 
     * @private
     */
    this._map = [];

    /** 
     * Array of loaded scripts' URLs.
     * @type {String[]}
     * @global 
     * @private
     * @see {@link DSClient#LoadScript}
     */
    this._scripts = [];

    /** 
     * Translations for DroidScript app strings.
     * @type {Object}
     * @global 
     * @private
     * @see {@link DSClient#Language2Code}
     * @see {@link DSClient#SetLanguage}
     */
    this._languages = null;

    /** 
     * Current language code, default is "en".
     * @type {String}
     * @global 
     * @private
     * @see {@link DSClient#Language2Code}
     * @see {@link DSClient#SetLanguage}
     */
    this._curLang = "en";

    /** 
     * Array of callback functions.
     * @type {String[]}
     * @global 
     * @private
     */
    this._cbMap = [];

    /** 
     * Callback map counter/pointer.
     * @type {Integer}
     * @global 
     * @private
     */
    this._cbId = 0;

    /** 
     * Documentation is open?
     * @type {Boolean}
     * @global 
     * @private
     */
    this._docs = false;

    /** 
     * Is code being _Run?
     * @type {Boolean}
     * @global 
     * @private
     */
    this._busy=false;

    /** 
     * Is engine Chrome V8?
     * @type {Boolean}
     * @global 
     * @private
     */
    this._isV8=false;

    /** 
     * BluetoothList singleton pointer.
     * @type {DSControl}
     * @global 
     * @private
     */
    this._btl = null;

    /** 
     * ListView singleton pointer.
     * @type {DSControl}
     * @global 
     * @private
     */
    this._lvw = null;

    /** 
     * ListDialog singleton pointer.
     * @type {DSControl}
     * @global 
     * @private
     */
    this._ldg = null;

    /** 
     * YesNoDialog singleton pointer.
     * @type {DSControl}
     * @global 
     * @private
     */
    this._ynd = null;

    /** 
     * NXT singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._nxt = null;

    /** 
     * SmartWatch singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._smw = null;

    /** 
     * NXT Info singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._inf = null;

    /** 
     * AudioRecorder singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._rec = null;

    /** 
     * PhoneState singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._pst = null;

    /** 
     * SMS singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._sms = null;

    /** 
     * Email singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._eml = null;

    /** 
     * Wallpaper singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._wpr = null;

    /** 
     * Crypt singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._crp = null;

    /** 
     * SpeechRec singleton pointer.
     * @type {DSObject}
     * @global 
     * @private
     */
    this._spr = null;

}).apply(this);
