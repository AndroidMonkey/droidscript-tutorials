
/**
 * List
 * @constructor
 * @param {String} list
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function List( list, width, height, options ) {
    var id = prompt( "#", "App.CreateList("+list+"\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating List"); }
}
List.prototype = Object.create( DSControl.prototype ); 
List.prototype.constructor = List; 

/**
 * 
 * @memberof List
 * @param {Number} hue
 * @param {Number} sat
 * @param {Number} bright
 * @param {Number} cont
 */
List.prototype.AdjustColor = function( hue,sat,bright,cont ) { prompt( this.id, "Lst.AdjustColor(\f"+hue+"\f"+sat+"\f"+bright+"\f"+cont ); }

/**
 * 
 * @memberof List
 * @param {String} title
 * @param {String} body
 * @param {String} image
 */
List.prototype.AddItem = function( title,body,image ) { prompt( this.id, "Lst.AddItem(\f"+title+"\f"+body+"\f"+image ); }

/**
 * 
 * @memberof List
 * @param {String} title
 * @returns {String}
 */
List.prototype.GetItem = function( title ) { var p="Lst.GetItem(\f"+title; return eval(prompt(this.id,p)); }

/**
 * 
 * @memberof List
 * @param {String} index
 * @returns {String}
 */
List.prototype.GetItemByIndex = function( index ) { var p="Lst.GetItemByIndex(\f"+index; return eval(prompt(this.id,p)); }

/**
 * 
 * @memberof List
 * @returns {Number}
 */
List.prototype.GetLength = function() { return parseInt(prompt(this.id,"Lst.GetLength(")); }

/**
 * 
 * @memberof List
 * @param {String} delim
 * @returns {String}
 */
List.prototype.GetList = function( delim ) { return eval(prompt( this.id, "Lst.GetList("+delim )); }

/**
 * Returns the size of text.
 * @memberof List
 * @param {MeasurementUnit} [mode="sp"]
 * @returns {Number}
 */
List.prototype.GetTextSize = function( mode ) { return parseFloat(prompt( this.id, "Lst.GetTextSize(\f"+mode )); }

/**
 * Returns the type name.
 * @memberof List
 * @returns {String}
 */
List.prototype.GetType = function() { return "List"; }

/**
 * 
 * @memberof List
 * @param {Number} index
 * @param {String} title
 * @param {String} body
 * @param {String} image
 */
List.prototype.InsertItem = function( index,title,body,image ) { prompt( this.id, "Lst.InsertItem(\f"+index+"\f"+title+"\f"+body+"\f"+image ); }

/**
 * 
 * @memberof List
 */
List.prototype.RemoveAll = function() { prompt( this.id, "Lst.RemoveAll(" ); }

/**
 * 
 * @memberof List
 * @param {String} title
 */
List.prototype.RemoveItem = function( title ) { prompt( this.id, "Lst.RemoveItem(\f"+title ); }

/**
 * 
 * @memberof List
 * @param {Number} index
 */
List.prototype.RemoveItemByIndex = function( index ) { prompt( this.id, "Lst.RemoveItemByIndex(\f"+index ); }

/**
 * 
 * @memberof List
 * @param {String} title
 * @param {String} body
 */
List.prototype.ScrollToItem = function( title,body ) { var p="Lst.ScrollToItem(\f"+title+"\f"+body; prompt(this.id,p); }

/**
 * 
 * @memberof List
 * @param {Number} index
 */
List.prototype.ScrollToItemByIndex = function( index ) { var p="Lst.ScrollToItemByIndex(\f"+index; prompt(this.id,p); }

/**
 * 
 * @memberof List
 * @param {String} title
 * @param {String} body
 * @param {Boolean} scroll
 */
List.prototype.SelectItem = function( title,body,scroll ) { var p="Lst.SelectItem(\f"+title+"\f"+body+"\f"+scroll; prompt(this.id,p); }

/**
 * 
 * @memberof List
 * @param {Number} index
 * @param {Boolean} scroll
 */
List.prototype.SelectItemByIndex = function( index,scroll ) { var p="Lst.SelectItemByIndex(\f"+index+"\f"+scroll; prompt(this.id,p); }

/**
 * 
 * @memberof List
 * @param {String} icon
 * @param {String} title
 * @param {String} body
 * @param {String} mode
 */
List.prototype.SetColumnWidths = function( icon,title,body,mode ) { prompt( this.id, "Lst.SetColumnWidths(\f"+icon+"\f"+title+"\f"+body+"\f"+mode ); }

/**
 * 
 * @memberof List
 * @param {Number} height
 * @param {String} color
 */
List.prototype.SetDivider = function( height,color ) { prompt( this.id, "Lst.SetDivider(\f"+height+"\f"+color ); }

/**
 * 
 * @memberof List
 * @param {String} mode
 */
List.prototype.SetEllipsize = function( mode ) { prompt( this.id, "Lst.SetEllipsize1(\f"+mode ); }

/**
 * 
 * @memberof List
 * @param {String} mode
 */
List.prototype.SetEllipsize1 = function( mode ) { prompt( this.id, "Lst.SetEllipsize1(\f"+mode ); }

/**
 * 
 * @memberof List
 * @param {String} mode
 */
List.prototype.SetEllipsize2 = function( mode ) { prompt( this.id, "Lst.SetEllipsize2(\f"+mode ); }

/**
 * 
 * @memberof List
 * @param {String} file
 */
List.prototype.SetFontFile = function( file ) { prompt( this.id, "Lst.SetFontFile(\f"+file ); }

/**
 * 
 * @memberof List
 * @param {String} clr
 */
List.prototype.SetHiTextColor1 = function( clr ) { prompt( this.id, "Lst.SetHiTextColor1("+clr ); }

/**
 * 
 * @memberof List
 * @param {String} clr
 */
List.prototype.SetHiTextColor2 = function( clr ) { prompt( this.id, "Lst.SetHiTextColor2("+clr ); }

/**
 * 
 * @memberof List
 * @param {Number} left
 * @param {Number} top
 * @param {Number} right
 * @param {Number} bottom
 * @param {String} mode
 */
List.prototype.SetIconMargins = function( left,top,right,bottom,mode ) { prompt( this.id, "Lst.SetIconMargins(\f"+left+"\f"+top+"\f"+right+"\f"+bottom+"\f"+mode ); }

/**
 * 
 * @memberof List
 * @param {Number} size
 * @param {String} mode
 */
List.prototype.SetIconSize = function( size,mode ) { prompt( this.id, "Lst.SetIconSize(\f"+size+"\f"+mode ); }

/**
 * 
 * @memberof List
 * @param {String} title
 * @param {String} newTitle
 * @param {String} newBody
 * @param {String} newImage
 */
List.prototype.SetItem = function( title,newTitle,newBody,newImage ) { prompt( this.id, "Lst.SetItem(\f"+title+"\f"+newTitle+"\f"+newBody+"\f"+newImage ); }

/**
 * 
 * @memberof List
 * @param {Number} index
 * @param {String} newTitle
 * @param {String} newBody
 * @param {String} newImage
 */
List.prototype.SetItemByIndex = function( index,newTitle,newBody,newImage ) { prompt( this.id, "Lst.SetItemByIndex(\f"+index+"\f"+newTitle+"\f"+newBody+"\f"+newImage ); }

/**
 * 
 * @memberof List
 * @param {String} list
 * @param {String} delim
 */
List.prototype.SetList = function( list,delim ) { prompt( this.id, "Lst.SetList(\f"+list+"\f"+delim ); }

/**
 * 
 * @memberof List
 * @param {Function} callback
 */
List.prototype.SetOnLongTouch = function( callback ) { prompt( this.id, "Lst.SetOnLongClick("+_Cbm(callback) ); }

/**
 * 
 * @memberof List
 * @param {Function} callback
 */
List.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Lst.SetOnClick("+_Cbm(callback) ); }

/**
 * 
 * @memberof List
 * @param {String} clr
 */
List.prototype.SetTextColor = function( clr ) { prompt( this.id, "Lst.SetTextColor1("+clr ); }

/**
 * 
 * @memberof List
 * @param {String} clr
 */
List.prototype.SetTextColor1 = function( clr ) { prompt( this.id, "Lst.SetTextColor1("+clr ); }

/**
 * 
 * @memberof List
 * @param {String} clr
 */
List.prototype.SetTextColor2 = function( clr ) { prompt( this.id, "Lst.SetTextColor2("+clr ); }

/**
 * 
 * @memberof List
 * @param {Number} left
 * @param {Number} top
 * @param {Number} right
 * @param {Number} bottom
 * @param {String} mode
 * @param {String} options
 */
List.prototype.SetTextMargins = function( left,top,right,bottom,mode,options ) { prompt( this.id, "Lst.SetTextMargins(\f"+left+"\f"+top+"\f"+right+"\f"+bottom+"\f"+mode+"\f"+options ); }

/**
 * 
 * @memberof List
 * @param {Number} radius
 * @param {Number} dx
 * @param {Number} dy
 * @param {String} color
 */
List.prototype.SetTextShadow = function( radius,dx,dy,color ) { prompt( this.id, "Lst.SetTextShadow1(\f"+radius+"\f"+dx+"\f"+dy+"\f"+color ); }

/**
 * 
 * @memberof List
 * @param {Number} radius
 * @param {Number} dx
 * @param {Number} dy
 * @param {String} color
 */
List.prototype.SetTextShadow1 = function( radius,dx,dy,color ) { prompt( this.id, "Lst.SetTextShadow1(\f"+radius+"\f"+dx+"\f"+dy+"\f"+color ); }

/**
 * 
 * @memberof List
 * @param {Number} radius
 * @param {Number} dx
 * @param {Number} dy
 * @param {String} color
 */
List.prototype.SetTextShadow2 = function( radius,dx,dy,color ) { prompt( this.id, "Lst.SetTextShadow2(\f"+radius+"\f"+dx+"\f"+dy+"\f"+color ); }

/**
 * 
 * @memberof List
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
List.prototype.SetTextSize = function( size,mode ) { prompt( this.id, "Lst.SetTextSize1(\f"+size+"\f"+mode ); }

/**
 * 
 * @memberof List
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
List.prototype.SetTextSize1 = function( size,mode ) { prompt( this.id, "Lst.SetTextSize1(\f"+size+"\f"+mode ); }

/**
 * 
 * @memberof List
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
List.prototype.SetTextSize2 = function( size,mode ) { prompt( this.id, "Lst.SetTextSize2(\f"+size+"\f"+mode ); }
