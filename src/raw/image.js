
/**
 * Image
 * @constructor
 * @param {String} file
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @param {Number} w
 * @param {Number} h
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function Image( file, width, height, options, w, h ) {
    var id = prompt( "#", "App.CreateImage("+file+"\f"+width+"\f"+height+"\f"+options+"\f"+w+"\f"+h );
    if ( id ) { 
        DSControl.call( this, id ); 
        this._auto = true; 
        this._gfb = "";
    }
    else {  throw new DSError("Error creating Image"); }
}
Image.prototype = Object.create( DSControl.prototype ); 
Image.prototype.constructor = Image; 

/**
 * 
 * @memberof Image
 * @param {Number} hue
 * @param {Number} sat
 * @param {Number} bright
 * @param {Number} cont
 */
Image.prototype.AdjustColor = function( hue,sat,bright,cont ) { prompt( this.id, "Img.AdjustColor(\f"+hue+"\f"+sat+"\f"+bright+"\f"+cont ); }

/**
 * 
 * @memberof Image
 */
Image.prototype.Clear = function() { if( this._auto ) prompt( this.id, "Img.Clear(" ); else { this.Draw("c"); } }

/**
 * 
 * @memberof Image
 * @param {String} func
 * @param {Any} p1
 * @param {Number} p2
 * @param {Number} p3
 * @param {Number} p4
 * @param {Number} p5
 * @param {Number} p6
 * @param {Number} p7
 */
Image.prototype.Draw = function( func, p1, p2, p3, p4, p5, p6, p7 ) {
		if( this._gfb.length > 2 ) this._gfb += "\f";
		this._gfb += func + "¬" + p1 + "¬" + p2 + "¬" + 
p3 + "¬" + p4 + "¬" + p5 + "¬" + p6 + "¬" + p7;
	}

/**
 * 
 * @memberof Image
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 * @param {Number} start
 * @param {Number} sweep
 */
Image.prototype.DrawArc = function( x1,y1,x2,y2,start,sweep ) { 
		if( this._auto ) prompt( this.id, "Img.DrawArc("+x1+"\f"+y1+"\f"+x2+"\f"+y2+"\f"+start+"\f"+sweep );
		else this.Draw( "a", null, x1,y1,x2,y2,start,sweep ); }

/**
 * 
 * @memberof Image
 * @param {Number} x
 * @param {Number} y
 * @param {Number} radius
 */
Image.prototype.DrawCircle = function( x,y,radius ) { 
		if( this._auto ) prompt( this.id, "Img.DrawCircle("+x+"\f"+y+"\f"+radius );
		else this.Draw( "e", null, x,y,radius ); }

/**
 * 
 * @memberof Image
 * @param {Number} ms
 */
Image.prototype.DrawFrame = function( ms ) { prompt( this.id, "Img.DrawFrame\f"+ms ); }

/**
 * 
 * @memberof Image
 * @param {String} image
 * @param {Number} x
 * @param {Number} y
 * @param {Number} w
 * @param {Number} h
 * @param {Number} angle
 * @param {String} mode
 */
Image.prototype.DrawImage = function( image,x,y,w,h,angle,mode ) { 
		if( this._auto ) prompt( this.id, "Img.DrawImage\f"+(image?image.id:null)+"\f"+x+"\f"+y+"\f"+w+"\f"+h+"\f"+angle+"\f"+mode ); 
		else this.Draw( "i", (image?image.id:null), x,y,(w?w:-1),(h?h:-1),angle,mode ); }

/**
 * 
 * @memberof Image
 * @param {String} image 
 * @param {String} matrix
 */
Image.prototype.DrawImageMtx = function( image,matrix ) { 
		if( this._auto ) prompt( this.id, "Img.DrawImageMtx\f"+(image?image.id:null)+"\f"+matrix ); 
		else this.Draw( "m", (image?image.id:null), matrix ); }

/**
 * 
 * @memberof Image
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 */
Image.prototype.DrawLine = function( x1,y1,x2,y2 ) { 
		if( this._auto ) prompt( this.id, "Img.DrawLine("+x1+"\f"+y1+"\f"+x2+"\f"+y2 ); 
		else this.Draw( "l", null, x1,y1,x2,y2 ); }

/**
 * 
 * @memberof Image
 * @param {Number} x
 * @param {Number} y
 */
Image.prototype.DrawPoint = function( x,y ) { 
		if( this._auto ) prompt( this.id, "Img.DrawPoint("+x+"\f"+y ); else this.Draw( "p", null, x,y ); }

/**
 * 
 * @memberof Image
 * @param {Number} x1
 * @param {Number} y1
 * @param {Number} x2
 * @param {Number} y2
 */
Image.prototype.DrawRectangle = function( x1,y1,x2,y2 ) { 
		if( this._auto ) prompt( this.id, "Img.DrawRect("+x1+"\f"+y1+"\f"+x2+"\f"+y2 );
		else this.Draw( "r", null, x1,y1,x2,y2 ); }

/**
 * 
 * @memberof Image
 * @param {Number} data
 * @param {Number} range
 */
Image.prototype.DrawSamples = function( data,range ) { 
		if( this._auto ) prompt( this.id, "Img.DrawSamples(\f"+data+"\f"+range ); 
		else this.Draw( "g", data, range, 0,0,0,0 ); }

/**
 * 
 * @memberof Image
 * @param {String} y
 * @param {Number} x
 * @param {Number} y
 */
Image.prototype.DrawText = function( txt,x,y ) { 
		if( this._auto ) prompt( this.id, "Img.DrawText("+txt+"\f"+x+"\f"+y ); 
		else this.Draw( "t", txt, x, y, 0,0,0 ); }

/**
 * 
 * @memberof Image
 */
Image.prototype.Flatten = function() { prompt( this.id, "Img.Flatten(" ); }

/**
 * Returns the 
 * @memberof Image
 * @returns {Number}
 */
Image.prototype.GetAbsHeight = function() { return parseFloat(prompt( this.id, "Img.GetAbsHeight(" )); }

/**
 * Returns the 
 * @memberof Image
 * @returns {Number}
 */
Image.prototype.GetAbsWidth = function() { return parseFloat(prompt( this.id, "Img.GetAbsWidth(" )); }

/**
 * Returns the 
 * @memberof Image
 * @returns {Number}
 */
Image.prototype.GetHeight = function() { return parseFloat(prompt( this.id, "Img.GetHeight(" )); }

/**
 * Returns the 
 * @memberof Image
 * @returns {Number}
 */
Image.prototype.GetName = function() { return prompt( this.id, "Img.GetName(" ); }

/**
 * Returns the 
 * @memberof Image
 * @returns {Number}
 */
Image.prototype.GetPixelColor = function( x,y ) { return eval(prompt( this.id, "Img.GetPixelColor(\f"+x+"\f"+y )); }

/**
 * Returns the 
 * @memberof Image
 * @returns {String}
 */
Image.prototype.GetPixelData = function( format,left,top,width,height ) { return prompt( this.id, "Img.GetPixelData(\f"+format+"\f"+left+"\f"+top+"\f"+width+"\f"+height ); }

/**
 * Returns the 
 * @memberof Image
 * @returns {String}
 */
Image.prototype.GetType = function() { return "Image"; }

/**
 * Returns the 
 * @memberof Image
 * @returns {String}
 */
Image.prototype.GetWidth = function() { return parseFloat(prompt( this.id, "Img.GetWidth(" )); }

/**
 * 
 * @memberof Image
 * @param {String} txt
 * @returns {Number}
 */
Image.prototype.MeasureText = function( txt ) { return eval(prompt( this.id, "Img.MeasureText(\f"+txt)); }

/**
 * 
 * @memberof Image
 * @param {Number} x
 * @param {Number} y
 */
Image.prototype.Move = function( x,y ) { prompt( this.id, "Img.Move("+x+"\f"+y ); }

/**
 * 
 * @memberof Image
 */
Image.prototype.Reset = function() { prompt( this.id, "Img.Reset(" ); }

/**
 * 
 * @memberof Image
 * @param {Number} angle
 * @param {Number} pivX
 * @param {Number} pivY
 */
Image.prototype.Rotate = function( angle,pivX,pivY ) { prompt( this.id, "Img.Rotate("+angle+"\f"+pivX+"\f"+pivY ); }

/**
 * 
 * @memberof Image
 * @param {String} fileName
 * @param {String} quality
 */
Image.prototype.Save = function( fileName,quality ) { prompt( this.id, "Img.Save\f"+fileName+"\f"+quality ); }

/**
 * 
 * @memberof Image
 * @param {Number} x
 * @param {Number} y
 */
Image.prototype.Scale = function( x,y ) { prompt( this.id, "Img.Scale("+x+"\f"+y ); }

/**
 * 
 * @memberof Image
 * @param {Number} alpha
 */
Image.prototype.SetAlpha = function( alpha ) { if( this._auto ) prompt( this.id, "Img.SetAlpha("+alpha ); else this.Draw( "k",null,alpha ); }

/**
 * 
 * @memberof Image
 * @param {Boolean} onoff
 */
Image.prototype.SetAutoUpdate = function( onoff ) { this._auto=onoff; prompt( this.id, "Img.SetAutoUpdate(\f"+onoff ); }

/**
 * 
 * @memberof Image
 * @param {Number} clr
 */
Image.prototype.SetColor = function( clr ) { if( this._auto ) prompt( this.id, "Img.SetColor("+clr ); else this.Draw( "o", clr ); }

/**
 * 
 * @memberof Image
 * @param {String} clr
 * @param {String} mode
 */
Image.prototype.SetColorFilter = function( clr,mode ) { prompt( this.id, "Img.SetColorFilter(\f"+clr+"\f"+mode ); }

/**
 * 
 * @memberof Image
 * @param {String} file
 */
Image.prototype.SetFontFile = function( file ) { if( this._auto ) prompt( this.id, "Img.SetFontFile(\f"+file ); else this.Draw( "f",file ); }

/**
 * 
 * @memberof Image
 * @param {String} image
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 */
Image.prototype.SetImage = function( image,width,height,options ) { 
		if( typeof image=="string" ) prompt( this.id, "Img.LoadImage(\f"+image+"\f"+width+"\f"+height+"\f"+options ); 
		else prompt( this.id, "Img.CopyImage(\f"+(image?image.id:null)+"\f"+width+"\f"+height+"\f"+options );

/**
 * 
 * @memberof Image
 * @param {Number} width
 */
Image.prototype.SetLineWidth = function( width ) { if( this._auto ) prompt( this.id, "Img.SetLineWidth("+width ); else this.Draw( "w",null,width ); }

/**
 * 
 * @memberof Image
 * @param {Number} ms
 */
Image.prototype.SetMaxRate = function( ms ) { prompt( this.id, "Img.SetMaxRate("+ms ); }

/**
 * 
 * @memberof Image
 * @param {String} name
 */
Image.prototype.SetName = function( name ) { prompt( this.id, "Img.SetName("+name ); }

/**
 * 
 * @memberof Image
 * @param {Function} callback
 */
Image.prototype.SetOnLoad = function( callback ) { prompt( this.id, "Img.SetOnLoad\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof Image
 * @param {Function} callback
 */
Image.prototype.SetOnLongTouch = function( callback ) { prompt( this.id, "Img.SetOnLongTouch("+_Cbm(callback) ); }

/**
 * 
 * @memberof Image
 * @param {Function} callback
 */
Image.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Img.SetOnTouch("+_Cbm(callback) ); }

/**
 * 
 * @memberof Image
 * @param {Function} callback
 */
Image.prototype.SetOnTouchDown = function( callback ) { prompt( this.id, "Img.SetOnTouchDown("+_Cbm(callback) ); }

/**
 * 
 * @memberof Image
 * @param {Function} callback
 */
Image.prototype.SetOnTouchMove = function( callback ) { prompt( this.id, "Img.SetOnTouchMove("+_Cbm(callback) ); }

/**
 * 
 * @memberof Image
 * @param {Function} callback
 */
Image.prototype.SetOnTouchUp = function( callback ) { prompt( this.id, "Img.SetOnTouchUp("+_Cbm(callback) ); }

/**
 * 
 * @memberof Image
 * @param {Number} clr
 */
Image.prototype.SetPaintColor = function( clr ) { if( this._auto ) prompt( this.id, "Img.SetPaintColor("+clr ); else this.Draw( "n",clr ); }

/**
 * 
 * @memberof Image
 * @param {String} style
 */
Image.prototype.SetPaintStyle = function( style ) { if( this._auto ) prompt( this.id, "Img.SetPaintStyle("+style ); else this.Draw( "s",style ); }

/**
 * 
 * @memberof Image
 * @param {String} data
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 */
Image.prototype.SetPixelData = function( data,width,height,options ) { return prompt( this.id, "Img.SetPixelData(\f"+data+"\f"+width+"\f"+height+"\f"+options ); }

/**
 * 
 * @memberof Image
 * @param {Boolean} onoff
 */
Image.prototype.SetPixelMode = function( onoff ) { prompt( this.id, "Img.SetPixelMode(\f"+onoff ); }

/**
 * 
 * @memberof Image
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 */
Image.prototype.SetSize = function( width,height,options ) { prompt( this.id, "Img.SetSize(\f"+width+"\f"+height+"\f"+options ); }

/**
 * 
 * @memberof Image
 * @param {Number} size
 */
Image.prototype.SetTextSize = function( size ) { if( this._auto ) prompt( this.id, "Img.SetTextSize("+size ); else this.Draw( "x",null,size ); }}

/**
 * 
 * @memberof Image
 * @param {Boolean} touchable
 */
Image.prototype.SetTouchable = function( touchable ) { prompt( this.id, "Img.SetTouchable("+touchable ); }

/**
 * 
 * @memberof Image
 * @param {Number} x
 * @param {Number} y
 */
Image.prototype.Skew = function( x,y ) { prompt( this.id, "Img.Skew("+x+"\f"+y ); }

/**
 * 
 * @memberof Image
 * @param {String} matrix
 */
Image.prototype.Transform = function( matrix ) { prompt( this.id, "Img.Transform(\f"+matrix ); }

/**
 * 
 * @memberof Image
 */
Image.prototype.Update = function() { if( this._auto ) prompt( this.id, "Img.Update(" ); else { prompt( this.id, "Img.Batch("+this._gfb ); this._gfb = ""; } }
