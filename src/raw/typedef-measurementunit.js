
/**
 * A string containing a measurement unit: 
 *   "dip", "dp" - Density Independant Pixel
 *     (dp = (width in pixels * 160) / screen density), 
 *   "mm" - Millimeter, 
 *   "pt" - Point, 
 *   "px" - Pixel, or 
 *   "sp" - Scale-independant Pixel.
 * @typedef {String} MeasurementUnit
 */
