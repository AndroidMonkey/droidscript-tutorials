
/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} value
 * @param {String} file
 */
DSClient.prototype.SaveBoolean = function ( name,value,file ) { prompt( "#", "App.SaveBoolean("+name+"\f"+value+"\f"+file ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.SaveCookies = function () { prompt( "#", "App.SaveCookies(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} value
 * @param {String} file
 */
DSClient.prototype.SaveNumber = function ( name,value,file ) { prompt( "#", "App.SaveNumber("+name+"\f"+value+"\f"+file ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} value
 * @param {String} file
 */
DSClient.prototype.SaveText = function ( name,value,file ) { prompt( "#", "App.SaveText("+name+"\f"+value+"\f"+file ); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 */
DSClient.prototype.ScanFile = function ( file ) { prompt( "#", "App.ScanFile(\f"+file); }

/**
 * 
 * @memberof DSClient
 * @param {String} fileName
 * @param {String} quality
 */
DSClient.prototype.ScreenShot = function ( fileName,quality ) { prompt( "#", "App.ScreenShot\f"+fileName+"\f"+quality ); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @param {String} subject
 * @param {String} text
 * @param {String} choose
 */
DSClient.prototype.SendFile = function ( file,subject,text,choose ) { prompt( "#", "App.SendFile(\f"+file+"\f"+subject+"\f"+text+"\f"+choose ); }

/**
 * 
 * @memberof DSClient
 * @param {String} packageName
 * @param {String} className
 * @param {String} action
 * @param {String} category
 * @param {String} uri
 * @param {String} type
 * @param {String} extras
 * @param {String} options
 * @param {String} callback
 */
DSClient.prototype.SendIntent = function ( packageName,className,action,category,uri,type,extras,options,callback ) { prompt( "#", "App.SendIntent(\f"+packageName+"\f"+className+"\f"+action+"\f"+category+"\f"+uri+"\f"+type+"\f"+extras+"\f"+options+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} address
 * @param {String} subject
 * @param {String} body
 * @param {String} attach
 * @param {String} type
 * @param {String} options
 */
DSClient.prototype.SendMail = function ( address,subject,body,attach,type,options ) { prompt( "#", "App.SendMail(\f"+address+"\f"+subject+"\f"+body+"\f"+attach+"\f"+type+"\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {String} msg
 */
DSClient.prototype.SendMessage = function ( msg ) { prompt( "#", "App.SendMessage(\f"+msg ); }

/**
 * 
 * @memberof DSClient
 * @param {String} text
 * @param {String} subject
 * @param {String} choose
 */
DSClient.prototype.SendText = function ( text,subject,choose ) { prompt( "#", "App.SendText(\f"+text+"\f"+subject+"\f"+choose ); }

/**
 * 
 * @memberof DSClient
 * @param {String} type
 * @param {String} id
 * @param {String} callback
 * @param {String} time
 * @param {String} interval
 * @param {String} options
 */
DSClient.prototype.SetAlarm = function ( type,id,callback,time,interval,options ) { return prompt( "#", "App.SetAlarm(\f"+type+"\f"+id+"\f"+_Cbm(callback)+"\f"+time+"\f"+interval+"\f"+options); }

/**
 * 
 * @memberof DSClient
 * @param {String} auto
 */
DSClient.prototype.SetAutoBoot = function ( auto ) { prompt( "#", "App.SetAutoBoot(\f"+auto); }

/**
 * 
 * @memberof DSClient
 * @param {String} auto
 */
DSClient.prototype.SetAutoStart = function ( appName ) { prompt( "#", "App.SetAutoStart(\f"+appName); }

/**
 * 
 * @memberof DSClient
 * @param {String} auto
 */
DSClient.prototype.SetAutoWifi = function ( auto ) { prompt( "#", "App.SetAutoWifi(\f"+auto); }

/**
 * 
 * @memberof DSClient
 * @param {String} auto
 */
DSClient.prototype.SetBackColor = function ( clr ) { prompt( "#", "App.SetBackColor(\f"+clr ); }

/**
 * 
 * @memberof DSClient
 * @param {String} auto
 */
DSClient.prototype.SetBluetoothEnabled = function ( enable ) { prompt( "#", "App.SetBluetoothEnabled(\f"+enable ); }

/**
 * 
 * @memberof DSClient
 * @param {String} txt
 */
DSClient.prototype.SetClipboardText = function ( txt ) { prompt( "#", "App.SetClipboardText("+txt ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} value
 */
DSClient.prototype.SetData = function ( name,value ) { prompt( "#", "App.SetData(\f"+name+"\f"+value ); }

/**
 * 
 * @memberof DSClient
 * @param {String} enable
 */
DSClient.prototype.SetDebugEnabled = function ( enable ) { prompt( "#", "App.SetDebugEnabled("+enable ); _dbg=enable; }

/**
 * 
 * @memberof DSClient
 * @param {String} dpi
 */
DSClient.prototype.SetDensity = function ( dpi ) { prompt( "#", "App.SetDensity(\f"+dpi ); }

/**
 * 
 * @memberof DSClient
 * @param {String} options
 */
DSClient.prototype.SetJoystickOptions = function ( options ) { prompt( "#", "App.SetJoystickOptions(\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {String} mode
 * @param {String} enable
 * @param {String} options
 */
DSClient.prototype.SetKioskMode = function ( mode,enable,options ) { prompt( "#", "App.SetKioskMode(\f"+mode+"\f"+enable+"\f"+options); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 */
DSClient.prototype.SetLanguage = function ( name ) { 
		var json = app.ReadFile( "lang.json" );
		_languages = JSON.parse(json);
		_curLang = _languages.codes[name.toLowerCase()];
	}

/**
 * 
 * @memberof DSClient
 * @param {String} list
 * @param {String} iconPath
 */
DSClient.prototype.SetMenu = function ( list,iconPath ) { prompt( "#", "App.SetMenu("+list+"\f"+iconPath ); }

/**
 * 
 * @memberof DSClient
 * @param {Function} callback
 */
DSClient.prototype.SetOnBroadcast = function ( callback ) { prompt( "#", "App.SetOnBroadcast("+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {Function} callback
 */
DSClient.prototype.SetOnDebug = function ( callback ) { prompt( "#", "App.SetOnDebug(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {Function} callback
 */
DSClient.prototype.SetOnError = function ( callback ) { prompt( "#", "App.SetOnError(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {Function} callback
 */
DSClient.prototype.SetOnKey = function ( callback ) { prompt( "#", "App.SetOnKey(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {Function} callback
 */
DSClient.prototype.SetOnShowKeyboard = function ( callback ) { prompt( "#", "App.SetOnShowKeyboard(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {Function} callback
 */
DSClient.prototype.SetOnWifiChange = function ( callback ) { prompt( "#", "App.SetOnWifiChange(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} options
 */
DSClient.prototype.SetOptions = function ( options ) { prompt( "#", "App.SetOptions(\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {String} orient
 * @param {Function} callback
 */
DSClient.prototype.SetOrientation = function ( orient,callback ) { prompt( "#", "App.SetOrientation(\f"+orient+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {Number} left
 * @param {Number} top
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 */
DSClient.prototype.SetPosition = function ( left,top,width,height,options ) { prompt( "#", "App.SetPosition(\f"+left+"\f"+top+"\f"+width+"\f"+height+"\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {String} level
 */
DSClient.prototype.SetPriority = function ( level ) { prompt( "#", "App.SetPriority(\f"+level ); }

/**
 * 
 * @memberof DSClient
 * @param {String} node
 */
DSClient.prototype.SetRingerMode = function ( mode ) { prompt( "#", "App.SetRingerMode(\f"+mode ); }

/**
 * 
 * @memberof DSClient
 * @param {String} level
 */
DSClient.prototype.SetScreenBrightness = function ( level ) { prompt( "#", "App.SetScreenBrightness(\f"+level); }

/**
 * 
 * @memberof DSClient
 * @param {String} mode
 */
DSClient.prototype.SetScreenMode = function ( mode ) { prompt( "#", "App.SetScreenMode(\f"+mode ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 */
DSClient.prototype.SetSharedApp = function ( name ) { prompt( "#", "App.SetSharedApp("+name ); }

/**
 * 
 * @memberof DSClient
 * @param {String} on
 */
DSClient.prototype.SetSpeakerPhone = function ( on ) { prompt( "#", "App.SetSpeakerPhone(\f"+on ); }

/**
 * 
 * @memberof DSClient
 * @param {String} theme
 */
DSClient.prototype.SetTheme = function ( theme ) { prompt( "#", "App.SetTheme(\f"+(theme?theme.id:null) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} title
 */
DSClient.prototype.SetTitle = function ( title ) { prompt( "#", "App.SetTitle("+title ); }

/**
 * 
 * @memberof DSClient
 * @param {String} agent
 */
DSClient.prototype.SetUserAgent = function ( agent ) { prompt( "#", "App.SetUserAgent(\f"+agent ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} password
 */
DSClient.prototype.SetUserCreds = function ( name,password ) { prompt( "#", "App.SetUserCreds(\f"+name+"\f"+password ); }

/**
 * 
 * @memberof DSClient
 * @param {String} stream
 * @param {String} level
 */
DSClient.prototype.SetVolume = function ( stream,level ) { prompt( "#", "App.SetVolume(\f"+stream+"\f"+level ); }

/**
 * 
 * @memberof DSClient
 * @param {String} enable
 */
DSClient.prototype.SetWifiEnabled = function ( enable ) { prompt( "#", "App.SetWifiEnabled(\f"+enable ); }

/**
 * 
 * @memberof DSClient
 * @param {String} show
 */
DSClient.prototype.ShowDebug = function ( show ) { prompt( "#", "App.ShowDebug("+show ); }

/**
 * 
 * @memberof DSClient
 * @param {String} obj
 */
DSClient.prototype.ShowKeyboard = function ( obj ) { return prompt( "#", "App.ShowKeyboard(\f"+obj.id )=="true"; }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.ShowMenu = function () { prompt( "#", "App.ShowMenu(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} msg
 * @param {String} options
 */
DSClient.prototype.ShowPopup = function ( msg,options ) { prompt( "#", "App.ShowPopup("+msg+"\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {String} msg
 * @param {String} options
 */
DSClient.prototype.ShowProgress = function ( msg,options ) { prompt( "#", "App.ShowProgress(\f"+msg+"\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {String} title
 * @param {String} percent
 * @param {String} options
 */
DSClient.prototype.ShowProgressBar = function ( title,percent,options ) { prompt( "#", "App.ShowProgressBar(\f"+title+"\f"+percent+"\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {String} title
 * @param {String} deflt
 * @param {Function} callback
 */
DSClient.prototype.ShowTextDialog = function ( title, deflt, callback )
{
    //Get orientation.
    var orient = app.GetOrientation();
    var textWidth = (orient=="Portrait" ? 0.8 : 0.5 );
    var btnWidth =  (orient=="Portrait" ? 0.4 : 0.25 );

    //Create dialog window.
    var dlgTxt = app.CreateDialog( title );
    var layTxtDlg = app.CreateLayout( "linear", "vertical,fillxy" );
    layTxtDlg.SetPadding( 0.04, 0.02, 0.04, 0 );

    //Create dialog controls.
    txtTxtDlg = app.CreateTextEdit( deflt, textWidth, -1, "Left" );
    layTxtDlg.AddChild( txtTxtDlg );
    
    var layTxtDlg2 = app.CreateLayout( "linear", "horizontal,fillxy,center" );
    layTxtDlg2.SetMargins( 0, 0.02, 0, 0.01 ); 
   
    //Create cancel button.
    var btnTxtCancel = app.CreateButton( "Cancel", btnWidth );
    btnTxtCancel.SetOnTouch( function(){dlgTxt.Dismiss()} );
    layTxtDlg2.AddChild( btnTxtCancel );
    
    //Create ok button.
    var btnTxtOK = app.CreateButton( "OK", btnWidth );
    btnTxtOK.SetOnTouch( function() {
        dlgTxt.Dismiss(); 
        var txt = txtTxtDlg.GetText();
        if( txt ) callback( txt ); 
        } );
    layTxtDlg2.AddChild( btnTxtOK );
    layTxtDlg.AddChild( layTxtDlg2 );

    //Add dialog layout and show dialog.
    dlgTxt.AddLayout( layTxtDlg );
    dlgTxt.Show();
}

/**
 * 
 * @memberof DSClient
 * @param {String} msg
 * @param {Number} left
 * @param {Number} top
 * @param {Number} timeOut
 * @param {String} options
 */
DSClient.prototype.ShowTip = function ( msg, left, top, timeOut, options )
{
    //Get options.
    if( options ) options = options.toLowerCase(); 
    else options = "";
    
    //Create dialog window.
    _dlgTip = app.CreateDialog( null, "AutoCancel,NoDim" );
    _dlgTip.SetBackColor( "#00000000" );
    _dlgTip.SetPosition( left, top );
    
    //Create tip layout.
    var layTipDlg = app.CreateLayout( "linear", "vertical,fillxy" );
    
    //Create tip text.
    var txt = app.CreateText( msg, -1,-1, "MultiLine,left,Html" );
    txt.SetTextSize( 22, "ps" );
    if( options.indexOf("up")>-1 ) txt.SetBackground( "/Res/drawable/tooltip_up" );
    else  txt.SetBackground( "/Res/drawable/tooltip_down" );
    txt.SetOnTouch( _Cb(txt, "txt_OnClick") );
    txt.txt_OnClick = function() { _dlgTip.Dismiss() }
    layTipDlg.AddChild( txt );
    
    //Add dialog layout and show dialog.
    _dlgTip.AddLayout( layTipDlg );
    _dlgTip.Show();
    if( timeOut ) setTimeout( function(){_dlgTip.Dismiss()}, timeOut );
}

/**
 * 
 * @memberof DSClient
 * @param {String} obj
 * @param {String} keyName
 * @param {String} modifiers
 * @param {String} pause
 */
DSClient.prototype.SimulateKey = function ( obj,keyName,modifiers,pause ) { prompt( "#", "App.SimulateKey(\f"+obj.id+"\f"+keyName+"\f"+modifiers+"\f"+pause ); }

/**
 * 
 * @memberof DSClient
 * @param {String} obj
 * @param {String} x
 * @param {String} y
 * @param {String} dir
 */
DSClient.prototype.SimulateTouch = function ( obj,x,y,dir ) { prompt( "#", "App.SimulateTouch(\f"+obj.id+"\f"+x+"\f"+y+"\f"+dir ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.Start = function () { if(typeof OnStart=='function') { OnStart(); prompt("#","_Start"); } }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @param {String} options
 * @param {String} intent
 */
DSClient.prototype.StartApp = function ( file,options,intent ) { prompt( "#", "App.StartApp(\f"+file+"\f"+options+"\f"+intent ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.StartDebugServer = function () { prompt( "#", "App.StartDebugServer(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} packageName
 * @param {String} className
 */
DSClient.prototype.StartService = function ( packageName,className ) { prompt( "#", "App.StartService(\f"+packageName+"\f"+className ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 */
DSClient.prototype.StopApp = function ( name ) { prompt( "#", "App.StopApp("+name ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.StopDebugServer = function () { prompt( "#", "App.StopDebugServer(" ); }

/**
 * 
 * @memberof DSClient
 */
DSClient.prototype.StopService = function () { prompt( "#", "App.StopService(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} cmd
 * @param {String} options
 * @param {String} maxRead
 * @param {String} timeout
 */
DSClient.prototype.SysExec = function ( cmd,options,maxRead,timeout ) { return prompt( "#", "App.SysExec(\f"+cmd+"\f"+options+"\f"+maxRead+"\f"+timeout ); }
