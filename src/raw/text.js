
/**
 * Text
 * @constructor
 * @param {String} text
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function Text( text, width, height, options ) {
    var id = prompt( "#", "App.CreateText("+text+"\f"+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating Text"); }
}
Text.prototype = Object.create( DSControl.prototype ); 
Text.prototype.constructor = Text; 

/**
 * Returns the HTML contents.
 * @memberof Text
 * @returns {String}
 */
Text.prototype.GetHtml = function() { return prompt( this.id, "Txt.GetHtml(" ); }

/**
 * Returns the line count.
 * @memberof Text
 * @returns {Number}
 */
Text.prototype.GetLineCount = function() { return parseInt(prompt( this.id, "Txt.GetLineCount(")); }

/**
 * Returns the line start.
 * @memberof Text
 * @returns {Number}
 */
Text.prototype.GetLineStart = function( line ) { return parseInt(prompt( this.id, "Txt.GetLineStart("+line )); }

/**
 * Returns the line top.
 * @memberof Text
 * @returns {Number}
 */
Text.prototype.GetLineTop = function( line ) { return parseFloat(prompt( this.id, "Txt.GetLineTop("+line )); }

/**
 * Returns the maximum number of lines.
 * @memberof Text
 * @returns {Number}
 */
Text.prototype.GetMaxLines = function() { return parseInt(prompt( this.id, "Txt.GetMaxLines(")); }

/**
 * Returns the text contents.
 * @memberof Text
 * @returns {String}
 */
Text.prototype.GetText = function() { return prompt( this.id, "Txt.GetText(" ); }

/**
 * Returns the size of text.
 * @memberof Text
 * @param {MeasurementUnit} [mode="sp"]
 * @returns {Number}
 */
Text.prototype.GetTextSize = function( mode ) { return parseFloat(prompt( this.id, "Txt.GetTextSize(\f"+mode )); }

/**
 * Returns the type name.
 * @memberof Text
 * @returns {String}
 */
Text.prototype.GetType = function() { return "Text"; }

/**
 * 
 * @memberof Text
 * @param {String} msg
 * @param {String} options
 */
Text.prototype.Log = function( msg,options ) { prompt( this.id, "Txt.Log(\f"+msg+"\f"+options ); }

/**
 * 
 * @memberof Text
 * @param {String} mode
 */
Text.prototype.SetEllipsize = function( mode ) { prompt( this.id, "Txt.SetEllipsize(\f"+mode ); }

/**
 * 
 * @memberof Text
 * @param {String} file
 */
Text.prototype.SetFontFile = function( file ) { prompt( this.id, "Txt.SetFontFile(\f"+file ); }

/**
 * 
 * @memberof Text
 * @param {String} html
 */
Text.prototype.SetHtml = function( html ) { prompt( this.id, "Txt.SetHtml("+html ); }

/**
 * 
 * @memberof Text
 * @param {Number} maxLines
 */
Text.prototype.SetLog = function( maxLines ) { prompt( this.id, "Txt.SetLog(\f"+maxLines ); }

/**
 * 
 * @memberof Text
 * @param {Function} callback
 */
Text.prototype.SetOnLongTouch = function( callback ) { prompt( this.id, "Txt.SetOnLongTouch("+_Cbm(callback) ); }

/**
 * 
 * @memberof Text
 * @param {Function} callback
 */
Text.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Txt.SetOnTouch("+_Cbm(callback) ); }

/**
 * 
 * @memberof Text
 * @param {Function} callback
 */
Text.prototype.SetOnTouchDown = function( callback ) { prompt( this.id, "Txt.SetOnTouchDown("+_Cbm(callback) ); }

/**
 * 
 * @memberof Text
 * @param {Function} callback
 */
Text.prototype.SetOnTouchMove = function( callback ) { prompt( this.id, "Txt.SetOnTouchMove("+_Cbm(callback) ); }

/**
 * 
 * @memberof Text
 * @param {Function} callback
 */
Text.prototype.SetOnTouchUp = function( callback ) { prompt( this.id, "Txt.SetOnTouchUp("+_Cbm(callback) ); }

/**
 * 
 * @memberof Text
 * @param {String} text
 */
Text.prototype.SetText = function( text ) { prompt( this.id, "Txt.SetText("+text ); }

/**
 * 
 * @memberof Text
 * @param {String} color
 */
Text.prototype.SetTextColor = function( color ) { prompt( this.id, "Txt.SetTextColor("+color ); }

/**
 * 
 * @memberof Text
 * @param {Number} radius
 * @param {Number} dx
 * @param {Number} dy
 * @param {String} color
 */
Text.prototype.SetTextShadow = function( radius,dx,dy,color ) { prompt( this.id, "Txt.SetTextShadow(\f"+radius+"\f"+dx+"\f"+dy+"\f"+color ); }

/**
 * 
 * @memberof Text
 * @param {Number} size
 * @param {MeasurementUnit} [mode="sp"]
 */
Text.prototype.SetTextSize = function( size,mode ) { prompt( this.id, "Txt.SetTextSize(\f"+size+"\f"+mode ); }

/**
 * 
 * @memberof Text
 * @param {Boolean} touchable
 */
Text.prototype.SetTouchable = function( touchable ) { prompt( this.id, "Txt.SetTouchable("+touchable ); }
