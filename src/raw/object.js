
/** 
 * DSObject - Represents a simple DroidScript object.
 * @constructor
 * @param {!String|Number} id - The object's id.
 */
function DSObject( id ) {
    _map[id] = this; 
    /** @property {Number} */
    this.id = id;
}

/**
 * Destroys (deletes) the object.
 * @memberof DSObject
 * @see {@link DSObject#Release}
 */
DSObject.prototype.Destroy = function() { prompt( this.id, "Obj.Release(" ); _map[this.id] = null; }

/**
 * Returns the type name of the object or control.
 * @memberof DSObject
 * @returns {String}
 */
DSObject.prototype.GetType = function() { return this.constructor.name; }

/**
 * Releases (deletes) the object.
 * @memberof DSObject
 * @see {@link DSObject#Destroy}
 */
DSObject.prototype.Release = function() { prompt( this.id, "Obj.Release(" ); _map[this.id] = null; }
