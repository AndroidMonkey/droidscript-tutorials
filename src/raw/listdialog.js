
/**
 * ListDialog
 * @constructor
 * @param {String} title
 * @param {String} list
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function ListDialog( title, list, options ) {
    if ( _ldg ) { _ldg.Release(); }
    var id = prompt( "#", "App.CreateListDialog("+title+"\f"+list+"\f"+options );
    if ( id ) { DSObject.call( this, id ); }
    else { throw new DSError("Error creating ListDialog"); }
    _ldg = this;
}
ListDialog.prototype = Object.create( DSObject.prototype ); 
ListDialog.prototype.constructor = ListDialog; 

ListDialog.prototype.AdjustColor = function( hue,sat,bright,cont ) { prompt( obj.id, "Ldg.AdjustColor(\f"+hue+"\f"+sat+"\f"+bright+"\f"+cont ); }
ListDialog.prototype.Dismiss = function() { prompt( obj.id, "Ldg.Dismiss(" ); }
ListDialog.prototype.GetType = function() { return "ListDialog"; }
ListDialog.prototype.Hide = function() { prompt( obj.id, "Ldg.Hide(" ); }
ListDialog.prototype.SetBackColor = function( clr ) { prompt( obj.id, "Ldg.SetBackColor(\f"+clr ); }
ListDialog.prototype.SetBackground = function( file,options ) { prompt( obj.id, "Ldg.SetBackground(\f"+file+"\f"+options ); }
ListDialog.prototype.SetOnTouch = function( callback ) { prompt( obj.id, "Ldg.SetOnClick("+_Cbm(callback) ); }
ListDialog.prototype.SetSize = function( width,height,options ) { prompt( obj.id, "Ldg.SetSize(\f"+width+"\f"+height+"\f"+options ); }
ListDialog.prototype.SetTextColor = function( clr ) { prompt( obj.id, "Ldg.SetTextColor(\f"+clr ); }
ListDialog.prototype.SetTitle = function( title ) { prompt( obj.id, "Ldg.SetTitle(\f"+ title ); }
ListDialog.prototype.Show = function() { prompt( obj.id, "Ldg.Show(" ); }
