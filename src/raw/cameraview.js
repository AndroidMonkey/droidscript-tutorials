
/**
 * Creates a camera view control.
 * @memberof DSClient
 * @constructs CameraView
 * @param {DecimalFraction} width - The width of the camera view as a percentage of the parent container.
 * @param {DecimalFraction} height - The height of the camera view as a percentage of the parent container.
 * @param {String} options - "CIF","QVGA","SVGA","VGA","UXGA","XGA", "Front","NoSound","UseBitmap","UseYUV","NoRotate", "Cam0","Cam1","Cam2","Cam3"
 * @returns {CameraView} The newly created camera view.
 * @Throws {DSError} Will throw an error if camera view cannot be created.
 * @see {@link [resolution comparison](http://www.videotechnology.com/0904/formats.html)}


/**
 * CameraView
 * @class
 * @hideconstructor
 */
function CameraView( width, height, options ) {
    var id = prompt( "#", "App.CreateCameraView("+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating CameraView"); }
}
CameraView.prototype = Object.create( DSControl.prototype ); 
CameraView.prototype.constructor = CameraView; 

/**
 * AutoCapture
 * @memberof CameraView
 * @todo Find out what this does...?
 * @param {String} pàth
 * @param {String} file
 * @param {Number} maxCount
 */
CameraView.prototype.AutoCapture = function( path, file, maxCount ) { prompt( this.id, "Cam.AutoCapture("+path+"\f"+file+"\f"+maxCount ); }

/**
 *
 * @memberof CameraView
 * @param {Number} max
 * @returns {Object}
 */
CameraView.prototype.FindFaces = function( max ) { return eval(prompt( this.id, "Cam.FindFaces(\f"+max )); }

/**
 *
 * @memberof CameraView
 */
CameraView.prototype.Focus = function() { prompt( this.id, "Cam.Focus(" ); }

/**
 *
 * @memberof CameraView
 * @returns {Integer}
 */
CameraView.prototype.GetCameraCount = function() { return parseInt(prompt( this.id, "Cam.GetCameraCount(" )); }

/**
 *
 * @memberof CameraView
 * @returns {String}
 */
CameraView.prototype.GetColorEffects = function() { return prompt( this.id, "Cam.GetColorEffects(" ); }

/**
 *
 * @memberof CameraView
 * @returns {Integer}
 */
CameraView.prototype.GetImageHeight = function() { return parseInt(prompt( this.id, "Cam.GetImageHeight(" )); }

/**
 *
 * @memberof CameraView
 * @returns {Integer}
 */
CameraView.prototype.GetImageWidth = function() { return parseInt(prompt( this.id, "Cam.GetImageWidth(" )); }

/**
 *
 * @memberof CameraView
 * @returns {Integer}
 */
CameraView.prototype.GetMaxZoom = function() { return parseInt(prompt( this.id, "Cam.GetMaxZoom(" )); }

/**
 *
 * @memberof CameraView
 * @returns {String}
 */
CameraView.prototype.GetParameters = function() { return prompt( this.id, "Cam.GetParams(\f" ); }

/**
 *
 * @memberof CameraView
 * @returns {String}
 */
CameraView.prototype.GetPictureSizes = function() { return prompt( this.id, "Cam.GetPictureSizes(" ); }

/**
 *
 * @memberof CameraView
 * @param {String} format - “RawBase64”, “PngBase64” or “JpgBase64"
 * @param {Number} left
 * @param {Number} top
 * @param {Number} width
 * @param {Number} height
 * @returns {String} 
 */
CameraView.prototype.GetPixelData = function( format,left,top,width,height ) { return prompt( this.id, "Cam.GetPixelData(\f"+format+"\f"+left+"\f"+top+"\f"+width+"\f"+height ); }

/**
 * Get the control type.
 * @memberof CameraView
 * @returns {String}
 */
CameraView.prototype.GetType = function() { return "CameraView"; }

/**
 *
 * @memberof CameraView
 * @returns {Integer} 
 */
CameraView.prototype.GetZoom = function() { return parseInt(prompt( this.id, "Cam.GetZoom(" )); }

/**
 *
 * @memberof CameraView
 * @returns {Boolean}
 */
CameraView.prototype.HasFlash = function() { return prompt( this.id, "Cam.HasFlash(" )==="true"; }

/**
 *
 * @memberof CameraView
 * @returns {Boolean}
 */
CameraView.prototype.IsRecording = function() { return prompt( this.id, "Cam.IsRecording(" )==="true"; }

/**
 *
 * @memberof CameraView
 * @param xtiles
 * @param ytiles
 * @param sensitivity
 * @param minPeriod
 * @param img
 */
CameraView.prototype.MotionMosaic = function( xtiles, ytiles, sensitivity, minPeriod, img ) { prompt( this.id, "Cam.MotionMosaic("+xtiles+"\f"+ytiles+"\f"+sensitivity+"\f"+minPeriod+"\f"+(img?img.id:null) ); }

/**
 *
 * @memberof CameraView
 * @param {String} file
 * @param {Number} seconds
 */
CameraView.prototype.Record = function( file, seconds ) { prompt( this.id, "Cam.Record(\f"+file+"\f"+seconds ); }

/**
 *
 * @memberof CameraView
 * @param {String} list
 * @param {Function} callback
 * @param {Number} sampSize
 * @param {Number} maxRate
 */
CameraView.prototype.ReportColors = function( list, callback, sampSize, maxRate ) { prompt( this.id, "Cam.ReportColors(\f"+list+"\f"+_Cbm(callback)+"\f"+sampSize+"\f"+maxRate ); }

/**
 *
 * @memberof CameraView
 * @param {String} effect
 */
CameraView.prototype.SetColorEffect = function( effect ) { prompt( this.id, "Cam.SetColorEffect(\f"+effect ); }

/**
 *
 * @memberof CameraView
 * @param {Image} img1
 * @param {Image} img2
 */
CameraView.prototype.SetDuplicateImage = function( img1, img2 ) { prompt( this.id, "Cam.SetDuplicateImage(\f"+(img1?img1.id:null)+"\f"+(img2?img2.id:null) ); }

/**
 *
 * @memberof CameraView
 * @param {Boolean} onoff - True for on, false for off.
 */
CameraView.prototype.SetFlash = function( onoff ) { prompt( this.id, "Cam.SetFlash("+onoff ); }

/**
 *
 * @memberof CameraView
 * @param {String} mode
 */
CameraView.prototype.SetFocusMode = function( mode ) { prompt( this.id, "Cam.SetFocusMode(\f"+mode ); }

/**
 *
 * @memberof CameraView
 * @param {Function} callback
 */
CameraView.prototype.SetOnFocus = function( callback ) { prompt( this.id, "Cam.SetOnFocus\f"+_Cbm(callback) ); }

/**
 *
 * @memberof CameraView
 * @param {Function} callback
 */
CameraView.prototype.SetOnMotion = function( callback ) { prompt( obj.id, "Cam.SetOnMotion("+_Cbm(callback) ); }

/**
 *
 * @memberof CameraView
 * @param {Function} callback
 */
CameraView.prototype.SetOnPicture = function( callback ) { prompt( this.id, "Cam.SetOnPicture\f"+_Cbm(callback) ); }

/**
 *
 * @memberof CameraView
 * @param {Function} callback
 */
CameraView.prototype.SetOnReady = function( callback ) { prompt( this.id, "Cam.SetOnReady("+_Cbm(callback) ); }

/**
 *
 * @memberof CameraView
 * @param angle
 */
CameraView.prototype.SetOrientation = function( angle ) { prompt( this.id, "Cam.SetOrientation(\f"+angle ); }

/**
 *
 * @memberof CameraView
 * @param {String} name
 * @param {String|Number} value
 */
CameraView.prototype.SetParameter = function( name, value ) { if( typeof value==="string" ) prompt( this.id, "Cam.SetParam(\f"+name+"\f"+value ); else prompt( this.id, "Cam.SetParamNum(\f"+name+"\f"+value ); }

/**
 *
 * @memberof CameraView
 * @param {Number} width
 * @param {Number} height
 */
CameraView.prototype.SetPictureSize = function( width, height ) { prompt( this.id, "Cam.SetPictureSize(\f"+width+"\f"+height ); }

/**
 *
 * @memberof CameraView
 * @param angle
 */
CameraView.prototype.SetPostRotation = function( angle ) { prompt( this.id, "Cam.SetPostRotation(\f"+angle ); }

/**
 *
 * @memberof CameraView
 * @param {Image} img
 */
CameraView.prototype.SetPreviewImage = function( img ) { prompt( this.id, "Cam.SetPreviewImage("+(img?img.id:null) ); }

/**
 *
 * @memberof CameraView
 * @param {Boolean} onoff - True for on, false for off.
 */
CameraView.prototype.SetSound = function( onoff ) { prompt( this.id, "Cam.SetSound("+onoff ); }

/**
 *
 * @memberof CameraView
 * @param {Number} width
 * @param {String}
 */
CameraView.prototype.SetVideoSize = function( width, height ) { prompt( this.id, "Cam.SetVideoSize(\f"+width+"\f"+height ); }

/**
 *
 * @memberof CameraView
 * @param {Number} level
 */
CameraView.prototype.SetZoom = function( level ) { prompt( this.id, "Cam.SetZoom(\f"+level ); }

/**
 *
 * @memberof CameraView
 */
CameraView.prototype.StartPreview = function() { prompt( this.id, "Cam.StartPreview(" ); }

/**
 *
 * @memberof CameraView
 */
CameraView.prototype.Stop = function() { prompt( this.id, "Cam.Stop(" ); }

/**
 *
 * @memberof CameraView
 */
CameraView.prototype.StopPreview = function() { prompt( this.id, "Cam.StopPreview(" ); }

/**
 *
 * @memberof CameraView
 * @param {Number} ip
 * @param {Number} port
 * @param {Number} quality
 * @param {Number} fps
 * @param {Number} mtu
 */
CameraView.prototype.Stream = function( ip, port, quality, fps, mtu ) { prompt( this.id, "Cam.Stream("+ip+"\f"+port+"\f"+quality+"\f"+fps+"\f"+mtu ); }

/**
 * Takes a picture, writing the image to file.
 * @memberof CameraView
 * @param {String} file
 */
CameraView.prototype.TakePicture = function( file ) { prompt( this.id, "Cam.TakePicture("+file ); }
