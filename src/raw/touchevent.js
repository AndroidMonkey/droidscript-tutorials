
/**
 * DSTouchEvent
 * @param {Object} event
 * @param {DSControl} event.source - Control that triggered event.
 * @param {String}    event.action - "Down","Move","Up"
 * @param {Integer}   event.count
 * @param {Float}     event.X - X coordinate from 0.0 to 1.0
 * @param {Float}     event.Y - Y coordinate from 0.0 to 1.0
 * @param {Float[]}   event.x
 * @param {Float[]}   event.y
 */
function DSTouchEvent(ev) {
    DSEvent.call( this, ev );
    //this.source = ev.source;
    //this.action = ev.action;
    this.count = ev.count;
    this.X = ev.X;
    this.Y = ev.Y;
    this.x = ev.x;
    this.y = ev.y;
}
DSTouchEvent.prototype = Object.create( DSEvent.prototype ); 
DSTouchEvent.prototype.constructor = DSTouchEvent; 
