
/**
 * SeekBar
 * @constructor
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function SeekBar( width, height, options ) {
    var id = prompt( "#", "App.CreateSeekBar("+width+"\f"+height+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating SeekBar"); }
}
SeekBar.prototype = Object.create( DSControl.prototype ); 
SeekBar.prototype.constructor = SeekBar; 

/**
 * 
 * @memberof SeekBar
 * @param {Number} hue
 * @param {Number} sat
 * @param {Number} bright
 * @param {Number} cont
 */
SeekBar.prototype.AdjustColor = function( hue,sat,bright,cont ) { prompt( this.id, "Skb.AdjustColor(\f"+hue+"\f"+sat+"\f"+bright+"\f"+cont ); }

/**
 * Returns the type name.
 * @memberof SeekBar
 * @returns {String}
 */
SeekBar.prototype.GetType = function() { return "SeekBar"; }

/**
 * 
 * @memberof SeekBar
 * @returns {Number}
 */
SeekBar.prototype.GetValue = function() { return parseFloat(prompt( this.id, "Skb.GetValue(" )); }

/**
 * 
 * @memberof SeekBar
 * @param {Number} clr
 * @param {Number} mode
 */
SeekBar.prototype.SetColorFilter = function( clr,mode ) { prompt( this.id, "Skb.SetColorFilter(\f"+clr+"\f"+mode ); }

/**
 * 
 * @memberof SeekBar
 * @param {Number} rate
 */
SeekBar.prototype.SetMaxRate = function( rate ) { prompt( this.id, "Skb.SetMaxRate("+rate ); }

/**
 * 
 * @memberof SeekBar
 * @param {Function} callback
 */
SeekBar.prototype.SetOnChange = function( callback ) { prompt( this.id, "Skb.SetOnClick("+_Cbm(callback) ); }

/**
 * 
 * @memberof SeekBar
 * @param {Function} callback
 */
SeekBar.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Skb.SetOnClick("+_Cbm(callback) ); }

/**
 * 
 * @memberof SeekBar
 * @param {Number} range
 */
SeekBar.prototype.SetRange = function( range ) { prompt( this.id, "Skb.SetRange("+range ); }

/**
 * 
 * @memberof SeekBar
 * @param {Number} val
 */
SeekBar.prototype.SetValue = function( val ) { prompt( this.id, "Skb.SetValue("+val ); }
