
/**
 * WebView
 * @constructor
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @param {String} zoom
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function WebView( width, height, options, zoom ) {
    var id = prompt( "#", "App.CreateWeb(\f"+width+"\f"+height+"\f"+options+"\f"+zoom );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating WebView"); }
}
WebView.prototype = Object.create( DSControl.prototype ); 
WebView.prototype.constructor = WebView; 

/**
 * 
 * @memberof WebView
 */
WebView.prototype.Back = function() { prompt(obj.id,"Web.Back(" ); }

/**
 * 
 * @memberof WebView
 * @returns {Boolean}
 */
WebView.prototype.CanGoBack = function() { return prompt( obj.id, "Web.CanGoBack(" )=="true"; }

/**
 * 
 * @memberof WebView
 * @returns {Boolean}
 */
WebView.prototype.CanGoForward = function() { return prompt( obj.id, "Web.CanGoForward(" )=="true"; }

/**
 * 
 * @memberof WebView
 * @param {String} file
 */
WebView.prototype.Capture = function( file ) { prompt( obj.id, "Web.Capture(\f"+file ); }

/**
 * 
 * @memberof WebView
 */
WebView.prototype.ClearHistory = function() { prompt(obj.id,"Web.ClearHistory(" ); }

/**
 * 
 * @memberof WebView
 * @param {String} code
 * @param {Function} callback
 */
WebView.prototype.Execute = function( code,callback ) { prompt( obj.id, "Web.Execute(\f"+code+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof WebView
 */
WebView.prototype.Forward = function() { prompt(obj.id,"Web.Forward(" ); }

/**
 * Returns the type name.
 * @memberof WebView
 * @returns {String}
 */
WebView.prototype.GetType = function() { return "WebView"; }

/**
 * 
 * @memberof WebView
 * @returns {String}
 */
WebView.prototype.GetUrl = function() { return prompt(obj.id,"Web.GetUrl(" ); }

/**
 * 
 * @memberof WebView
 * @param {String} html
 * @param {String} base
 * @param {String} options
 */
WebView.prototype.LoadHtml = function( html,base,options ) { prompt(obj.id,"Web.LoadHtml(\f"+html+"\f"+base+"\f"+options); }

/**
 * 
 * @memberof WebView
 * @param {String} url
 * @param {String} options
 */
WebView.prototype.LoadUrl = function( url,options ) { prompt(obj.id,"Web.LoadUrl(\f"+url+"\f"+options); }

/**
 * 
 * @memberof WebView
 */
WebView.prototype.Print = function() { prompt( obj.id, "Web.Print(\f" ); }

/**
 * 
 * @memberof WebView
 */
WebView.prototype.Reload = function() { prompt(obj.id,"Web.Reload(" ); }

/**
 * 
 * @memberof WebView
 * @param {Function} callback
 */
WebView.prototype.SetOnConsole = function( callback ) { prompt( obj.id, "Web.SetOnConsole(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof WebView
 * @param {Function} callback
 */
WebView.prototype.SetOnError = function( callback ) { prompt( obj.id, "Web.SetOnError(\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof WebView
 * @param {Function} callback
 */
WebView.prototype.SetOnProgress = function( callback ) { prompt( obj.id, "Web.SetOnProgress("+_Cbm(callback) ); }

/**
 * 
 * @memberof WebView
 * @param {String} urlFrom
 * @param {String} urlTo
 */
WebView.prototype.SetRedirect = function( urlFrom, urlTo ) { prompt( obj.id, "Web.SetRedirect(\f"+urlFrom+"\f"+urlTo ); }

/**
 * 
 * @memberof WebView
 * @param {String} agent
 */
WebView.prototype.SetUserAgent = function( agent ) { prompt( obj.id, "Web.SetUserAgent(\f"+agent ); }

/**
 * 
 * @memberof WebView
 * @param {String} name
 * @param {String} password
 */
WebView.prototype.SetUserCreds = function( name,password ) { prompt( obj.id, "Web.SetUserCreds(\f"+name+"\f"+password ); }

/**
 * 
 * @memberof WebView
 * @param {String} keyName
 * @param {String} modifiers
 * @param {String} pause
 */
WebView.prototype.SimulateKey = function( keyName,modifiers,pause ) { prompt( obj.id, "Web.SimulateKey(\f"+keyName+"\f"+modifiers+"\f"+pause ); }
