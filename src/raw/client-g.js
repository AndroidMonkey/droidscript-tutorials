
/**
 * 
 * @memberof DSClient
 * @param {String} cmd
 */
DSClient.prototype.GA = function ( cmd )
	{
		try {
			if( cmd.toLowerCase()=='create' ) {
				_LoadScriptSync( "/Sys/ga.js" );
				window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
				var dbg = _dbg; app.SetDebugEnabled( false );
				ga('create', arguments[1], {'storage':'none', 'clientId':app.GetDeviceId()});
				ga('set', { checkProtocolTask: null, checkStorageTask: null });
				app.SetDebugEnabled( dbg );
			}
			else ga.apply( this, arguments );
		}
		catch(e){}
	}

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetAccounts = function () { return prompt( "#", "App.GetAccounts(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} options
 * @returns {String}
 */
DSClient.prototype.GetActivities = function ( options ) { return eval(prompt( "#", "App.GetActivities(\f"+options )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetAppName = function () { return prompt( "#", "App.GetAppName(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetAppPath = function () { return prompt( "#", "App.GetAppPath(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetBatteryLevel = function () { return parseFloat(prompt( "#", "App.GetBatteryLevel(\f" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetBluetoothAddress = function () { return prompt( "#", "App.GetBluetoothAddress(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetBluetoothName = function () { return prompt( "#", "App.GetBluetoothName(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} type
 * @returns {String}
 */
DSClient.prototype.GetBtProfileState = function ( type ) { return prompt( "#", "App.GetBtProfileState(\f"+type ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetBuildNum = function () { return parseInt( prompt( "#", "App.GetBuildNum(" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetChargeType = function () { return prompt( "#", "App.GetChargeType(\f" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetClipboardText = function () { return prompt( "#", "App.GetClipboardText(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetCountry = function () { return prompt( "#", "App.GetCountry(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetCountryCode = function () { return prompt( "#", "App.GetCountryCode(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetDSVersion = function () { return parseFloat(prompt( "#", "App.GetDSVersion(" )); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @returns {String}
 */
DSClient.prototype.GetData = function ( name ) { return prompt( "#", "App.GetData(\f"+name ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetDatabaseFolder = function () { return prompt( "#", "App.GetDatabaseFolder(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetDefaultOrientation = function () { return prompt( "#", "App.GetDefaultOrientation(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetDeviceId = function () { return prompt( "#", "App.GetDeviceId(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetDisplayHeight = function () { return parseFloat(prompt( "#", "App.GetDisplayHeight(" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetDisplayWidth = function () { return parseFloat(prompt( "#", "App.GetDisplayWidth(" )); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @returns {String}
 */
DSClient.prototype.GetEnv = function ( name ) { return prompt( "#", "App.GetEnv(\f"+name ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetExternalFolder = function () { return prompt( "#", "App.GetExternalFolder(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @returns {String}
 */
DSClient.prototype.GetFileDate = function ( file ) { var d = parseInt(prompt( "#", "App.GetFileDate(\f"+file)); if( d ) return new Date(d); else return null; }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @returns {String}
 */
DSClient.prototype.GetFileSize = function ( file ) { return parseInt(prompt( "#", "App.GetFileSize(\f"+file)); }

/**
 * 
 * @memberof DSClient
 * @param {String} mode
 * @returns {String}
 */
DSClient.prototype.GetFreeSpace = function ( mode ) { return parseFloat(prompt( "#", "App.GetFreeSpace(\f"+mode)); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetIPAddress = function () { return prompt( "#", "App.GetIPAddress(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetInstalledApps = function () { return eval(prompt( "#", "App.GetInstalledApps(\f" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetIntent = function () { var s = prompt( "#", "App.GetIntent(" ); if(s.length) return JSON.parse(s); else return null; }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetInternalFolder = function () { return prompt( "#", "App.GetInternalFolder(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} id
 * @returns {String}
 */
DSClient.prototype.GetJoystickName = function ( id ) { return prompt( "#", "App.GetJoyName(\f"+id); }

/**
 * 
 * @memberof DSClient
 * @param {String} id
 * @param {String} key
 * @returns {String}
 */
DSClient.prototype.GetJoystickState = function ( id,key ) { return parseFloat(prompt( "#", "App.GetJoyState(\f"+id+"\f"+key)); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetKeyboardHeight = function () { return prompt( "#", "App.GetKeyboardHeight(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetLanguage = function () { return prompt( "#", "App.GetLanguage(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetLanguageCode = function () { return prompt( "#", "App.GetLanguageCode(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetLastButton = function () { var ret = prompt( "#", "App.GetLastButton(" ); if( ret ) return (_map[ret]); else return null; }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetLastCheckBox = function () { var ret = prompt( "#", "App.GetLastCheckBox(" ); if( ret ) return (_map[ret]); else return null; }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetLastImage = function () { var ret = prompt( "#", "App.GetLastImage(" ); if( ret ) return (_map[ret]); else return null; }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetLastToggle = function () { var ret = prompt( "#", "App.GetLastToggle(" ); if( ret ) return (_map[ret]); else return null; }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetMacAddress = function () { return prompt( "#", "App.GetMacAddress(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} appName
 * @param {String} ext
 * @returns {String}
 */
DSClient.prototype.GetMediaFile = function ( appName,ext ) { return prompt( "#", "App.GetMediaFile(\f"+appName+"\f"+ext ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetMemoryInfo = function () { return eval(prompt( "#", "App.GetMemoryInfo(\f" )); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @param {String} keys
 * @returns {String}
 */
DSClient.prototype.GetMetadata = function ( file,keys ) { return prompt( "#", "App.GetMetadata(\f"+file+"\f"+keys); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetModel = function () { return prompt( "#", "App.GetModel(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetName = function () { return prompt( "#", "App.GetName(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetNotifyId = function () { return prompt( "#", "App.GetNotifyId(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetOSVersion = function () { return parseInt( prompt( "#", "App.GetBuildNum(" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetObjects = function () { return _map; }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetOptions = function () { return prompt( "#", "App.GetOptions(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetOrientation = function () { return prompt( "#", "App.GetOrientation(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetPackageName = function () { return prompt( "#", "App.GetPackageName(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetPairedBtDevices = function () { return eval(prompt( "#", "App.GetPairedBTDevices(\f" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetPath = function () { return prompt( "#", "App.GetPath(" ); }

/**
 * 
 * @memberof DSClient
 * @param {String} type
 * @param {Function} callback
 * @returns {String}
 */
DSClient.prototype.GetPermission = function ( type,callback ) { prompt( "#", "App.GetPermission(\f"+type+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @returns {String}
 */
DSClient.prototype.GetPrivateFolder = function ( name ) { return prompt( "#", "App.GetPrivateFolder(\f"+name ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetRSSI = function () { return parseInt(prompt( "#", "App.GetRSSI(" )); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} options
 * @returns {String}
 */
DSClient.prototype.GetResourceId = function ( name,options ) { return parseInt(prompt( "#", "App.GetResourceId(\f"+name+"\f"+options )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetRingerMode = function () { return prompt( "#", "App.GetRingerMode(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetRotation = function () { return parseInt(prompt( "#", "App.GetRotation(" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetRunningApps = function () { return eval(prompt( "#", "App.GetRunningApps(\f" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetRunningServices = function () { return eval(prompt( "#", "App.GetRunningServices(\f" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetSSID = function () { return prompt( "#", "App.GetSSID(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetScreenDensity = function () { return parseFloat(prompt( "#", "App.GetScreenDensity(" )); }

/**
 * 
 * @memberof DSClient
 * @param {String} options
 * @returns {String}
 */
DSClient.prototype.GetScreenHeight = function ( options ) { return parseFloat(prompt( "#", "App.GetScreenHeight(\f"+options )); }

/**
 * 
 * @memberof DSClient
 * @param {String} options
 * @returns {String}
 */
DSClient.prototype.GetScreenWidth = function ( options ) { return parseFloat(prompt( "#", "App.GetScreenWidth(\f"+options )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetSharedFiles = function () { var s = prompt( "#", "App.GetSharedFiles(" ); if(s.length) return s.split(","); else return null; }

/**
 * 
 * @memberof DSClient
 * @param {String} index
 * @returns {String}
 */
DSClient.prototype.GetSharedText = function ( index ) { return prompt( "#", "App.GetSharedText("+index ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetSpeakerPhone = function () { return prompt( "#", "App.GetSpeakerPhone(" )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @returns {String}
 */
DSClient.prototype.GetSpecialFolder = function ( name ) { return prompt( "#", "App.GetSpecialFolder(\f"+name ); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 * @param {Number} width
 * @param {Number} height
 * @returns {String}
 */
DSClient.prototype.GetThumbnail = function ( src,dest,width,height ) { prompt( "#", "App.GetThumbnail(\f"+src+"\f"+dest+"\f"+width+"\f"+height); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetTop = function () { return parseFloat(prompt( "#", "App.GetTop(" )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetType = function () { return "App"; }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetUser = function () { return prompt( "#", "App.GetUser(" ); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GetVersion = function () { return parseFloat(prompt( "#", "App.GetVersion(" )); }

/**
 * 
 * @memberof DSClient
 * @param {String} stream
 * @returns {String}
 */
DSClient.prototype.GetVolume = function ( stream ) { return parseFloat(prompt( "#", "App.GetVolume(\f"+stream )); }

/**
 * 
 * @memberof DSClient
 * @returns {String}
 */
DSClient.prototype.GoToSleep = function () { prompt( "#", "App.GoToSleep(" ); }
