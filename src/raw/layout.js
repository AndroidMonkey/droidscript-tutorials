
DSClient.prototype.CreateLayout = function( type, options ) { return new Layout( type, options );}

/**
 * Layout
 * @constructor
 * @param {!String} type - "Absolute", "Frame", "Linear"
 * @param {String} [options="Center,Top"] - "Left","Right","Top","Bottom","Center","VCenter","TouchThrough","TouchSpy"
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function Layout ( type, options ) {
    var id = prompt( "#", "App.CreateLayout("+type+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating Layout"); }
}
Layout.prototype = Object.create( DSControl.prototype ); 
Layout.prototype.constructor = Layout; 

/**
 * Adds child control to layout.
 * @memberof Layout
 * @param {DSControl} child
 * @param {Number} order
 * @see {@link Layout#ChildToFront}
 * @see {@link Layout#DestroyChild}
 * @see {@link Layout#GetChildOrder}
 * @see {@link Layout#RemoveChild}
 */
Layout.prototype.AddChild = function( child, order ) { prompt( this.id, "Lay.AddChild(\f"+(child?child.id:null)+"\f"+order ); if(child) child._parent = this; }

/**
 * Animate transitions between layouts.
 * @memberof Layout
 * @param {String} type - /Fade(In|Out)/,Flip,/(Scale|Slide)(To|From)(Left|Right|Top|Bottom)/
 * @param {Function} callback - Function to call when animation starts.
 * @param {Number} time
 */
Layout.prototype.Animate = function( type, callback, time ) { prompt( this.id, "Lay.Animate(\f"+type+"\f"+_Cbm(callback)+"\f"+time ); }

/**
 * Moves child control to front.
 * @memberof Layout
 * @param {DSControl} child
 * @see {@link Layout#AddChild}
 * @see {@link Layout#DestroyChild}
 * @see {@link Layout#GetChildOrder}
 * @see {@link Layout#RemoveChild}
 */
Layout.prototype.ChildToFront = function( child ) { prompt( this.id, "Lay.ChildToFront("+(child?child.id:null) ); }

/**
 * Removes child control from layout.
 * @memberof Layout
 * @param {DSControl} child
 * @see {@link Layout#AddChild}
 * @see {@link Layout#ChildToFront}
 * @see {@link Layout#GetChildOrder}
 * @see {@link Layout#RemoveChild}
 */
Layout.prototype.DestroyChild = function( child ) { prompt( this.id, "Lay.DestroyChild("+(child?child.id:null) ); if(child) child._parent = null; }

/**
 * Returns the position of child within the layout's children array.
 * @memberof Layout
 * @param {DSControl} child
 * @see {@link Layout#AddChild}
 * @see {@link Layout#ChildToFront}
 * @see {@link Layout#DestroyChild}
 * @see {@link Layout#RemoveChild}
 */
Layout.prototype.GetChildOrder = function( child ) { return parseInt(prompt( this.id, "Lay.GetChildOrder(\f"+(child?child.id:null) )); }

/**
 * Removes child control from layout.
 * @memberof Layout
 * @param {DSControl} child
 * @see {@link Layout#AddChild}
 * @see {@link Layout#ChildToFront}
 * @see {@link Layout#DestroyChild}
 * @see {@link Layout#GetChildOrder}
 */
Layout.prototype.RemoveChild = function( child ) { prompt( this.id, "Lay.RemoveChild("+(child?child.id:null) ); if(child) child._parent = null; }

/**
 * Sets the horizontal and/or vertical alignment.
 * @memberof Layout
 * @param {String} gravity - "Left","Right","Top","Bottom","Center","VCenter"
 * @see {@link Layout#SetOrientation}
 */
Layout.prototype.SetGravity = function( gravity ) { prompt( this.id, "Lay.SetGravity(\f"+gravity ); }

/**
 * Sets the function to call when the layout is tapped.
 * @memberof Layout
 * @param {Function} callback
 * @see {@link Layout#SetOnTouchDown}
 * @see {@link Layout#SetOnTouchMove}
 * @see {@link Layout#SetOnTouchUp}
 * @see {@link Layout#SetOnLongTouch}
 * @see {@link Layout#SetTouchable}
 */
Layout.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Lay.SetOnTouch(\f"+_Cbm(callback) ); } 

/**
 * Sets the function to call when the layout is touched.
 * @memberof Layout
 * @param {Function} callback
 * @see {@link Layout#SetOnTouch}
 * @see {@link Layout#SetOnTouchMove}
 * @see {@link Layout#SetOnTouchUp}
 * @see {@link Layout#SetOnLongTouch}
 * @see {@link Layout#SetTouchable}
 */
Layout.prototype.SetOnTouchDown = function( callback ) { prompt( this.id, "Lay.SetOnTouchDown(\f"+_Cbm(callback) ); } 

/**
 * Sets the function to call when the layout touchpoint is moved.
 * @memberof Layout
 * @param {Function} callback
 * @see {@link Layout#SetOnTouch}
 * @see {@link Layout#SetOnTouchDown}
 * @see {@link Layout#SetOnTouchUp}
 * @see {@link Layout#SetOnLongTouch}
 * @see {@link Layout#SetTouchable}
 */
Layout.prototype.SetOnTouchMove = function( callback ) { prompt( this.id, "Lay.SetOnTouchMove(\f"+_Cbm(callback) ); }

/**
 * Sets the function to call when the touch is released.
 * @memberof Layout
 * @param {Function} callback
 * @see {@link Layout#SetOnTouch}
 * @see {@link Layout#Layout#SetOnTouchDown}
 * @see {@link Layout#SetOnTouchMove}
 * @see {@link Layout#SetOnLongTouch}
 * @see {@link Layout#SetTouchable}
 */
Layout.prototype.SetOnTouchUp = function( callback ) { prompt( this.id, "Lay.SetOnTouchUp(\f"+_Cbm(callback) ); }

/**
 * Sets the function to call after an extended touch.
 * @memberof Layout
 * @param {Function} callback
 * @see {@link Layout#SetOnTouch}
 * @see {@link Layout#SetOnTouchDown}
 * @see {@link Layout#SetOnTouchMove}
 * @see {@link Layout#SetOnTouchUp}
 * @see {@link Layout#SetTouchable}
 */
Layout.prototype.SetOnLongTouch = function( callback ) { prompt( this.id, "Lay.SetOnLongTouch(\f"+_Cbm(callback) ); } 

/**
 * Sets the function to call when a user makes changes to a TextEdit.
 * @memberof Layout
 * @param {Function} callback
 */
Layout.prototype.SetOnChildChange = function( callback ) { prompt( this.id, "Lay.SetOnChildChange(\f"+_Cbm(callback) ); }

/**
 * Sets the orientation to "Horizontal" or "Vertical".
 * @memberof Layout
 * @param {String} orient
 * @see {@link Layout#SetGravity}
 */
Layout.prototype.SetOrientation = function( orient ) { prompt( this.id, "Lay.SetOrientation(\f"+orient ); } 

/**
 * Sets whether the layout responds to touch events or not.
 * @memberof Layout
 * @param {Boolean} touchable
 * @see {@link Layout#SetOnTouch}
 * @see {@link Layout#SetOnTouchDown}
 * @see {@link Layout#SetOnTouchMove}
 * @see {@link Layout#SetOnTouchUp}
 * @see {@link Layout#SetOnLongTouch}
 */
Layout.prototype.SetTouchable = function( touchable ) { prompt( this.id, "Lay.SetTouchable(\f"+touchable ); }
