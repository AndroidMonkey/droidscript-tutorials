
/**
 * DSClient - Class representing the DroidScript "App" function.
 * @constructor
 */
function DSClient() {
}

/**
 * Adds a layout like a drawer on the side you can drag open.
 * @memberof DSClient
 * @param {Layout} layout
 * @param {String} side
 * @param {Number} width
 * @param {Number} grabWidth
 * @see {@link DSClient#AddLayout}
 * @see {@link DSClient#CloseDrawer}
 * @see {@link DSClient#CreateLayout}
 * @see {@link DSClient#DestroyDrawer}
 * @see {@link DSClient#LockDrawer}
 * @see {@link DSClient#OpenDrawer}
 * @see {@link DSClient#RemoveDrawer}
 * @see {@link DSClient#UnlockDrawer}
 */
DSClient.prototype.AddDrawer = function ( layout,side,width,grabWidth ) { prompt( "#", "App.AddDrawer(\f"+layout.id+"\f"+side+"\f"+width+"\f"+grabWidth ); }

/**
 * Adds a layout.
 * @memberof DSClient
 * @param {Layout} layout
 * @see {@link DSClient#AddDrawer}
 * @see {@link DSClient#DestroyLayout}
 * @see {@link DSClient#RemoveLayout}
 * @see {@link Dialog#AddLayout}
 * @see {@link Dialog#RemoveLayout}
 * @see {@link Tabs#GetLayout}
 * @see {@link Layout}
 */
DSClient.prototype.AddLayout = function ( layout ) { prompt( "#", "App.AddLayout("+layout.id ); }

/**
 * 
 * @memberof DSClient
 * @param {String} msg
 * @param {String} title
 * @param {String} options
 * @param {String} hue
 */
DSClient.prototype.Alert = function ( msg,title,options,hue ) { prompt( "#", "App.Alert(\f"+msg+"\f"+title+"\f"+options+"\f"+hue ); }

/**
 * 
 * @memberof DSClient
 * @param {Function} callback
 * @param {Number} fps
 */
DSClient.prototype.Animate = function ( callback,fps )
	{
		_cbAnimate = callback;
		_anim_t = new Date().getTime();
		if( _isV8 ) {
			_fps=(fps?fps:30);
			if( _cbAnimate ) _tmAnimate = setInterval( _animatev8, 1000/_fps );
			else if( _tmAnimate ) clearInterval( _tmAnimate );
		}
		else {
			window._fps=(fps?fps:30); 
			requestAnimationFrame(_animate); 
		}
	}

/**
 * 
 * @memberof DSClient
 * @param {String} type
 * @param {String} msg
 */
DSClient.prototype.Broadcast = function ( type,msg ) { prompt( "#", "App.Broadcast("+type+"\f"+msg ); }

/**
 * 
 * @memberof DSClient
 * @param {String} action
 * @param {String} category
 * @param {String} data
 * @param {String} type
 * @param {String} extras
 * @param {String} options
 */
DSClient.prototype.BroadcastIntent = function ( action,category,data,type,extras,options ) { prompt( "#", "App.BroadcastIntent(\f"+action+"\f"+category+"\f"+data+"\f"+type+"\f"+extras+"\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {String} number
 */
DSClient.prototype.Call = function ( number ) { prompt( "#", "App.Call(\f"+number ); }

/**
 * 
 * @memberof DSClient
 * @param {String} key
 */
DSClient.prototype.CheckLicense = function ( key ) { prompt( "#", "App.CheckLicense(\f"+key ); }

/**
 * 
 * @memberof DSClient
 * @param {String} type
 * @returns {Boolean}
 */
DSClient.prototype.CheckPermission = function ( type ) { return prompt( "#", "App.CheckPermission(\f"+type )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} type
 * @param {Function} callback
 */
DSClient.prototype.ChooseContact = function ( type,callback ) { prompt( "#", "App.ChooseContact(\f"+type+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} msg
 * @param {String} type
 * @param {Function} callback
 */
DSClient.prototype.ChooseFile = function ( msg,type,callback ) { prompt( "#", "App.ChooseFile(\f"+msg+"\f"+type+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} options
 * @param {Function} callback
 */
DSClient.prototype.ChooseImage = function ( options,callback ) { prompt( "#", "App.ChooseImage(\f"+options+"\f"+_Cbm(callback) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} session
 */
DSClient.prototype.ClearCookies = function ( session ) { prompt( "#", "App.ClearCookies(\f"+session ); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 */
DSClient.prototype.ClearData = function ( file ) { prompt( "#", "App.ClearData(\f"+file ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 * @param {String} file
 */
DSClient.prototype.ClearValue = function ( name,file ) { prompt( "#", "App.ClearValue(\f"+name+"\f"+file ); }

/**
 * 
 * @memberof DSClient
 * @param {String} side
 */
DSClient.prototype.CloseDrawer = function ( side ) { prompt( "#", "App.CloseDrawer(\f"+side ); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 */
DSClient.prototype.CopyFile = function ( src,dest ) { prompt( "#", "App.CopyFile("+src+"\f"+dest); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 * @param {Boolean} overwrite
 * @param {String} filter
 */
DSClient.prototype.CopyFolder = function ( src,dest,overwrite,filter ) { prompt( "#", "App.CopyFolder(\f"+src+"\f"+dest+"\f"+overwrite+"\f"+filter); }

/*
 * Creation methods are located alongside the object definition(s) of the object(s) that they create.
 *
 * DSClient.prototype.CreateAudioRecorder
 * DSClient.prototype.CreateBluetoothList
 * DSClient.prototype.CreateBluetoothSerial
 * DSClient.prototype.CreateButton
 * DSClient.prototype.CreateCameraView
 * DSClient.prototype.CreateCheckBox
 * DSClient.prototype.CreateCodeEdit
 * DSClient.prototype.CreateCrypt
 * DSClient.prototype.CreateDebug
 * DSClient.prototype.CreateDialog
 * DSClient.prototype.CreateDownloader
 * DSClient.prototype.CreateEmail
 * DSClient.prototype.CreateFile
 * DSClient.prototype.CreateGLView
 * DSClient.prototype.CreateImage
 * DSClient.prototype.CreateLayout
 * DSClient.prototype.CreateList
 * DSClient.prototype.CreateListDialog
 * DSClient.prototype.CreateListView
 * DSClient.prototype.CreateLocator
 * DSClient.prototype.CreateMediaPlayer
 * DSClient.prototype.CreateMediaStore
 * DSClient.prototype.CreateNetClient
 * DSClient.prototype.CreateNotification
 * DSClient.prototype.CreateNxt
 * DSClient.prototype.CreateNxtRemote
 * DSClient.prototype.CreateObject
 * DSClient.prototype.CreatePhoneState
 * DSClient.prototype.CreatePlayStore
 * DSClient.prototype.CreateSMS
 * DSClient.prototype.CreateScroller
 * DSClient.prototype.CreateSeekBar
 * DSClient.prototype.CreateSensor
 * DSClient.prototype.CreateService
 * DSClient.prototype.CreateShortcut
 * DSClient.prototype.CreateSmartWatch
 * DSClient.prototype.CreateSpeechRec
 * DSClient.prototype.CreateSpinner
 * DSClient.prototype.CreateSynth
 * DSClient.prototype.CreateSysProc
 * DSClient.prototype.CreateTabs
 * DSClient.prototype.CreateText
 * DSClient.prototype.CreateTextEdit
 * DSClient.prototype.CreateTheme
 * DSClient.prototype.CreateToggle
 * DSClient.prototype.CreateUSBSerial
 * DSClient.prototype.CreateVideoView
 * DSClient.prototype.CreateWallpaper
 * DSClient.prototype.CreateWebGLView
 * DSClient.prototype.CreateWebServer
 * DSClient.prototype.CreateWebSocket
 * DSClient.prototype.CreateWebView
 * DSClient.prototype.CreateYesNoDialog
 * DSClient.prototype.CreateZipUtil
*/


/**
 * 
 * @memberof DSClient
 * @param {String} msg
 */
DSClient.prototype.Debug = function ( msg ) { prompt( "#", "App.Debug(\f"+msg ); }

/**
 * 
 * @memberof DSClient
 * @param {String} name
 */
DSClient.prototype.DeleteDatabase = function ( name ) { prompt( "#", "App.DeleteDatabase(\f"+name); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 */
DSClient.prototype.DeleteFile = function ( file ) { prompt( "#", "App.DeleteFile("+file); }

/**
 * 
 * @memberof DSClient
 * @param {String} fldr
 */
DSClient.prototype.DeleteFolder = function ( fldr ) { prompt( "#", "App.DeleteFolder("+fldr); }

/**
 * 
 * @memberof DSClient
 * @param {Layout} layout
 */
DSClient.prototype.DestroyDrawer = function ( layout ) { prompt( "#", "App.DestroyDrawer(\f"+ layout.id ); }

/**
 * 
 * @memberof DSClient
 * @param {Layout} layout
 */
DSClient.prototype.DestroyLayout = function ( layout ) { prompt( "#", "App.DestroyLayout("+ layout.id ); }

/**
 * 
 * @memberof DSClient
 * @param {String} keyList
 */
DSClient.prototype.DisableKeys = function ( keyList ) { prompt( "#", "App.DisableKeys(\f"+keyList ); }

/**
 * 
 * @memberof DSClient
 * @param {Boolean} disable
 */
DSClient.prototype.DisableTouch = function ( disable ) { prompt( "#", "App.DisableTouch(\f"+disable ); }

/**
 * 
 * @memberof DSClient
 * @param {String} filter
 * @param {Function} onFound
 * @param {Function} onComplete
 */
DSClient.prototype.DiscoverBtDevices = function ( filter,onFound,onComplete ) { prompt( "#", "App.DiscoverBtDevices(\f"+filter+"\f"+_Cbm(onFound)+"\f"+_Cbm(onComplete) ); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 * @param {String} title
 * @param {String} desc
 * @param {String} options
 */
DSClient.prototype.DownloadFile = function ( src,dest,title,desc,options ) { prompt( "#", "App.DownloadFile(\f"+src+"\f"+dest+"\f"+title+"\f"+desc+"\f"+options ); }

/**
 * 
 * @memberof DSClient
 * @param {Boolean} enable
 */
DSClient.prototype.EnableBackKey = function ( enable ) { prompt( "#", "App.EnableBackKey("+enable ); }

/**
 * 
 * @memberof DSClient
 * @param {String} msg
 * @param {String} line
 * @param {String} file
 * @param {String} quit
 */
DSClient.prototype.Error = function ( msg,line,file,quit ) { prompt( "#", "App.Error(\f"+msg+"\f"+line+"\f"+file+"\f"+quit ); }

/**
 * 
 * @memberof DSClient
 * @param {String} js
 */
DSClient.prototype.Execute = function ( js ) { prompt( "#", "App.Execute("+js ); }

/**
 * 
 * @memberof DSClient
 * @param {String} kill
 */
DSClient.prototype.Exit = function ( kill ) { prompt( "#", "App.Exit("+kill ); }

/**
 * 
 * @memberof DSClient
 * @param {String} src
 * @param {String} dest
 * @param {Boolean} overwrite
 */
DSClient.prototype.ExtractAssets = function ( src,dest,overwrite ) { prompt( "#", "App.ExtractAssets(\f"+src+"\f"+dest+"\f"+overwrite ); }

/**
 * 
 * @memberof DSClient
 * @param {String} file
 * @returns {Boolean}
 */
DSClient.prototype.FileExists = function ( file ) { return prompt( "#", "App.FileExists("+file )=="true"; }

/**
 * 
 * @memberof DSClient
 * @param {String} fldr
 * @returns {Boolean}
 */
DSClient.prototype.FolderExists = function ( fldr ) { return prompt( "#", "App.FolderExists("+fldr )=="true"; }
