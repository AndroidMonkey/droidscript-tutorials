
/**
 * Dialog
 * @constructor
 * @param {String} title
 * @param {String} options 
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function Dialog( text, options ) {
    var id = prompt( "#", "App.CreateDialog(\f"+title+"\f"+options );
    if ( id ) { DSControl.call( this, id ); }
    else { throw new DSError("Error creating Dialog"); }
}
Dialog.prototype = Object.create( DSControl.prototype ); 
Dialog.prototype.constructor = Dialog; 

Dialog.prototype.AddLayout = function( layout ) { prompt( this.id, "Dlg.AddLayout("+ layout.id ); }	
Dialog.prototype.Dismiss = function() { prompt( this.id, "Dlg.Dismiss(" ); }
Dialog.prototype.EnableBackKey = function( enable ) { prompt( this.id, "Dlg.EnableBackKey(\f"+enable ); }
Dialog.prototype.GetType = function() { return "Dialog"; }
Dialog.prototype.RemoveLayout = function( layout ) { prompt( this.id, "Dlg.RemoveLayout("+ layout.id ); }
Dialog.prototype.SetOnBack = function( callback ) { prompt( this.id, "Dlg.SetOnBack(\f"+_Cbm(callback) ); }
Dialog.prototype.SetOnCancel = function( callback ) { prompt( this.id, "Dlg.SetOnCancel(\f"+_Cbm(callback) ); }
Dialog.prototype.SetOnTouch = function( callback ) { prompt( this.id, "Dlg.SetOnClick("+_Cbm(callback) ); }
Dialog.prototype.SetTitle = function( title, options ) { prompt( this.id, "Dlg.SetTitle(\f"+title+"\f"+options ); }

/** @todo put these in order and cross-reference with layout. */
Dialog.prototype.Show = function() { prompt( this.id, "Dlg.Show(" ); }
Dialog.prototype.Hide = function() { prompt( this.id, "Dlg.Hide(" ); }
Dialog.prototype.SetBackColor = function( clr ) { prompt( this.id, "Dlg.SetBackColor(\f"+clr ); } 
Dialog.prototype.SetBackground = function( file, options ) { prompt( this.id, "Dlg.SetBackground(\f"+file+"\f"+options ); }
Dialog.prototype.AdjustColor = function( hue, sat, bright, cont ) { prompt( this.id, "Dlg.AdjustColor(\f"+hue+"\f"+sat+"\f"+bright+"\f"+cont ); }
Dialog.prototype.SetSize = function( width, height, options ) { prompt( this.id, "Dlg.SetSize(\f"+width+"\f"+height+"\f"+options ); }
Dialog.prototype.SetPosition = function( left, top, width, height, options ) { prompt( this.id, "Dlg.SetPosition(\f"+left+"\f"+top+"\f"+width+"\f"+height+"\f"+options ); }
