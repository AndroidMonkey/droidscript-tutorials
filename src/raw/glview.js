
/**
 * GLView
 * @constructor
 * @param {Number} width
 * @param {Number} height
 * @param {String} options
 * @Throws {DSError} Will throw an error if control cannot be created.
 */
function GLView( width, height, options ) {
    if ( options.toLowerCase().includes( "fast2d" )) {
        var id = prompt( "#", "App.CreateGLView("+width+"\f"+height+"\f"+options );
        if ( id ) { 
            DSControl.call( this, id );
            _LoadScriptSync( "/Sys/cp.js" );
            _LoadScriptSync( "/Sys/gl.js" );
            this.canvas = FastCanvas.create(); 
            this.ctx = this.canvas.getContext("2d");
            this.width = Math.round(app.GetDisplayWidth()*width);
            this.height = Math.round(app.GetDisplayHeight()*height);
            this.aspect = this.width / this.height;
        }
        else { throw new DSError("Error creating GLView"); }
    }
    else { throw new DSError("Error creating GLView, try including 'fast2d' in your options"); }
}
GLView.prototype = Object.create( DSControl.prototype ); 
GLView.prototype.constructor = GLView; 

GLView.prototype.Destroy = function() { prompt( this.id, "GLV.Release(" ); _map[this.id] = null; }
GLView.prototype.Exec = function( p1,p2,p3,p4 ) { _gfx.Exec( p1,p2,p3,p4 ); }
GLView.prototype.Execute = function( p1,p2,p3,p4 ) { prompt( this.id, "GLV.Execute(\f"+p1+"\f"+p2+"\f"+p3+"\f"+p4 ); }
GLView.prototype.GetType = function() { return "GLView"; }
GLView.prototype.Release = function() { prompt( this.id, "GLV.Release(" ); _map[this.id] = null; }
GLView.prototype.SetOnTouch = function( callback ) { prompt( this.id, "GLV.SetOnTouch(\f"+_Cbm(callback) ); }
GLView.prototype.SetOnTouchDown = function( callback ) { prompt( this.id, "GLV.SetOnTouchDown(\f"+_Cbm(callback) ); }
GLView.prototype.SetOnTouchMove = function( callback ) { prompt( this.id, "GLV.SetOnTouchMove(\f"+_Cbm(callback) ); }
GLView.prototype.SetOnTouchUp = function( callback ) { prompt( this.id, "GLV.SetOnTouchUp(\f"+_Cbm(callback) ); }
GLView.prototype.SetTouchable = function( touchable ) { prompt( this.id, "GLV.SetTouchable(\f"+touchable ); }
