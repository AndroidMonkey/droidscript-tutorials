# Function List

List of DSObjects and DSControls

(Objects in the order they appear in `App.js`, individual objects' methods in alphabetical order.)

Progress:
1. Function in place.
2. JSDoc scaffolded.
3. JSDoc completed. 
4. JSDoc cross referencing done.
5. Tested (√)

## Auto-release

    [2] Layout
        [4] AddChild
        [4] Animate
        [4] ChildToFront
        [4] DestroyChild
        [4] GetChildOrder
        [-] GetType
        [4] RemoveChild
        [4] SetGravity
        [4] SetOnChildChange
        [4] SetOnLongTouch
        [4] SetOnTouch
        [4] SetOnTouchDown
        [4] SetOnTouchMove
        [4] SetOnTouchUp
        [2] SetOrientation
        [4] SetTouchable

    [2] Image
        [2] Clear
        [2] Draw
        [2] DrawArc
        [2] DrawCircle
        [2] DrawFrame
        [2] DrawImage
        [2] DrawImageMtx
        [2] DrawLine
        [2] DrawPoint
        [2] DrawRectangle
        [2] DrawSamples
        [2] DrawText
        [2] Flatten
        [2] GetName
        [2] GetPixelColor
        [2] GetPixelData
        [2] GetType
        [2] MeasureText
        [2] Move
        [2] Reset
        [2] Rotate
        [2] Save
        [2] Scale
        [2] SetAlpha
        [2] SetAutoUpdate
        [2] SetColor
        [2] SetFontFile
        [2] SetImage
        [2] SetLineWidth
        [2] SetMaxRate
        [2] SetName
        [2] SetOnLoad
        [2] SetOnLongTouch
        [2] SetOnTouch
        [2] SetOnTouchDown
        [2] SetOnTouchMove
        [2] SetOnTouchUp
        [2] SetPaintColor
        [2] SetPaintStyle
        [2] SetPixelData
        [2] SetPixelMode
        [2] SetTextSize
        [2] SetTouchable
        [2] Skew
        [2] Transform
        [2] Update

    [3] Button
        [3] GetText
        [3] GetTextSize
        [3] GetType
        [3] SetEllipsize
        [3] SetFontFile
        [3] SetHtml
        [3] SetOnTouch
        [3] SetOnTouchEx
        [3] SetStyle
        [3] SetText
        [3] SetTextColor
        [3] SetTextShadow
        [3] SetTextSize

    [2] Toggle
        [2] GetChecked
        [2] GetText
        [2] GetTextSize
        [2] GetType
        [2] SetChecked
        [2] SetOnTouch
        [2] SetStyle
        [2] SetText
        [2] SetTextColor
        [2] SetTextSize

    [2] CheckBox
        [2] GetChecked
        [2] GetText
        [2] GetTextSize
        [2] GetType
        [2] SetChecked
        [2] SetOnTouch
        [2] SetText
        [2] SetTextColor
        [2] SetTextSize

    [2] Spinner
        [2] GetText
        [3] GetTextSize
        [3] GetType
        [2] SelectItem
        [2] SetList
        [2] SetOnChange
        [2] SetOnTouch
        [2] SetText
        [2] SetTextColor
        [3] SetTextSize

    [2] SeekBar
        [3] GetType
        [2] GetValue
        [2] SetMaxRate
        [2] SetOnChange
        [2] SetOnTouch
        [2] SetRange
        [2] SetValue

    [2] Text
        [3] GetHtml
        [3] GetLineCount
        [3] GetLineStart
        [3] GetLineTop
        [3] GetMaxLines
        [3] GetText
        [3] GetTextSize
        [3] GetType
        [2] Log
        [2] SetEllipsize
        [2] SetFontFile
        [2] SetHtml
        [2] SetLog
        [2] SetOnLongTouch
        [2] SetOnTouch
        [2] SetOnTouchDown
        [2] SetOnTouchMove
        [2] SetOnTouchUp
        [2] SetText
        [2] SetTextColor
        [2] SetTextShadow
        [2] SetTextSize
        [2] SetTouchable

    [2] TextEdit
        [2] ClearHistory
        [3] GetCursorLine
        [3] GetCursorPos
        [3] GetHtml
        [3] GetLineCount
        [3] GetLineStart
        [3] GetLineTop
        [3] GetMaxLines
        [3] GetSelectedText
        [3] GetSelectionEnd
        [3] GetSelectionStart
        [3] GetText
        [3] GetTextSize
        [3] GetType
        [2] InsertText
        [2] Redo
        [2] ReplaceText
        [2] SetCursorColor
        [2] SetCursorPos
        [2] SetHint
        [2] SetHtml
        [2] SetOnChange
        [2] SetOnEnter
        [2] SetOnTouch
        [2] SetSelection
        [2] SetText
        [2] SetTextColor
        [2] SetTextSize
        [2] Undo

    [2] List
        [2] AddItem
        [2] GetItem
        [2] GetItemByIndex
        [2] GetLength
        [2] GetList
        [3] GetTextSize
        [3] GetType
        [2] InsertItem
        [2] RemoveAll
        [2] RemoveItem
        [2] RemoveItemByIndex
        [2] ScrollToItem
        [2] ScrollToItemByIndex
        [2] SelectItem
        [2] SelectItemByIndex
        [2] SetColumnWidths
        [2] SetDivider
        [2] SetEllipsize
        [2] SetEllipsize1
        [2] SetEllipsize2
        [2] SetFontFile
        [2] SetHiTextColor1
        [2] SetHiTextColor2
        [2] SetIconMargins
        [2] SetIconSize
        [2] SetItem
        [2] SetItemByIndex
        [2] SetList
        [2] SetOnLongTouch
        [2] SetOnTouch
        [2] SetTextColor
        [2] SetTextColor1
        [2] SetTextColor2
        [2] SetTextMargins
        [2] SetTextShadow
        [2] SetTextShadow1
        [2] SetTextShadow2
        [2] SetTextSize
        [2] SetTextSize1
        [2] SetTextSize2

    [2] WebView
        [2] Back
        [2] CanGoBack
        [2] CanGoForward
        [2] Capture
        [2] ClearHistory
        [2] Execute
        [2] Forward
        [2] GetType
        [2] GetUrl
        [2] LoadHtml
        [2] LoadUrl
        [2] Print
        [2] Reload
        [2] SetOnConsole
        [2] SetOnError
        [2] SetOnProgress
        [2] SetRedirect
        [2] SetUserAgent
        [2] SetUserCreds
        [2] SimulateKey

    [2] Scroller
        [2] AddChild
        [2] DestroyChild
        [2] GetScrollX
        [2] GetScrollY
        [3] GetType
        [2] RemoveChild
        [2] ScrollBy
        [2] ScrollTo

    [2] CameraView
        [2] AutoCapture
        [2] FindFaces
        [2] GetCameraCount
        [2] GetColorEffects
        [2] GetImageHeight
        [2] GetImageWidth
        [2] GetMaxZoom
        [2] GetParameters
        [2] GetPictureSizes
        [2] GetPixelData
        [2] GetType
        [2] GetZoom
        [2] HasFlash
        [2] IsRecording
        [2] MotionMosaic
        [2] Record
        [2] ReportColors
        [2] SetColorEffect
        [2] SetDuplicateImage
        [2] SetFlash
        [2] SetFocusMode
        [2] SetOnFocus
        [2] SetOnMotion
        [2] SetOnPicture
        [2] SetOnReady
        [2] SetOrientation
        [2] SetParameter
        [2] SetPictureSize
        [2] SetPostRotation
        [2] SetPreviewImage
        [2] SetSound
        [2] SetVideoSize
        [2] SetZoom
        [2] StartPreview
        [2] Stop
        [2] StopPreview
        [2] Stream
        [3] TakePicture

    [2] VideoView
        [2] GetDuration
        [2] GetType
        [2] IsPlaying
        [2] IsReady
        [2] Pause
        [2] Play
        [2] SeekTo
        [2] SetFile
        [2] SetOnComplete
        [2] SetOnError
        [2] SetOnReady
        [2] SetOnSubtitle
        [2] SetSubtitles
        [2] SetVolume
        [2] Stop

    [2] CodeEdit
        [2] ClearHistory
        [2] Copy
        [2] Cut
        [2] GetCursorLine
        [2] GetCursorPos
        [2] GetLineStart
        [2] GetSelectMode
        [2] GetSelectedText
        [2] GetText
        [2] GetType
        [2] HighlightLine
        [2] InsertText
        [2] Paste
        [2] Redo
        [2] Replace
        [2] ReplaceAll
        [2] ReplaceText
        [2] Search
        [2] SelectAll
        [2] SetColorScheme
        [2] SetCursorPos
        [2] SetHtml
        [2] SetLanguage
        [2] SetNavigationMethod
        [2] SetOnChange
        [2] SetOnDoubleTap
        [2] SetOnKey
        [2] SetSelectMode
        [2] SetSelection
        [2] SetText
        [2] SetTextColor
        [2] SetTextSize
        [2] SetUseKeyboard
        [2] Undo

    [2] Theme
        [3] GetType
        [2] SetBtnTextColor
        [2] SetButtonOptions
        [2] SetButtonStyle
        [2] SetCheckBoxOptions
        [2] SetDialogBtnColor
        [2] SetDialogBtnTxtColor
        [2] SetDialogColor
        [2] SetDimBehind
        [2] SetProgressBackColor
        [2] SetProgressBarOptions
        [2] SetProgressTextColor
        [2] SetTextColor
        [2] SetTextEditOptions
        [2] SetTitleColor
        [2] SetTitleDividerColor
        [2] SetTitleHeight

## Singleton

    [ ] YesNoDialog
        [ ] Dismiss
        [ ] GetType
        [ ] SetButtonText
        [ ] SetOnTouch

    [1] ListDialog
        [1] Dismiss
        [1] GetType
        [1] SetOnTouch
        [1] SetTextColor
        [1] SetTitle

    [1] ListView
        [1] GetType
        [1] SetOnTouch

    [ ] BluetoothList
        [ ] GetType
        [ ] SetOnTouch
        [ ] SetOnTouchEx

    [ ] AudioRecorder
        [ ] GetData
        [ ] GetPeak
        [ ] GetRMS
        [ ] GetType
        [ ] Pause
        [ ] SetFile
        [ ] SetFrequency
        [ ] Start
        [ ] Stop

    [ ] SMS
        [ ] GetType
        [ ] Send
        [ ] SetOnMessage
        [ ] SetOnStatus

    [ ] Email
        [ ] GetType
        [ ] Receive
        [ ] Send
        [ ] SetIMAP
        [ ] SetOnMessage
        [ ] SetOnStatus
        [ ] SetSMTP

    [ ] SmartWatch

    [ ] Crypt
        [ ] Decrypt
        [ ] Encrypt
        [ ] GetType
        [ ] Hash

    [ ] SpeechRec
        [ ] Cancel
        [ ] GetRMS
        [ ] GetType
        [ ] IsListening
        [ ] Recognize
        [ ] SetOnError
        [ ] SetOnReady
        [ ] SetOnResult
        [ ] Stop

    [ ] PhoneState
        [ ] GetType
        [ ] SetOnChange
        [ ] Start
        [ ] Stop

    [ ] Wallpaper
        [ ] GetType

## Manual-release

    [ ] Dialog
        [ ] AddLayout
        [ ] Dismiss
        [ ] EnableBackKey
        [ ] GetType
        [ ] RemoveLayout
        [ ] SetOnBack
        [ ] SetOnCancel
        [ ] SetOnTouch
        [ ] SetTitle

    [ ] MediaPlayer
        [ ] Close
        [ ] GetDuration
        [ ] GetType
        [ ] IsLooping
        [ ] IsPlaying
        [ ] IsReady
        [ ] Pause
        [ ] Play
        [ ] SeekTo
        [ ] SetFile
        [ ] SetLooping
        [ ] SetOnComplete
        [ ] SetOnReady
        [ ] SetOnSeekDone
        [ ] SetVolume
        [ ] Stop

    [ ] Sensor
        [ ] GetAzimuth
        [ ] GetNames
        [ ] GetPitch
        [ ] GetRoll
        [ ] GetType
        [ ] GetValues
        [ ] SetMaxRate
        [ ] SetMinChange
        [ ] SetOnChange
        [ ] Start
        [ ] Stop

    [ ] Locator
        [ ] GetBearingTo
        [ ] GetDistanceTo
        [ ] GetType
        [ ] SetOnChange
        [ ] SetRate
        [ ] Start
        [ ] Stop

    [ ] NetClient
        [ ] AutoReceive
        [ ] Connect
        [ ] Disconnect
        [ ] DownloadFile
        [ ] GetBroadcastAddress
        [ ] GetType
        [ ] IsConnected
        [ ] ReceiveBytes
        [ ] ReceiveDatagram
        [ ] ReceiveDatagrams
        [ ] ReceiveFile
        [ ] ReceiveText
        [ ] SendBytes
        [ ] SendDatagram
        [ ] SendText
        [ ] SetOnConnect
        [ ] SetOnDownload
        [ ] SetOnReceive
        [ ] SetTimeout
        [ ] WakeOnLan

    [ ] NxtRemote
        [ ] Beep
        [ ] Brake
        [ ] CheckConnection
        [ ] Connect
        [ ] Disconnect
        [ ] Drive
        [ ] FileFindFirst
        [ ] FileFindNext
        [ ] GetBtAddress
        [ ] GetBtName
        [ ] GetCurrentProgram
        [ ] GetRotationCount
        [ ] GetType
        [ ] IsBluetoothEnabled
        [ ] IsConnected
        [ ] IsMotorIdle
        [ ] IsPaired
        [ ] PlaySoundFile
        [ ] ReadColorSensor
        [ ] ReadDistanceSensor
        [ ] ReadLightSensor
        [ ] ReadMail
        [ ] ReadSoundSensor
        [ ] ReadTouchSensor
        [ ] RequestEnable
        [ ] Reset
        [ ] SendMail
        [ ] SetInvert
        [ ] SetLampColor
        [ ] SetOnConnect
        [ ] SetOnConnectEx
        [ ] SetOnConnected
        [ ] ShowDevices
        [ ] StartProgram
        [ ] Stop
        [ ] StopProgram
        [ ] ToColorName

    [ ] WebServer
        [ ] AddRedirect
        [ ] AddServlet
        [ ] GetType
        [ ] GetWebSockClients
        [ ] SendText
        [ ] SetFolder
        [ ] SetOnReceive
        [ ] SetOnUpload
        [ ] SetResponse
        [ ] SetUploadFolder
        [ ] Start
        [ ] Stop

    [ ] USBSerial
        [ ] GetType
        [ ] IsConnected
        [ ] SetDataMode
        [ ] SetDTR
        [ ] SetMaxRead
        [ ] SetMaxWrite
        [ ] SetOnReceive
        [ ] SetRTS
        [ ] SetSplitMode
        [ ] SetTimeout
        [ ] Start
        [ ] Stop
        [ ] Write

    [ ] SysProc
        [ ] Err
        [ ] GetType
        [ ] In
        [ ] Out
        [ ] ReadFileAsByte
        [ ] WriteToFile

    [ ] Service
        [ ] GetType
        [ ] SendMessage
        [ ] SetOnMessage
        [ ] Stop

    [ ] Object

    [ ] Synth
        [ ] GetType
        [ ] PlayMidiTune
        [ ] PlayNote
        [ ] PlayTone
        [ ] SetDelay
        [ ] SetDelayEnabled
        [ ] SetFeedback
        [ ] SetFrequency
        [ ] SetNoteLength
        [ ] SetPhaser
        [ ] SetPhaserDryWet
        [ ] SetPhaserEnabled
        [ ] SetPhaserFeedback
        [ ] SetPhaserRange
        [ ] SetPhaserRate
        [ ] SetVca
        [ ] SetVcaAttack
        [ ] SetVcaDecay
        [ ] SetVcaEnabled
        [ ] SetVcaRelease
        [ ] SetVcaSustain
        [ ] SetVcf
        [ ] SetVcfAttack
        [ ] SetVcfCutoff
        [ ] SetVcfDecay
        [ ] SetVcfDepth
        [ ] SetVcfEnabled
        [ ] SetVcfRelease
        [ ] SetVcfResonance
        [ ] SetVcfSustain
        [ ] SetVolume
        [ ] SetWaveShape
        [ ] Start
        [ ] Stop

    [ ] BluetoothSerial
        [ ] Clear
        [ ] Connect
        [ ] Disconnect
        [ ] GetType
        [ ] IsBluetoothEnabled
        [ ] IsConnected
        [ ] IsPaired
        [ ] Listen
        [ ] RequestEnable
        [ ] SetOnConnect
        [ ] SetOnDisconnect
        [ ] SetOnReceive
        [ ] SetSplitMode
        [ ] SetTimeout
        [ ] Write

    [ ] ZipUtil
        [ ] AddFile
        [ ] AddText
        [ ] Close
        [ ] Create
        [ ] CreateDebugKey
        [ ] CreateKey
        [ ] Extract
        [ ] GetType
        [ ] List
        [ ] Open
        [ ] Sign
        [ ] UpdateManifest

    [ ] Downloader
        [ ] Download
        [ ] GetProgress
        [ ] GetSize
        [ ] GetType
        [ ] IsComplete
        [ ] SetOnCancel
        [ ] SetOnComplete
        [ ] SetOnDownload
        [ ] SetOnError

    [ ] MediaStore
        [ ] GetAlbumArt
        [ ] GetSongArt
        [ ] GetType
        [ ] QueryAlbums
        [ ] QueryArtists
        [ ] QueryMedia
        [ ] SetOnAlbumsResult
        [ ] SetOnArtistsResult
        [ ] SetOnMediaResult

    [ ] PlayStore
        [ ] GetBillingInfo
        [ ] GetPurchases
        [ ] GetType
        [ ] Purchase

    [ ] Notification
        [ ] Cancel
        [ ] GetType
        [ ] Notify
        [ ] SetLargeImage
        [ ] SetLights
        [ ] SetMessage

    [ ] File
        [ ] Close
        [ ] GetLength
        [ ] GetPointer
        [ ] GetType
        [ ] ReadData
        [ ] ReadNumber
        [ ] ReadText
        [ ] Seek
        [ ] SetLength
        [ ] Skip
        [ ] WriteData
        [ ] WriteNumber
        [ ] WriteText

    [ ] Nxt
        [ ] Beep
        [ ] Brake
        [ ] CheckConnection
        [ ] Connect
        [ ] Disconnect
        [ ] Drive
        [ ] FileFindFirst
        [ ] FileFindNext
        [ ] GetBtAddress
        [ ] GetBtName
        [ ] GetCurrentProgram
        [ ] GetRotationCount
        [ ] GetType
        [ ] IsBluetoothEnabled
        [ ] IsConnected
        [ ] IsMotorIdle
        [ ] IsPaired
        [ ] PlaySoundFile
        [ ] ReadColorSensor
        [ ] ReadDistanceSensor
        [ ] ReadLightSensor
        [ ] ReadMail
        [ ] ReadSoundSensor
        [ ] ReadTouchSensor
        [ ] RequestEnable
        [ ] Reset
        [ ] SendMail
        [ ] SetInvert
        [ ] SetLampColor
        [ ] SetOnConnect
        [ ] SetOnConnectEx
        [ ] SetOnConnected
        [ ] ShowDevices
        [ ] StartProgram
        [ ] Stop
        [ ] StopProgram
        [ ] ToColorName

    [ ] Tabs
        [ ] AddChild
        [ ] Animate
        [ ] ChildToFront
        [ ] DestroyChild
        [ ] GetChildOrder
        [ ] GetLayout
        [ ] GetType
        [ ] OnChange
        [ ] RemoveChild
        [ ] SetGravity
        [ ] SetOnChange
        [ ] SetOnChildChange
        [ ] SetOnLongTouch
        [ ] SetOnTouch
        [ ] SetOnTouchDown
        [ ] SetOnTouchMove
        [ ] SetOnTouchUp
        [ ] SetOrientation
        [ ] SetTouchable
        [ ] ShowTab

    [ ] WebSocket
        [ ] Close
        [ ] GetSocket
        [ ] IsOpen
        [ ] Send
        [ ] SetOnClose
        [ ] SetOnMessage
        [ ] SetOnOpen

    [1] GLView
        [1] Exec
        [1] Execute
        [1] GetType
        [1] SetOnTouch
        [1] SetOnTouchDown
        [1] SetOnTouchMove
        [1] SetOnTouchUp
        [1] SetTouchable

# `App.js` Method List

## Non Creation Methods

    [4] AddDrawer
    [4] AddLayout
    [2] Alert
    [2] Animate
    [2] Broadcast
    [2] BroadcastIntent
    [2] Call
    [2] CheckLicense
    [2] CheckPermission
    [2] ChooseContact
    [2] ChooseFile
    [2] ChooseImage
    [2] ClearCookies
    [2] ClearData
    [2] ClearValue
    [2] CloseDrawer
    [2] CopyFile
    [2] CopyFolder
    [2] Debug
    [2] DeleteDatabase
    [2] DeleteFile
    [2] DeleteFolder
    [2] DestroyDrawer
    [2] DestroyLayout
    [2] DisableKeys
    [2] DisableTouch
    [2] DiscoverBtDevices
    [2] DownloadFile
    [2] EnableBackKey
    [2] Error
    [2] Execute
    [2] Exit
    [2] ExtractAssets
    [2] FileExists
    [2] FolderExists
    [2] GA
    [2] GetAccounts
    [2] GetActivities
    [2] GetAppName
    [2] GetAppPath
    [2] GetBatteryLevel
    [2] GetBluetoothAddress
    [2] GetBluetoothName
    [2] GetBtProfileState
    [2] GetBuildNum
    [2] GetChargeType
    [2] GetClipboardText
    [2] GetCountry
    [2] GetCountryCode
    [2] GetDSVersion
    [2] GetData
    [2] GetDatabaseFolder
    [2] GetDefaultOrientation
    [2] GetDeviceId
    [2] GetDisplayHeight
    [2] GetDisplayWidth
    [2] GetEnv
    [2] GetExternalFolder
    [2] GetFileDate
    [2] GetFileSize
    [2] GetFreeSpace
    [2] GetIPAddress
    [2] GetInstalledApps
    [2] GetIntent
    [2] GetInternalFolder
    [2] GetJoystickName
    [2] GetJoystickState
    [2] GetKeyboardHeight
    [2] GetLanguage
    [2] GetLanguageCode
    [2] GetLastButton
    [2] GetLastCheckBox
    [2] GetLastImage
    [2] GetLastToggle
    [2] GetMacAddress
    [2] GetMediaFile
    [2] GetMemoryInfo
    [2] GetMetadata
    [2] GetModel
    [2] GetName
    [2] GetNotifyId
    [2] GetOSVersion
    [2] GetObjects
    [2] GetOptions
    [2] GetOrientation
    [2] GetPackageName
    [2] GetPairedBtDevices
    [2] GetPath
    [2] GetPermission
    [2] GetPrivateFolder
    [2] GetRSSI
    [2] GetResourceId
    [2] GetRingerMode
    [2] GetRotation
    [2] GetRunningApps
    [2] GetRunningServices
    [2] GetSSID
    [2] GetScreenDensity
    [2] GetScreenHeight
    [2] GetScreenWidth
    [2] GetSharedFiles
    [2] GetSharedText
    [2] GetSpeakerPhone
    [2] GetSpecialFolder
    [2] GetThumbnail
    [2] GetTop
    [2] GetType
    [2] GetUser
    [2] GetVersion
    [2] GetVolume
    [2] GoToSleep
    [2] HasSoftNav
    [2] HideKeyboard
    [2] HideProgress
    [2] HideProgressBar
    [2] HttpRequest
    [2] InIDE
    [2] InstallWallpaper
    [2] IsAPK
    [2] IsAppInstalled
    [2] IsBluetoothEnabled
    [2] IsBluetoothOn
    [2] IsBtDevicePaired
    [2] IsCharging
    [2] IsChrome
    [2] IsConnected
    [2] IsEngine
    [2] IsFolder
    [2] IsKeyboardShown
    [2] IsLocationEnabled
    [2] IsNewVersion
    [2] IsPremium
    [2] IsScreenOn
    [2] IsService
    [2] IsTablet
    [2] IsThings
    [2] IsWifiEnabled
    [2] KillApp
    [2] Language2Code
    [2] ListFolder
    [2] LoadBoolean
    [2] LoadNumber
    [2] LoadPlugin
    [2] LoadScript
    [2] LoadText
    [2] Lock
    [2] LockDrawer
    [2] MakeFolder
    [2] Odroid
    [2] OpenDatabase
    [2] OpenDrawer
    [2] OpenFile
    [2] OpenUrl
    [2] PairBtDevice
    [2] PlayRingtone
    [2] PreventScreenLock
    [2] PreventWifiSleep
    [2] QueryContent
    [2] ReadFile
    [2] RemoveDrawer
    [2] RemoveLayout
    [2] RenameFile
    [2] RenameFolder
    [2] SaveBoolean
    [2] SaveCookies
    [2] SaveNumber
    [2] SaveText
    [2] ScanFile
    [2] ScreenShot
    [2] SendFile
    [2] SendIntent
    [2] SendMail
    [2] SendMessage
    [2] SendText
    [2] SetAlarm
    [2] SetAutoBoot
    [2] SetAutoStart
    [2] SetAutoWifi
    [2] SetBackColor
    [2] SetBluetoothEnabled
    [2] SetClipboardText
    [2] SetData
    [2] SetDebugEnabled
    [2] SetDensity
    [2] SetJoystickOptions
    [2] SetKioskMode
    [2] SetLanguage
    [2] SetMenu
    [2] SetOnBroadcast
    [2] SetOnDebug
    [2] SetOnError
    [2] SetOnKey
    [2] SetOnShowKeyboard
    [2] SetOnWifiChange
    [2] SetOptions
    [2] SetOrientation
    [2] SetPosition
    [2] SetPriority
    [2] SetRingerMode
    [2] SetScreenBrightness
    [2] SetScreenMode
    [2] SetSharedApp
    [2] SetSpeakerPhone
    [2] SetTheme
    [2] SetTitle
    [2] SetUserAgent
    [2] SetUserCreds
    [2] SetVolume
    [2] SetWifiEnabled
    [2] ShowDebug
    [2] ShowKeyboard
    [2] ShowMenu
    [2] ShowPopup
    [2] ShowProgress
    [2] ShowProgressBar
    [2] ShowTextDialog
    [2] ShowTip
    [2] SimulateKey
    [2] SimulateTouch
    [2] Start
    [2] StartApp
    [2] StartDebugServer
    [2] StartService
    [2] StopApp
    [2] StopDebugServer
    [2] StopService
    [2] SysExec
    [2] TextToSpeech
    [2] ToBack
    [2] Unlock
    [2] UnlockDrawer
    [2] UnpairBtDevice
    [2] UnzipFile
    [2] UpdateProgressBar
    [2] UploadFile
    [2] Uri2Path
    [2] Vibrate
    [2] Wait
    [2] WakeUp
    [2] WifiConnect
    [2] WriteFile
    [2] ZipFile
    [2] ZipFolder