# DroidScript Notes


### Options
  * Default - Options have a default value.
  * Requirement - Options are not required, 
    the default is used for skipped options.
  * Case - Options can be any combination of upper and lower case.
    Meaning "px" is the same as "PX" or "Px".
  * Typos - Mispelled or incorrect options are ignored, 
    the default is used instead.