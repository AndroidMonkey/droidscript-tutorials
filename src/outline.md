# DroidScript Object Reference

## Contents

### 1. Terms Used

#### "Objects" and "Controls"

JavaScript tutorials and references talk about "objects" a great deal, because well...

> “In JavaScript, almost "everything" is an object.“

<caption>‹Quoted from w3schools.com <a href="https://www.w3schools.com/js/js_object_definition.asp">"JavaScript Objects"</a>›</caption>

Within *this* guide, DroidScript Objects (DSO or DSObject) have the specific meaning of objects created by DroidScript and have the *`id`* property and `GetType()` methods in common, while DroidScript objects with additional methods and properties to interact with the user — such as the `Button` or `Text` objects — will be referred to as DroidScript Controls (DSControl).

### 2. Controls List

- 2.2. Button
- 2.3. CameraView
- CheckBox
- Dialog
- GLView
- Image
- Layout
- List
- ListDialog
- ListView


- CodeEdit (*premium*)

### 2.1. Common Control Methods

 - AdjustColor
 - ClearFocus
 - Destroy
 - Focus
 - GetAbsHeight
 - GetAbsWidth
 - GetHeight
 - GetLeft
 - GetPosition
 - GetTop
 - GetVisibility
 - GetWidth
 - Gone
 - Hide
 - IsEnabled
 - IsVisible
 - Release
 - SetBackAlpha
 - SetBackColor
 - SetBackGradient
 - SetBackGradientRadial
 - SetBackground
 - SetColorFilter
 - SetEnabled
 - SetMargins
 - SetPadding
 - SetPosition
 - SetScale
 - SetSize
 - SetVisibility
 - Show
 - Tween

### 2.2. Button Methods

 - GetText
 - GetTextSize
 - GetType
 - SetEllipsize
 - SetFontFile
 - SetHtml
 - SetOnTouch
 - SetOnTouchEx
 - SetStyle
 - SetText
 - SetTextColor
 - SetTextShadow
 - SetTextSize

### CameraView Methods

 - AutoCapture
 - FindFaces
 - GetCameraCount
 - GetColorEffects
 - GetImageHeight
 - GetImageWidth
 - GetMaxZoom
 - GetParameters
 - GetPictureSizes
 - GetPixelData
 - GetType
 - GetZoom
 - HasFlash
 - IsRecording
 - MotionMosaic
 - Record
 - ReportColors
 - SetColorEffect
 - SetDuplicateImage
 - SetFlash
 - SetFocusMode
 - SetOnFocus
 - SetOnMotion
 - SetOnPicture
 - SetOnReady
 - SetOrientation
 - SetParameter
 - SetPictureSize
 - SetPostRotation
 - SetPreviewImage
 - SetSound
 - SetVideoSize
 - SetZoom
 - StartPreview
 - Stop
 - StopPreview
 - Stream
 - TakePicture

### Checkbox Methods

 - GetChecked
 - GetText
 - GetTextSize
 - GetType
 - SetChecked
 - SetOnTouch
 - SetText
 - SetTextColor
 - SetTextSize

### Dialog Methods

 - AddLayout
 - Dismiss
 - EnableBackKey
 - GetType
 - RemoveLayout
 - SetOnBack
 - SetOnCancel
 - SetOnTouch
 - SetTitle

### GLView Methods

 - Exec
 - Execute
 - GetType
 - SetOnTouch
 - SetOnTouchDown
 - SetOnTouchMove
 - SetOnTouchUp
 - SetTouchable

### GLView Properties
 - aspect
 - canvas
 - ctx
 - height
 - width

### Image Methods

 - Clear
 - Draw
 - DrawArc
 - DrawCircle
 - DrawFrame
 - DrawImage
 - DrawImageMtx
 - DrawLine
 - DrawPoint
 - DrawRectangle
 - DrawSamples
 - DrawText
 - Flatten
 - GetName
 - GetPixelColor
 - GetPixelData
 - GetType
 - MeasureText
 - Move
 - Reset
 - Rotate
 - Save
 - Scale
 - SetAlpha
 - SetAutoUpdate
 - SetColor
 - SetFontFile
 - SetImage
 - SetLineWidth
 - SetMaxRate
 - SetName
 - SetOnLoad
 - SetOnLongTouch
 - SetOnTouch
 - SetOnTouchDown
 - SetOnTouchMove
 - SetOnTouchUp
 - SetPaintColor
 - SetPaintStyle
 - SetPixelData
 - SetPixelMode
 - SetTextSize
 - SetTouchable
 - Skew
 - Transform
 - Update

### Layout Methods

 - AddChild
 - Animate
 - ChildToFront
 - DestroyChild
 - GetChildOrder
 - GetType
 - RemoveChild
 - SetGravity
 - SetOnChildChange
 - SetOnLongTouch
 - SetOnTouch
 - SetOnTouchDown
 - SetOnTouchMove
 - SetOnTouchUp
 - SetOrientation
 - SetTouchable

### List Methods

 - AddItem
 - GetItem
 - GetItemByIndex
 - GetLength
 - GetList
 - GetTextSize
 - GetType
 - InsertItem
 - RemoveAll
 - RemoveItem
 - RemoveItemByIndex
 - ScrollToItem
 - ScrollToItemByIndex
 - SelectItem
 - SelectItemByIndex
 - SetColumnWidths
 - SetDivider
 - SetEllipsize
 - SetEllipsize1
 - SetEllipsize2
 - SetFontFile
 - SetHiTextColor1
 - SetHiTextColor2
 - SetIconMargins
 - SetIconSize
 - SetItem
 - SetItemByIndex
 - SetList
 - SetOnLongTouch
 - SetOnTouch
 - SetTextColor
 - SetTextColor1
 - SetTextColor2
 - SetTextMargins
 - SetTextShadow
 - SetTextShadow1
 - SetTextShadow2
 - SetTextSize
 - SetTextSize1
 - SetTextSize2

### ListDialog Methods

 - Dismiss
 - GetType
 - SetOnTouch
 - SetTextColor
 - SetTitle

### ListView Methods

 - GetType
 - SetOnTouch

### Scroller Methods

 - AddChild
 - DestroyChild
 - GetScrollX
 - GetScrollY
 - GetType
 - RemoveChild
 - ScrollBy
 - ScrollTo

### SeekBar Methods

 - GetType
 - GetValue
 - SetMaxRate
 - SetOnChange
 - SetOnTouch
 - SetRange
 - SetValue